﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Search : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["search"] != null)
            {
                lblamit.Text = "Search Result for " + Request.QueryString["search"];
                bindpenproducts(Request.QueryString["search"]);
                bindrefillproducts(Request.QueryString["search"]);


            }
            else if (Convert.ToInt32(Request.QueryString["nosearch"]) == 1)
            {
                lblamit.Text = "No Search Result Found";
            }
        }
    }

    private void bindpenproducts(string str)
    {

        //DataTable objtable = Class1.GetRecord("select *  from ProductTB where P_search like '%" + str + "%' and P_Status='Active'");
        DataTable objtable = Class1.GetRecord("EXEC GetProductTB 'Search',0,'"+str+"'");

        string[] name = new string[objtable.Rows.Count];        

        for (int i = 0; i <= objtable.Rows.Count - 1; i++)
        {
            name[i] = "~/Addgel/AdminImage/" + objtable.Rows[i]["P_Thumbnail"].ToString();

            objtable.Rows[i]["P_Thumbnail"] = name[i];
        }

        
        if (objtable.Rows.Count > 0)
        {
            pentab.Visible = true;
            DataList1.DataSource = objtable;
            DataList1.DataBind();
            lblamit.Text = "Search Result for " + Request.QueryString["search"];
        }
        //else
        //{
        //    lblamit.Text = "No Product Found";
        //}      


    }



    private void bindrefillproducts(string str)
    {
        //DataTable objtable = Class1.GetRecord("select *  from RefillTB where R_search like '%" + str + "%' and R_Status='Active'");
        DataTable objtable = Class1.GetRecord("EXEC GETRefillTB 'Search',0,'" + str + "'");

        string[] name = new string[objtable.Rows.Count];


        for (int i = 0; i <= objtable.Rows.Count - 1; i++)
        {
            name[i] = "~/Addgel/AdminImage/" + objtable.Rows[i]["R_Thumbnail"].ToString();

            objtable.Rows[i]["R_Thumbnail"] = name[i];
        }
        if (objtable.Rows.Count > 0)
        {
            refilltab.Visible = true;
            DataList2.DataSource = objtable;
            DataList2.DataBind();
            lblamit.Text = "Search Result for " + Request.QueryString["search"];
        }
        //else
        //{
        //    lblamit.Text = "No Product Found";
        //}


    }


    protected void seedetail_Click(object sender, ImageClickEventArgs e)
    {


    }
   
    public void Item_Command(Object sender, DataListCommandEventArgs e)
    {
        if (e.CommandName.Equals("ProductDetails"))
        {
            try
            {
                int keyField = Convert.ToInt32(DataList1.DataKeys[e.Item.ItemIndex]);
                DataTable dt = Class1.GetRecord("select * from CategoryTB where C_PId=" + keyField + " and C_CatType=1");
                Response.Redirect("" + dt.Rows[0]["C_Navigation"].ToString() + "?id=" + keyField);

            }

            catch (Exception ex)
            {
                lblamit.Text = ex.Message.ToString();
            }
        }  
           
    }

    protected void DataList2_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName.Equals("ProductDetails"))
        {
            try
            {
                int keyField = Convert.ToInt32(DataList2.DataKeys[e.Item.ItemIndex]);
                DataTable dt = Class1.GetRecord("select * from CategoryTB where C_PId=" + keyField + " and C_CatType=2");
                Response.Redirect("" + dt.Rows[0]["C_Navigation"].ToString() + "?id=" + keyField);

            }

            catch (Exception ex)
            {
                lblamit.Text = ex.Message.ToString();
            }
        }  
    }
}

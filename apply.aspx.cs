﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class apply : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["id"] != null)
            {
                int id = Convert.ToInt32(Request.QueryString["id"]);
            }
        }
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        int productid = Class1.MaxResumeId() + 1;

        string name = txtname.Text.Replace("'", "''");
        string email = txtemail.Text.Replace("'", "''");
        string extensionI1 = System.IO.Path.GetExtension(FileUpload1.FileName);
        string resume = string.Empty;
        if (extensionI1 != "")
        {
            resume = name + "" + productid + "" + extensionI1;
        }

        string date=DateTime.Today.ToShortDateString();


        bool check = false;
        SqlConnection objcon = new SqlConnection(Class1.ConnectionString());
       using (SqlCommand cmd = new SqlCommand("AddUpdateGetApplyJobsTB", objcon))
       {
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Parameters.AddWithValue("@Para_vcr","Add");
           cmd.Parameters.AddWithValue("@ApplyJobs_Name", name);
           cmd.Parameters.AddWithValue("@ApplyJobs_EmailId", email);
           cmd.Parameters.AddWithValue("@ApplyJobs_Resume", resume);
           cmd.Parameters.AddWithValue("@ApplyJobs_Status", "UnCheck");
           cmd.Parameters.AddWithValue("@ApplyJobs_Date", date);
           if (!objcon.State.Equals(ConnectionState.Open))
           {
               objcon.Open();
           }
           cmd.ExecuteNonQuery();
           check = true;
       }

       // bool check = Class1.InsertUpdate("insert into ApplyJobsTB values('" + name + "','" + email + "','" + resume + "','UnCheck','" + date + "')");
        if (extensionI1 != "")
        {
            FileUpload1.SaveAs(Server.MapPath("Addgel/DocFile/" + resume + ""));
        }
        if (check)
        {
            Response.Redirect("apply_confirm.aspx");
        }
        
    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class FreeInkRollerSeries : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Seriesbind();
        }
    }
    private void Seriesbind()
    {
        DataTable dt = Class1.GetRecord("select * from PenSeriesTB where PSeries_Id=7");
        lblball.Text = dt.Rows[0]["PSeries_Head"].ToString();
        lbldescription.Text = dt.Rows[0]["PSeries_Description"].ToString();
        if (dt.Rows[0]["PSeries_BannerImage"].ToString() != "")
        {
            bannerimg.Src = "Addgel/adminImage/" + dt.Rows[0]["PSeries_BannerImage"].ToString();
        }
        else
        {

        }

        if (dt.Rows[0]["PSeries_BannerFooterImage"].ToString() != "")
        {
            bannerfooterimg.Src = "Addgel/adminImage/" + dt.Rows[0]["PSeries_BannerFooterImage"].ToString();
        }
        else
        {

        }

        if (dt.Rows[0]["PSeries_LogoImage"].ToString() != "")
        {
            logoimg.Src = "Addgel/adminImage/" + dt.Rows[0]["PSeries_LogoImage"].ToString();
        }
        else
        {
            logoimg.Visible = false;
        }
    }
}

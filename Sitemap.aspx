﻿<%@ Page Language="C#" MasterPageFile="~/main1.master" AutoEventWireup="true" CodeFile="Sitemap.aspx.cs" Inherits="Sitemap" Title="Add Gel - SiteMap" %>

<%@ Register src="about.ascx" tagname="about" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="1000px" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
      <tr>
        <td align="left" valign="top"><img src="images/spacer.gif" width="10" height="10" /></td>
      </tr>
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="15" align="left" valign="top"><img src="images/spacer.gif" width="15" height="1" /></td>
                 
                  <td  align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="23%" align="left" valign="top">
                      
                      
                          <uc1:about ID="about1" runat="server" />
                      
                      
                      </td>
                      <td width="77%" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1%" align="left" valign="top">&nbsp;</td>
                          <td width="99%" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="right" valign="top"><table width="716" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif"><table width="716" border="0" align="right" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td align="left" valign="top" class="sitemap"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td height="45" align="left" valign="top">&nbsp;</td>
                                          </tr>
                                          <tr>
                                            <td align="left" valign="top"><table width="684" border="0" align="center" cellpadding="0" cellspacing="0">
                                                <tr>
                                                  <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                      <td align="left" valign="top"><img src="images/sitemap_banner.jpg" width="684" height="151" /></td>
                                                    </tr>
                                                  </table></td>
                                                </tr>
                                                
                                            </table></td>
                                          </tr>
                                      </table></td>
                                    </tr>
                                    
                                    
                                  </table></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif">&nbsp;</td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif">
                                  
                                  
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="2%" align="left" valign="top"><img id="Img1" alt="" runat="server" src="images/middle_line.gif" width="2" height="425" /></td>
                        <td width="95%" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                           <tr>
                                      <td align="left" valign="top" bgcolor="#f7f5f6" class="textone"><strong>Sitemap</strong></td>
                                      </tr>
                          
                          <tr>
                            <td align="left" valign="top">
                            <div id="treecontrol" align="right">
		<a title="Collapse the entire tree below" href="#" class="side_links1"> <b>Collapse All</b></a> | 
		<a title="Expand the entire tree below" href="#" class="side_links1"> <b>Expand All</b></a> 
		
	</div>

<ul id="browser" class="filetree">
		<li><span class="file"> <a href="index.aspx">&nbsp;Home&nbsp;</a></span>		</li>
		 <li><span class="folder"> <a href="#">&nbsp;About Us&nbsp;</a></span>	
          <ul>
				<li><span class="file"><a href="aboutus.aspx">Quality Quest</a></span></li>
                <li><span class="file"><a href="technology_first.aspx">Technology First</a></span></li>
                <li><span class="file"><a href="winning.aspx">Winning By Design</a></span></li>
                <li><span class="file"><a href="attractive_prices.aspx">Attractive Prices</a></span></li>
                <li><span class="file"><a href="detailed.aspx">Detailed To Perfection</a></span></li>
                <li><span class="file"><a href="reaching.aspx">Reaching Out</a></span></li>
                			</ul>	
                            </li>
                            
             <li class="closed"><span class="file"><a href="history.aspx">&nbsp;History&nbsp;</a></span>		</li>               
              <li class="closed"><span class="file"><a href="Infrastructure.aspx">&nbsp;Infrastructure&nbsp;</a></span>		</li> 
               <li class="closed"><span class="file"><a href="mission.aspx">&nbsp;Mission Statement&nbsp;</a></span>		</li> 
                <li class="closed"><span class="file"><a href="awards.aspx">&nbsp;Awards/Certification&nbsp;</a></span>		</li> 
                 <li class="closed"><span class="file"><a href="history.aspx">&nbsp;Quality/R & N &nbsp;</a></span>		</li>                
                            
		<li><span class="folder"><a href="#">&nbsp;Products&nbsp;</a></span>
			<ul>
			
			
			<li><span class="folder"><a href="#">&nbsp;Pen Series&nbsp;</a></span>
					<ul id="folder21">
						<li><span class="folder"><a href="product_ballroller1.aspx">&nbsp;Ball Roller Series&nbsp;</a></span>
					<ul id="Ul1">
						<li><span class="file"> <a href="product_ballrollerdetails.aspx?id=1">Eins</a></span></li>                        
					</ul>
				</li>
				
				
				<li><span class="folder"><a href="product_exclusive1.aspx">&nbsp;Exclusive Series &nbsp;</a></span>
					<ul id="Ul4">
						<li><span class="file"> <a href="product_exclusivdetails.aspx?id=2">Carbon-X</a></span></li>
                        <li><span class="file"> <a href="product_exclusivdetails.aspx?id=5">Highness</a></span></li>
						<li><span class="file"> <a href="product_exclusivdetails.aspx?id=18">Ivy </a></span></li>
                        <li><span class="file">  <a href="product_exclusivdetails.aspx?id=19">Maestro</a></span></li>
					</ul>
				</li>
				
				<li><span class="folder"><a href="#">&nbsp;Gel Pen Series &nbsp;</a></span>
					<ul id="Ul5">
						<li><span class="file"> <a href="product_geldetails.aspx?id=3">Achiever</a></span></li>
                        <li><span class="file"> <a href="product_geldetails.aspx?id=28">Gypsy</a></span></li>
						<li><span class="file"> <a href="product_geldetails.aspx?id=27">Cat</a></span></li>
                        <li><span class="file"> <a href="product_geldetails.aspx?id=29">PG-300</a></span></li>
                        <li><span class="file"> <a href="product_geldetails.aspx?id=30">PG-500</a></span></li>
						<li><span class="file"> <a href="product_geldetails.aspx?id=31">T-Jell</a></span></li>
					</ul>
				</li>
				
				<li><span class="folder"><a href="#">&nbsp;Gel Roller Pen Series &nbsp;</a></span>
					<ul id="Ul6">
						<li><span class="file"> <a href="product_gelrolloverdetails.aspx?id=21">Classic</a></span></li>
                        <li><span class="file"> <a href="product_gelrolloverdetails.aspx?id=22">Enis Gel Roller</a></span></li>
						<li><span class="file"> <a href="product_gelrolloverdetails.aspx?id=23">Diamond Roller </a></span></li>
                        <li><span class="file">  <a href="product_gelrolloverdetails.aspx?id=25">Pro-Tech</a></span></li>
					</ul>
				</li>
				
				<li><span class="folder"><a href="#">&nbsp;Liquid Fluid Pen Series &nbsp;</a></span>
					<ul id="Ul7">
						<li><span class="file"> <a href="product_liquiddetail.aspx?id=6">2K</a></span></li>
                        <li><span class="file"> <a href="product_liquiddetail.aspx?id=32">2K-Pro</a></span></li>
						<li><span class="file"> <a href="product_liquiddetail.aspx?id=33">Atom </a></span></li>
                        <li><span class="file">  <a href="product_liquiddetail.aspx?id=34">Puzzle</a></span></li>
                        <li><span class="file"> <a href="product_liquiddetail.aspx?id=36">Silver Sand</a></span></li>
						<li><span class="file"> <a href="product_liquiddetail.aspx?id=45">Silver Line </a></span></li>
                        <li><span class="file">  <a href="product_liquiddetail.aspx?id=37">T-Ball</a></span></li>
					</ul>
				</li>
				
				<li><span class="folder"><a href="#">&nbsp;Premium Stationery Series &nbsp;</a></span>
					<ul id="Ul8">
						<li><span class="file"> <a href="product_premiumdeatils.aspx?id=38">White Lighter</a></span></li>
                        <li><span class="file"> <a href="product_premiumdeatils.aspx?id=39">White Board Marker</a></span></li>
						<li><span class="file"> <a href="product_premiumdeatils.aspx?id=40">Parmanent Marker</a></span></li>
                        <li><span class="file">  <a href="product_premiumdeatils.aspx?id=41">Photopen</a></span></li>
                         <li><span class="file"> <a href="product_premiumdeatils.aspx?id=42">Kidz Sketch Pen</a></span></li>
						<li><span class="file"> <a href="product_premiumdeatils.aspx?id=43">Tech Pencil</a></span></li>
                        <li><span class="file">  <a href="product_premiumdeatils.aspx?id=44">White Ink Pen</a></span></li>
					</ul>
				</li>
					</ul>
				</li>
			<li><span class="folder"><a href="#">&nbsp;Refill Series&nbsp;</a></span>
					<ul id="Ul2">
						<li><span class="folder"><a href="#">&nbsp;Ball Roller Refill &nbsp;</a></span>
					<ul id="Ul3">
						<li><span class="file"> <a href="product_ballrollerrefilldetails.aspx?id=1">Ball Roller</a></span></li>
                       
					</ul>
				</li>
				
				<li><span class="folder"><a href="#">&nbsp;Exclusive Refill &nbsp;</a></span>
					<ul id="Ul9">
						<li><span class="file"> <a href="product_exclusivrefilldetails.aspx?id=2">Exclusive</a></span></li>
                       
					</ul>
				</li>
				
				<li><span class="folder"><a href="#">&nbsp;Gel Refill &nbsp;</a></span>
					<ul id="Ul10">
						<li><span class="file"> <a href="product_gelrefilldetails.aspx?id=3">NB R-20</a></span></li>
                        <li><span class="file"> <a href="product_gelrefilldetails.aspx?id=12">PG R-30</a></span></li>
					</ul>
				</li>
				
				<li><span class="folder"><a href="#">&nbsp;Gel Roller Refill &nbsp;</a></span>
					<ul id="Ul11">
						<li><span class="file"> <a href="product_gelrolloverrefilldetails.aspx?id=8">Gel Roller</a></span></li>
					</ul>
				</li>
				
				<li><span class="folder"><a href="#">&nbsp;Liquid Fluid Refill &nbsp;</a></span>
					<ul id="Ul12">
						<li><span class="file"> <a href="product_liquidrefilldetail.aspx?id=4">Jumbo</a></span></li>
                        <li><span class="file"> <a href="product_liquidrefilldetail.aspx?id=14">PB R-70</a></span></li>
						<li><span class="file"> <a href="product_liquidrefilldetail.aspx?id=15">T-Ball </a></span></li>
					</ul>
				</li>
					</ul>
				</li>	
			
			</ul>
		</li>
				
		
		<li><span class="folder"><a href="#">&nbsp;Downloads&nbsp;</a></span>
			<ul>
			
			
			<li><span class="folder"><a href="#">&nbsp;Promotion&nbsp;</a></span>
				<ul>
						<li><span class="file"><a href="VideoGallery.aspx">&nbsp;TV Advertisement&nbsp;</a></span></li>
					
						<li><span class="file"> <a href="press.aspx">Press Advertisement</a></span></li>                        
					</ul>
				</li>
				
				<li><span class="file"><a href="wallpaper.aspx">&nbsp;Wallpaper&nbsp;</a></span></li>
				
		</ul>
		</li>
		
		
		
		<li class="closed"><span class="file"><a href="institutional_sales.aspx">&nbsp;Institutional Sales&nbsp;</a></span>		</li>
        <li class="closed"><span class="file"><a href="international_division.aspx">&nbsp;Internaional Division&nbsp;</a></span></li>
        <li class="closed"><span class="file"><a href="careers.aspx">&nbsp;Carrers&nbsp;</a></span></li>			
       <li class="closed"><span class="file"><a href="contactus.aspx">&nbsp;Contact Us</a></span>		</li>
	</ul></td>
                          </tr>
                          
                          
                          
                          
                          
                        </table></td>
                      </tr>
                    </table>
                                  </td>
                                </tr>
                                
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif" class="disbot">&nbsp;</td>
                                </tr>
                                
                              </table></td>
                            </tr>
                          </table></td>
                        </tr>
                      </table></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top">&nbsp;</td>
                      <td align="left" valign="top">&nbsp;</td>
                    </tr>
                  </table></td>
                  <td width="12"  align="right" valign="top"><img src="images/spacer.gif" width="15" height="1" /></td>
                </tr>
                
          </table></td>
      </tr>
    </table>
</asp:Content>


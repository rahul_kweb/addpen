﻿<%@ Page Language="C#" MasterPageFile="~/main1.master" AutoEventWireup="true" CodeFile="Sitemap1.aspx.cs" Inherits="aboutus" Title="ADD Gel - About Us" %>

<%@ Register src="about.ascx" tagname="about" tagprefix="uc1" %>

<%@ Register src="QualityQuest.ascx" tagname="QualityQuest" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="1000px" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
      <tr>
        <td align="left" valign="top"><img src="images/spacer.gif" width="10" height="10" /></td>
      </tr>
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="15" align="left" valign="top"><img src="images/spacer.gif" width="15" height="1" /></td>
                 
                  <td  align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="23%" align="left" valign="top">
                      
                      
                          <uc1:about ID="about1" runat="server" />
                      
                      
                      </td>
                      <td width="77%" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1%" align="left" valign="top">&nbsp;</td>
                          <td width="99%" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="right" valign="top"><table width="716" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif"><table width="716" border="0" align="right" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td align="left" valign="top" class="Sitemap12"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td height="45" align="left" valign="top">&nbsp;</td>
                                          </tr>
                                          <tr>
                                            <td align="left" valign="top"><table width="684" border="0" align="center" cellpadding="0" cellspacing="0">
                                                <tr>
                                                  <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                      <td align="left" valign="top"><img src="images/quility.jpg" width="684" height="151" /></td>
                                                    </tr>
                                                  </table></td>
                                                </tr>
                                                
                                            </table></td>
                                          </tr>
                                      </table></td>
                                    </tr>
                                    
                                    
                                  </table></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif">&nbsp;</td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif">
                                  <table width="684" border="0" align="center" cellpadding="2" cellspacing="2">
                                    <tr>
                                      <td align="left" valign="top" bgcolor="#f7f5f6" class="textone"><strong>Sitemap</strong></td>
                                      </tr>
                                    
                                    <tr>
                                      <td align="left" valign="top" class="textone"><table width="100%" border="0" cellspacing="1" cellpadding="1">
                                        <tr>
                                          <td width="2%" align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td width="98%" align="left" valign="top" class="sitemap"><strong>About Us</strong></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="aboutus.aspx"> Quality Quest</a> </td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="technology_first.aspx">Technology First</a> </td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="winning.aspx">Winning By Design</a> </td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="attractive_prices.aspx">Attractive Prices</a> </td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="detailed.aspx">Detailed To Perfection</a> </td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="reaching.aspx">Reaching Out</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="history.aspx">History </a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="Infrastructure.aspx">Infrastructure</a> </td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="mission.aspx">Mission Statement</a> </td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="awards.aspx">Awards/Certification</a> </td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="quilityrd.aspx">Quality/R &amp; D </a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="history.aspx"><strong>History</strong></a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="Infrastructure.aspx"><strong>Infrastructure</strong></a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="mission.aspx"><strong>Mission Statement</strong></a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="awards.aspx"><strong>Awards/Certification</strong></a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="quilityrd.aspx"><strong>Quality/R &amp; D</strong></a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><strong> Products</strong></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_ballroller1.aspx"><strong>Ball Roller Series</strong></a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_ballrollerdetails.aspx?id=1">Eins</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_gelpen1.aspx"><strong>Gel Pen Series</strong></a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_geldetails.aspx?id=3">Achiever</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">Cat</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">Gypsy</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">PG-300</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">PG-500</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_geldetails.aspx?id=12">T-Gel</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_exclusive1.aspx"><strong>Exclusives Series</strong></a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_exclusivdetails.aspx?id=2">Carbon-x</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_exclusivdetails.aspx?id=5">Highness</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">IVY</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">Meastro</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_gelrolloverpen1.aspx"><strong>Gel Roller Pens Series</strong></a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_gelrolloverdetails.aspx?id=4">Classic</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">Diamond Roller</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">Eins</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">Pro-Tech</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><strong><a href="product_liquid1.aspx">Liquid Fluid Series</strong></a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_liquiddetail.aspx?id=6">2K</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">2K Pro</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">Atom</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">Puzzle</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">Silver Line</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">Silver Sand</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">T-Ball</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_premium1.aspx"><strong>Premium Stationery Series</strong></a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_premiumdeatils.aspx?id=7">Photopen</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_premiumdeatils.aspx?id=9">Colours Sketch Pen</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">Highlighter</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">Permanent Marker</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">Tree SaverMeanical Pencils</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">White Ink Pen</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">Whiteboard</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_ballrollerrefill1.aspx"><strong>Ball Roller  Refills</strong></a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_ballrollerrefilldetails.aspx?id=1">Ball Roller</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_exclusiverefill1.aspx"><strong>Exclusives Refills</strong></a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_exclusivrefilldetails.aspx?id=2">Exclusive Refill</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_gelpenrefill1.aspx"><strong>Gel  Refills</strong></a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_gelrefilldetails.aspx?id=3">NB R-20</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">PG R-30.psd</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_gelrolloverrefill1.aspx"><strong>Gel Roller Refills</strong></a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_gelrolloverrefilldetails.aspx?id=8">Gel Roller Refill</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_liquidrefill1.aspx"><strong>Liquid Fluid Refills</strong></a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="product_liquidrefilldetail.aspx?id=4">Jumbo Refil</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#"><strong>Downloads</strong></a> </td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">Promotions</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="video.aspx">T V Advertisement</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">Press Advertisement</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">PSO</a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet12.gif" width="7" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="#">Wallpapers</a></td>
                                        </tr>
                                        
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="institutional_sales.aspx"><strong> Institutional Sales </strong></a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="international_division.aspx"><strong>International Divison </strong></a></td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="middle"><img src="images/bullet.gif" width="8" height="8" /></td>
                                          <td align="left" valign="top" class="sitemap"><a href="contactus.aspx"><strong>Contact Us </strong></a></td>
                                        </tr>
                                        
                                      </table></td>
                                    </tr>
                                    <tr>
                                      <td align="left" valign="top" class="textone">&nbsp;</td>
                                    </tr>
                                  </table>
                                  </td>
                                </tr>
                                
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif" class="aboutusbot">&nbsp;</td>
                                </tr>
                                
                              </table></td>
                            </tr>
                          </table></td>
                        </tr>
                      </table></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top">&nbsp;</td>
                      <td align="left" valign="top">&nbsp;</td>
                    </tr>
                  </table></td>
                  <td width="12"  align="right" valign="top"><img src="images/spacer.gif" width="15" height="1" /></td>
                </tr>
                
          </table></td>
      </tr>
    </table>
</asp:Content>


﻿<%@ Page Language="C#" MasterPageFile="~/main1.master" AutoEventWireup="true" CodeFile="product_liquid1.aspx.cs" Inherits="product_liquid1" Title="ADD Gel - Liquid" %>

<%@ Register src="app.ascx" tagname="app" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .softink {
            background-image: url(../images/soft-ink_series.jpg);
            background-repeat: no-repeat;
            background-position: right top;
            height: 192px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="1000px" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
      <tr>
        <td align="left" valign="top"><img src="images/spacer.gif" width="10" height="10" /></td>
      </tr>
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="15" align="left" valign="top"><img src="images/spacer.gif" width="15" height="1" /></td>
                 
                  <td  align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="23%" align="left" valign="top"><table width="226" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td align="left" valign="top"><img src="images/toppr.gif" width="226" height="14" /></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top" background="images/prbg.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="6%" align="left" valign="top">&nbsp;</td>
                              <td width="94%" align="left" valign="top">
                                  <uc1:app ID="app1" runat="server" />
                                </td>
                            </tr>
                          </table>                         </td>
                          </tr>
                        <tr>
                          <td align="left" valign="top"><img src="images/bot.gif" width="226" height="16" /></td>
                        </tr>
                      </table></td>
                      <td width="77%" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1%" align="left" valign="top">&nbsp;</td>
                          <td width="99%" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="right" valign="top">
                               <div id="info">
                               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1%" align="left" valign="top">&nbsp;</td>
                          <td width="99%" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="right" valign="top">
                              <div id="Div1">
                              <table width="716" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif"><table width="716" border="0" align="right" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td align="left" valign="top" class="softink"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td height="45" align="left" valign="top">&nbsp;</td>
                                          </tr>
                                          <tr>
                                            <td align="left" valign="top"><table width="684" border="0" align="center" cellpadding="0" cellspacing="0">
                                                <tr>
                                                  <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                      <td align="left" valign="top"><img id="bannerimg" runat="server" width="684" height="213" /></td>
                                                    </tr>
                                                  </table></td>
                                                </tr>
                                                <tr>
                                                  <td align="left" valign="top"><img id="bannerfooterimg" runat="server" width="684" height="40" /></td>
                                                </tr>
                                            </table></td>
                                          </tr>
                                      </table></td>
                                    </tr>
                                    <tr>
                                      <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td><img id="logoimg" runat="server"  width="189" height="30" /></td>
                                    </tr>
                                  </table></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif">&nbsp;</td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif"><table width="684" border="0" align="center" cellpadding="2" cellspacing="2">
                                    <tr>
                                      <td align="left" valign="top" bgcolor="#f7f5f6" class="textone"><strong>
                                          <asp:label id="lblball" runat="server" text=""></asp:label>
                                      </strong></td>
                                      </tr>
                                    <tr>
                                      <td align="left" valign="top" class="textone"><asp:label id="lbldescription" runat="server" text=""></asp:label></td>
                                    </tr>
                                  </table></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif">&nbsp;</td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif" class="liquidrbot">&nbsp;</td>
                                </tr>
                                
                              </table>
                                  </div>
                              </td>
                            </tr>
                          </table></td>
                        </tr>
                      </table>
                              </div>
                              </td>
                            </tr>
                          </table></td>
                        </tr>
                      </table></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top">&nbsp;</td>
                      <td align="left" valign="top">&nbsp;</td>
                    </tr>
                  </table></td>
                  <td width="12"  align="right" valign="top"><img src="images/spacer.gif" width="15" height="1" /></td>
                </tr>
                
          </table></td>
      </tr>
    </table>
</asp:Content>


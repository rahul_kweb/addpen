﻿<%@ Page Language="C#" MasterPageFile="~/main1.master" AutoEventWireup="true" CodeFile="product_exclusivrefilldetails.aspx.cs" Inherits="product_exclusivrefilldetails" %>

<%@ Register src="app.ascx" tagname="app" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="1000px" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
      <tr>
        <td align="left" valign="top"><img src="images/spacer.gif" width="10" height="10" /></td>
      </tr>
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="15" align="left" valign="top"><img src="images/spacer.gif" width="15" height="1" /></td>
                 
                  <td  align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="23%" align="left" valign="top"><table width="226" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td align="left" valign="top"><img src="images/toppr.gif" width="226" height="14" /></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top" background="images/prbg.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="6%" align="left" valign="top">&nbsp;</td>
                              <td width="94%" align="left" valign="top">
                                  <uc1:app ID="app1" runat="server" />
                                </td>
                            </tr>
                          </table>                         </td>
                          </tr>
                        <tr>
                          <td align="left" valign="top"><img src="images/bot.gif" width="226" height="16" /></td>
                        </tr>
                      </table></td>
                      <td width="77%" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1%" align="left" valign="top">&nbsp;</td>
                          <td width="99%" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="right" valign="top"> <div id="info">
    <table width="716" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif"><table width="716" border="0" align="right" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td align="left" valign="top" class="exclusiverefill">
                                      
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td height="45" align="left" valign="top">&nbsp;</td>
                                          </tr>
                                          <tr>
                                            <td align="left" valign="top"><table width="684" border="0" align="center" cellpadding="0" cellspacing="0">
                                              <tr>
                                                <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                      <td align="left" valign="top"><img id="Zoomimage" runat="server" border="0" width="684" height="322" onclick="return Zoomimage_onclick()" /></td>
                                                    </tr>
                                                </table></td>
                                              </tr>
                                            </table></td>
                                          </tr>
                                          <tr>
                                            <td align="left" valign="top">&nbsp;</td>
                                          </tr>
                                          
                                      </table>
                                      
                                      </td>
                                    </tr>
                                    
                                    <tr>
                                      <td><table width="684" border="0" align="center" cellpadding="0" cellspacing="0">
                                        <tr>                                        
                                        
                                <td align="left" class="content2">
                             
                             <asp:DataList id="DLProduct"  width="150" height="75" 
                                            ItemStyle-VerticalAlign="Top" border="0" CellPadding="1"  CellSpacing="3"  
                                            RepeatColumns="4" RepeatDirection="Horizontal" runat="server"                                         
                                            onitemcommand="datalist_command">         
                                         <ItemTemplate>                             
                        
                                <table id=dltable runat="server" width="100%" border="0" cellpadding="2" cellspacing="2">
                                  <tr>
                                    <td align="center" valign="top">
                                        <asp:LinkButton ID="lnklarge1" CommandName="Large1" runat="server">  
                                   <img id="Img2" alt="" src='Addgel/adminImage/<%#(Eval("Refill_ThumbnailImage"))%>' width="155" height="58" border="0" /> </asp:LinkButton> 
                                        <asp:Label ID="lblzoom1" runat="server" Text='<%#(Eval("Refill_ZoomImage"))%>' Visible="false"></asp:Label>
                                   </td>                                   
                                  </tr>                                
                                </table>
                                   </ItemTemplate>
                                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                                    </asp:DataList>
                            </td>
                              
                                          
                                        </tr>
                                      </table></td>
                                    </tr>
                                  </table></td>
                                </tr>
                                
                                <tr>
                                  <td align="center" valign="top" background="images/probg.gif" class="botexclusive">
                                  <table width="684" border="0" align="center" cellpadding="2" cellspacing="2">
                                    <tr>
                                      <td align="left" valign="top" bgcolor="#f7f5f6" class="textone"><strong>
                                          <asp:Label ID="lblhead" runat="server" Text=""></asp:Label>
                                      </strong></td>
                                      </tr>
                                    <tr>
                                      <td align="left" valign="top" class="textone">
                                          <asp:Label ID="lbldescription" runat="server" Text=""></asp:Label></td>
                                    </tr>
                                    <tr>
                                    <td>
                                  
                                    <br />
                                    </td>
                                    </tr>
                                    <tr>
                                      <td align="left" valign="top" class="textone">
                                          <asp:Label ID="lblfeatures" runat="server" Text=""></asp:Label>
                                   </td>
                                    </tr>
                                    <tr>
                                    <td align="center" valign="top" class="textone">
                                        <asp:ImageButton ID="btntellafriend" runat="server" Width="110px" Height="19px" 
                                            ImageUrl="~/images/tellafriend.gif" onclick="btntellafriend_Click" />
                                        &nbsp;&nbsp;
                                        <asp:ImageButton ID="btnenquiry" runat="server" Width="110px" Height="19px" 
                                            ImageUrl="~/images/enquirenow.gif" onclick="btnenquiry_Click" />
                                        
                                    </td>
                                        
                                    </tr>
                                  </table>
                                  </td>
                                </tr>
                                
                              </table>
    </div></td>
                            </tr>
                          </table></td>
                        </tr>
                      </table></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top">&nbsp;</td>
                      <td align="left" valign="top">&nbsp;</td>
                    </tr>
                  </table></td>
                  <td width="12"  align="right" valign="top"><img src="images/spacer.gif" width="15" height="1" /></td>
                </tr>
                
          </table></td>
      </tr>
    </table>
</asp:Content>





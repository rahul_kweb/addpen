﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="new_slider.aspx.cs" Inherits="new_slider" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    
    <script type="text/javascript" src="js/jquery-1.2.6.pack.js"></script> 

<script type="text/javascript" src="js/stepcarousel.js"></script>
<script type="text/javascript" src="js/stepcarousel.js"></script>
<script type="text/javascript">
stepcarousel.setup({
	galleryid: 'scroll3', //id of carousel DIV
	beltclass: 'belt', //class of inner "belt" DIV containing all the panel DIVs
	panelclass: 'panel', //class of panel DIVs each holding content
	panelbehavior: {speed:500, wraparound:true, persist:false},
	autostep: {enable:true, moveby:1, pause:3000},
	defaultbuttons: {enable: false, moveby: 1},
	statusvars: ['statusA1', 'statusB1', 'statusC1'], //register 3 variables that contain current panel (start), current panel (last), and total panels
	contenttype: ['inline'] //content setting ['inline'] or ['external', 'path_to_external_file']
})

</script><style type="text/css">
#scroll3{overflow: hidden;}
</style>
<style type="text/css">
.mbox{width:373px; height:224px; background:url(/images/newimg/bgban.gif) no-repeat 0 0; padding:10px; float:right}
.r_35t { font:bold 35px/35px "Trebuchet MS", Arial, Helvetica, sans-serif; color:#bc0108; }
.r_11 { font:bold 11px Arial; color:#bc0108; }
.r_14 { font:bold 14px Arial; color:#bc0108; }
.gD_12 { font:12px Arial; color:#555555 }
.gD_15 { font:15px Arial; color:#555555 }
.MT5 { margin-top:5px; }
.MB10 { margin-bottom:10px; }
.scroll_3 { position:relative; overflow:scroll; width:244px; height:145px; }
.scroll_3 .belt { position:absolute; left:0; top:0; }
.scroll_3 .panel { float:left; overflow:hidden; width:244px; margin-right:2px }
</style>
<!--End Best Price-->


<!-- Photo Gallery -->

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery_carousel.js"></script>
 

<style type="text/css">
* {margin:0; padding:0;}
.r_11 { font:bold 11px Arial; color:#bc0108; }
.r_14 { font:bold 14px Arial; color:#bc0108; }
.gD_12 { font:12px Arial; color:#555555 }
.gD_19 { font:19px Arial; color:#515151; text-decoration:none;} 
.gL_12 {font:12px Arial; color:#666666;} .gL_15 {font:15px Arial; color:#666666;}
.MT5 { margin-top:5px; } .MT10 { margin-top:10px; }  .PR5 {padding-right:5px;} .PL5 {padding-left:5px;} .PL10 {padding-left:10px;} .PB5 {padding-bottom:5px;}
.MB10 { margin-bottom:10px; }
.FL {float:left;}
.FR {float:right;}
.CL {clear:both;}


#container .js{overflow:hidden;zoom:1;}
#container .carousel,
#container .carousel .carousel-wrap{margin:0;border:0;zoom:1;}
#container .js .carousel-wrap{width:285px;margin:5px 0px;}
.IE #container .js .carousel-wrap{display:inline;}
#container .carousel ul{margin:0;padding:0;zoom:1;}
#container .js ul li{overflow:hidden;display:inline;height:150px;}
#container .carousel div{margin:0;padding:0;border:0;}
#container .carousel ul{padding:0;list-style:none;}
#container div.center-wrap{overflow:hidden;clear:both;zoom:1;}
#container div.center-wrap div{border:0;margin:5px 0;}
#container div.center-wrap a{margin:0 2px;padding:2px 5px;cursor:pointer;color:#fff;-moz-border-radius:12px;webkit-border-radius:12px;border-radius:12px;}
#container div.center-wrap a.active{font-weight:bold;color:#5db0e6;}

</style>
<!-- Photo Gallery -->




    
    
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div height="150px">
<div class="mbox">
		
			<p class="gD_15 MB10">More Images</p>
			<div style="height:150px">
				<div class="FL" onclick="javascript:stepcarousel.stepBy(&#39;scroll3&#39;, -1);" style="cursor:pointer; width:15px; padding:10px 10px 0 0"><img src="images/ltarw.gif" width="15" height="35" alt=""></div>
				<div id="scroll3" class="FL scroll_3">
					<div class="belt" style="width: 300px;">
									  <asp:Label ID="lblpictures" runat="server" ></asp:Label>			 						
												 						
												 						
								
												 					
											</div>
				</div>
				<div class="FR" onclick="javascript:stepcarousel.stepBy(&#39;scroll3&#39;, 1);" style="cursor:pointer; width:15px; padding:10px 0 0 0"><img src="images/rtarw.gif" width="15" height="35" alt=""></div>
				<div class="CL"></div>
				
					</div>
	
		</div>
				
				
				
			</div>
    </div>
    </form>
</body>
</html>

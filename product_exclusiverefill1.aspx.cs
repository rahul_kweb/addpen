﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class product_exclusiverefill1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Seriesbind();
        }
    }
    private void Seriesbind()
    {
        DataTable dt = Class1.GetRecord("select * from RefillSeriesTB where RSeries_Name='Exclusives Pen Refill'");
        lblball.Text = dt.Rows[0]["RSeries_Head"].ToString();
        lbldescription.Text = dt.Rows[0]["RSeries_Description"].ToString();
        if (dt.Rows[0]["RSeries_BannerImage"].ToString() != "")
        {
            bannerimg.Src = "Addgel/adminImage/" + dt.Rows[0]["RSeries_BannerImage"].ToString();
        }
        else
        {

        }

        if (dt.Rows[0]["RSeries_BannerFooterImage"].ToString() != "")
        {
            bannerfooterimg.Src = "Addgel/adminImage/" + dt.Rows[0]["RSeries_BannerFooterImage"].ToString();
        }
        else
        {

        }

        if (dt.Rows[0]["RSeries_LogoImage"].ToString() != "")
        {
            logoimg.Src = "Addgel/adminImage/" + dt.Rows[0]["RSeries_LogoImage"].ToString();
        }
        else
        {
            logoimg.Visible = false;
        }
    }
}

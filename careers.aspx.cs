﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class careers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindjobs();
        }

    }

    private void bindjobs()
    {
        DataTable dt = Class1.GetRecord("select * from JobsTB where Jobs_Status='Active'");
        DataList1.DataSource = dt;
        DataList1.DataBind();

    }


    protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName.Equals("applyjob"))
        {
            try
            {

                int keyField = Convert.ToInt32(DataList1.DataKeys[e.Item.ItemIndex]);

                string vvjvh = "apply.aspx?id=" + keyField;
                Response.Redirect("apply.aspx?id=" + keyField);

            }

            catch (Exception ex)
            {
               
            }

        }
    }

    protected void addtolist_Click(object sender, ImageClickEventArgs e)
    {

    }
}

<%@ Page Language="C#" MasterPageFile="~/main1.master" EnableViewState="true" AutoEventWireup="true" CodeFile="Search.aspx.cs"
    Inherits="Search" Title="Add Gel - Search" %>

<%@ Register Src="app.ascx" TagName="app" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 1%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="1000px" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF"
        align="center">
        <tr>
            <td align="left" valign="top">
                <img src="images/spacer.gif" width="10" height="10" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="15" align="left" valign="top">
                            <img src="images/spacer.gif" width="15" height="1" />
                        </td>
                        <td align="left" valign="top">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="23%" align="left" valign="top">
                                        <table border="0" cellspacing="0" cellpadding="0" style="width: 226px">
                                            <tr>
                                                <td align="left" valign="top">
                                                    <img src="images/toppr.gif" width="226" height="14" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" background="images/prbg.gif">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td width="6%" align="left" valign="top">
                                                                &nbsp;
                                                            </td>
                                                            <td width="94%" align="left" valign="top">
                                                                <uc1:app ID="app1" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                <tr>
                    <td align="left" valign="top">
                        <img src="images/bot.gif" width="226" height="16" />
                    </td>
                </tr>
            </table>
        </td>
        <td width="77%" align="left" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" class="style1">
                        &nbsp;
                    </td>
                    <td width="99%" align="left" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="right" valign="top">
                                    <div id="info">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
<td width="1%" align="left" valign="top">
&nbsp;
</td>
<td width="99%" align="left" valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="right" valign="top">
    <table width="716" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="left" valign="top" background="images/probg.gif">
                <table width="716" border="0" align="right" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" valign="top" class="search">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td height="45" align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <table width="684" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <img src="images/search_banner.jpg" width="684" height="151" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" background="images/probg.gif">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" background="images/probg.gif">
                <table width="684" border="0" align="center" cellpadding="2" cellspacing="2">
                    <tr>
                        <td align="left" valign="top" bgcolor="#f7f5f6" class="textone">
                            <strong>Search</strong>
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="left" valign="top" class="textone">
                            <asp:Label ID="lblamit" runat="server" Text=""></asp:Label>                                                       
                        </td>
                    </tr>
                    <tr>
                    <td> 
                    <table id=pentab runat="server" Visible=false width="100%">
                      <tr>
                        <td align="center" valign="top" class="textone" bgcolor="Silver"><strong>Pen Result</strong>
                            &nbsp;
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <asp:DataList ID="DataList1" runat="server" Width="100%" DataKeyField="P_Id" OnItemCommand="Item_Command" >
<ItemTemplate>
                                    <table width="99%" border="0" align="right" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="right" valign="top">
                                                <table width="99%" border="0" cellspacing="0" cellpadding="0">
                                                
                                                <tr>
                                            <td>
                                                <img id="Img1" alt="" runat="server" src="images/spacer.gif" width="7" height="12" />
                                            </td>
                                        </tr>
                                                    <tr>
                                                        <td width="20%" align="left" valign="top">
                                                            <asp:Image ID="Image1" runat="server" Width="155" Height="58" border="0" ImageUrl='<%# Bind("P_Thumbnail") %>' />
                                                            <%--<img alt="" runat="server" src="images/spin2.jpg" width="86" height="83" border="0" />--%>
                                                        </td>
                                                        <td width="80%" align="left" valign="top">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="2">
                                                                <tr>
                                                                    <td align="left" valign="top" >
                                                                        <strong>
                                                                            &nbsp;&nbsp;&nbsp;
                                                                            <asp:Label ID="lblName" CssClass="textone"  runat="server" Text='<%# Eval("P_Name") %>'></asp:Label>
                                                                        </strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" >
                                                                        &nbsp;&nbsp;&nbsp;
                                                                        <asp:Label ID="lblhead" CssClass="textone" runat="server" Text='<%# Eval("P_Head") %>'></asp:Label>
                                                                    </td>
                                                                     
                                                              
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" >
                                                                        &nbsp;&nbsp;&nbsp;
                                                                        <asp:Label ID="lbldiscription" CssClass="textone" runat="server" Text='<%# Eval("P_Description").ToString().Length > 70 ? Eval("P_Description").ToString().Substring(0,70) : Eval("P_Description") %>'></asp:Label>
                                                                        <strong>&nbsp;</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                <td align="left" valign="top" >
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:ImageButton 
                                                ID="seedetail" runat="server"  width="65" 
                                                height="22" border="0" onclick="seedetail_Click" CommandName="ProductDetails" 
                                                                            ImageUrl="~/images/see_details.jpg" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img id="Img6" alt="" runat="server" src="images/spacer.gif" width="7" height="12" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top" bgcolor="gray">
                                                <img id="Img71" alt="" runat="server" src="images/spacer.gif" width="600" height="1" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img src="images/spacer.gif" width="7" height="7" />
                                            </td>
                                        </tr>
                                       <%-- <tr>
                                            <td>
                                                <img id="Img1" alt="" runat="server" src="images/dot_line.gif" width="461" height="1" />
                                            </td>
                                        </tr>--%>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                    </table>
                    
                    </td>
                    </tr>
                  
                  <tr>
                  <td>
                  <table id=refilltab runat="server" Visible=false width="100%">
                   <tr>
                        <td align="center" valign="top" class="textone" bgcolor="Silver"><strong>Refill Result</strong>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DataList ID="DataList2"  runat="server" Width="100%" 
                                DataKeyField="R_Id" onitemcommand="DataList2_ItemCommand">
<ItemTemplate>
                                    <table width="99%" border="0" align="right" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="right" valign="top">
                                                <table width="99%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                            <td>
                                                <img id="Img2" alt="" runat="server" src="images/spacer.gif" width="7" height="12" />
                                            </td>
                                        </tr>
                                                    <tr>
                                                        <td width="20%" align="left" valign="top">
                                                            <asp:Image ID="Image1" runat="server" Width="155" Height="58" border="0" ImageUrl='<%# Bind("R_Thumbnail") %>' />
                                                            <%--<img alt="" runat="server" src="images/spin2.jpg" width="86" height="83" border="0" />--%>
                                                        </td>
                                                        <td width="80%" align="left" valign="top">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="2">
                                                                <tr>
                                                                    <td align="left" valign="top" >
                                                                        <strong>  &nbsp;&nbsp;&nbsp;
                                                                            <asp:Label ID="lblName" CssClass="textone" runat="server" Text='<%# Eval("R_Name") %>'></asp:Label>
                                                                        </strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" >
                                                                     &nbsp;&nbsp;&nbsp;     <asp:Label ID="lblhead" CssClass="textone" runat="server" Text='<%# Eval("R_Head") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" >
                                                                     &nbsp;&nbsp;&nbsp;     <asp:Label ID="lbldiscription" CssClass="textone" runat="server" Text='<%# Eval("R_Description").ToString().Length > 70 ? Eval("R_Description").ToString().Substring(0,70) : Eval("R_Description") %>'></asp:Label>
                                                                        <strong>&nbsp;</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                <td align="left">
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                                   <asp:ImageButton 
                                                ID="seedetail" runat="server"  width="65" 
                                                height="22" border="0" onclick="seedetail_Click" CommandName="ProductDetails" 
                                                                        ImageUrl="~/images/see_details.jpg"/>                                                                     
                                                                    
                                                                </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img id="Img6" alt="" runat="server" src="images/spacer.gif" width="7" height="12" />
                                            </td>
                                        </tr>
                                         <tr>
                                            <td align="center" valign="top" bgcolor="gray">
                                                <img id="Img71" alt="" runat="server" src="images/spacer.gif" width="600" height="1" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img src="images/spacer.gif" width="7" height="7" />
                                            </td>
                                        </tr>
                                        <%--<tr>
                                            <td>
                                                <img id="Img1" alt="" runat="server" src="~/images/dot_line.gif" width="461" height="1" />
                                            </td>
                                        </tr>--%>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </td>
            </tr>
                  </table>
                  </td>
                  </tr>
                  
                   
        </table>
    </td>
</tr>
<tr>
    <td align="left" valign="top" background="images/probg.gif" class="aboutusbot">
        &nbsp;
    </td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="left" valign="top">
&nbsp;
</td>
<td align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="12" align="right" valign="top">
                            <img src="images/spacer.gif" width="15" height="1" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

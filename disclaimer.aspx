﻿<%@ Page Language="C#" MasterPageFile="~/main1.master" AutoEventWireup="true" CodeFile="disclaimer.aspx.cs" Inherits="disclaimer" Title="ADD Gel - Disclaimer" %>

<%@ Register src="app.ascx" tagname="app" tagprefix="uc1" %>

<%@ Register src="about.ascx" tagname="about" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="1000px" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF"
        align="center">
        <tr>
            <td align="left" valign="top">
                <img src="images/spacer.gif" width="10" height="10" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="15" align="left" valign="top">
                            <img src="images/spacer.gif" width="15" height="1" />
                        </td>
                        <td align="left" valign="top">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="23%" align="left" valign="top">
                                        <table border="0" cellspacing="0" cellpadding="0" style="width: 226px">
                                            
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td width="6%" align="left" valign="top">
                                                                &nbsp;
                                                            </td>
                                                            <td width="94%" align="left" valign="top">
                                                                <uc2:about ID="about1" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
              
            </table>
        </td>
        <td width="77%" align="left" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" >
                        &nbsp;
                    </td>
                    <td width="99%" align="left" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="right" valign="top">
                                    <div id="info">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
<td width="1%" align="left" valign="top">
&nbsp;
</td>
<td width="99%" align="left" valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="right" valign="top">
    <table width="716" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="left" valign="top" background="images/probg.gif">
                <table width="716" border="0" align="right" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" valign="top" class="wall">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td height="45" align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <table width="684" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <img src="images/disclaimer_banner.jpg" width="684" height="149" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" background="images/probg.gif">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" background="images/probg.gif">
                <table width="684" border="0" align="center" cellpadding="2" cellspacing="2">
                    <tr>
                        <td align="left" valign="top" bgcolor="#f7f5f6" class="textone">
                            <strong>Discalimer</strong>
                        </td>
                    </tr>
                    
                    
                    <tr>
                        <td align="left" valign="top"><img id="Img6" alt="" runat="server" src="~/images/spacer.gif" width="1" height="7" /></td>
                    </tr
                    
                    <tr>
                        <td align="left" valign="top" class="textone">
                            This site is best viewed with an 1280 x 1024 monitor resolution and with Internet Explorer 6.0 and above. </td>
                    </tr>
                    
                    <tr>
                        <td align="left" valign="top"><img alt="" runat="server" src="~/images/spacer.gif" width="1" height="7" /></td>
                    </tr>
                    
                    <tr>
                        <td align="left" valign="top" class="textone">
                            This official site of <a href="http://www.addpens.com/" class="link">www.addpens.com </a> has been developed to provide general public information. The documents and information displayed in this site are for reference purposes only. </td>
                    </tr>
                    
                    <tr>
                        <td align="left" valign="top"><img id="Img1" alt="" runat="server" src="~/images/spacer.gif" width="1" height="7" /></td>
                    </tr
                    
                    <tr>
                        <td align="left" valign="top" class="textone">
                            The content on the site is updated on a continual basis. While ADD Corporation 
                            Ltd. attempts to keep its web information accurate and timely, it neither 
                            guarantees nor endorses the content, accuracy, or completeness of the 
                            information, text, graphics, hyperlinks, and other items contained on this 
                            server or any other server. </td>
                    </tr>
                   
                    <tr>
                        <td align="left" valign="top"><img id="Img2" alt="" runat="server" src="~/images/spacer.gif" width="1" height="7" /></td>
                    </tr
                    
                    <tr>
                        <td align="left" valign="top" class="textone">
                            As a result of updates and corrections, web materials are subject to change 
                            without notice from ADD Corporation Ltd. Commercial use of web materials is 
                            prohibited without the written permission of the company. </td>
                    </tr>
                    
                    
                    <tr>
                        <td align="left" valign="top"><img id="Img3" alt="" runat="server" src="~/images/spacer.gif" width="1" height="7" /></td>
                    </tr
                    
                    <tr>
                        <td align="left" valign="top" class="textone">
                            Product design, graphics are the properties of ADD Corporation Ltd. and fully 
                            protected. The information is based and content has been framed based on the 
                            laws applicable in India . </td>
                    </tr>
                    
                    
                    <tr>
                        <td align="left" valign="top"><img id="Img4" alt="" runat="server" src="~/images/spacer.gif" width="1" height="7" /></td>
                    </tr>
                    
                    <tr>
                        <td align="left" valign="top" class="textone">
                            Some of the hyperlinks contained in this site may lead to resources outside the 
                            site. Information contained in any site linked from this site has not been 
                            reviewed for accuracy or legal sufficiency. ADD Corporation Ltd. is not 
                            responsible for the content of any such external hyperlinks. References to any 
                            external links should not be construed as an endorsement of the links or their 
                            content. </td>
                    </tr>
                    
                     
                    <tr>
                        <td align="left" valign="top"><img id="Img5" alt="" runat="server" src="~/images/spacer.gif" width="1" height="7" /></td>
                    </tr
                    
                    <tr>
                        <td align="left" valign="top" class="textone">
                            ADD Corporation Ltd. is neither responsible nor liable for any viruses or other 
                            contamination of your system, nor for any delays, inaccuracies, errors or 
                            omissions arising out of your use of the site or with respect to the material 
                            contained on the site. The company is not responsible for any special, indirect, 
                            incidental or consequential damages that may arise from the use of, or the 
                            inability to use, the site and/or the materials contained on the site whether 
                            the materials contained on the site are provided by us.</td>
                    </tr>
                    
                   
                    
                    </table>
    </td>
</tr>
<tr>
    <td align="left" valign="top" background="images/probg.gif" class="wallbot">
        &nbsp;
    </td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="left" valign="top">
&nbsp;
</td>
<td align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="12" align="right" valign="top">
                            <img src="images/spacer.gif" width="15" height="1" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>


</asp:Content>


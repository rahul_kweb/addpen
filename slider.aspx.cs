﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class slider : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            bind_photo();
        }
    }


    public void bind_photo()
    {
        string picture = "";
        DataTable dt1 = Class1.GetRecord("select * from ProductImageGalleryTB where ProductId=1");
        // zoomimg = dt1.Rows[0]["Product_ZoomImage"].ToString();
        picture = "<div align='left'>";
        for (int i = 0; dt1.Rows.Count - 1 >= i; i++)
        {

            picture += "   <img src='Addgel/adminImage/" + dt1.Rows[i]["Product_ThumbnailImage"].ToString() + "' alt='' width='244' height='122' />&nbsp;&nbsp; ";
        }

        picture += "</div>"; 

        lblpictures.Text = picture;

    }
}

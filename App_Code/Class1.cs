﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Xml;
using System.Text;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for Class1
/// </summary>
public class Class1
{
	public Class1()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    public static string ConnectionString()
    {
        //return "Data Source=206.183.111.57,1533;Initial Catalog=addpens_1;User ID=addpens_1;Password=Irg~v579";

        return "Data Source=DESKTOP-PSKN0TK;Initial Catalog=addpens_1;Integrated Security=true";

        //return "Data Source=192.168.1.11;Initial Catalog=AddPen_DB;User ID=sa;Password=12345";
        // return "Data Source=k2;Initial Catalog=Addpen;User ID=sa;Password=om";
        //  return "Data Source=addpen.db.5401497.hostedresource.com;Initial Catalog=addpen;User ID=addpen;Password=CorpAdd2010";
    }


    public static int uptwebprofile(int web_id, string desc, string webpostt)
    {
        SqlConnection objcon = new SqlConnection(ConnectionString());
        objcon.Open();
        SqlCommand cmd = new SqlCommand("proc_CMS", objcon);
        cmd.CommandType = CommandType.StoredProcedure;
        try
        {
            cmd.Parameters.Add("@web_id", SqlDbType.Int).Value = web_id;

            cmd.Parameters.Add("@web_Description", SqlDbType.NText).Value = desc;

            cmd.Parameters.Add("@web_post_date", SqlDbType.DateTime).Value = webpostt;

            cmd.Parameters.Add(new SqlParameter("@mode", SqlDbType.VarChar, 40));
            cmd.Parameters["@mode"].Value = "upt";
            cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            objcon.Close();
        }
        return 0;
    }


    public static bool Validation(string name, string password)
    {
        SqlConnection objconn = new SqlConnection(ConnectionString());
        objconn.Open();
        SqlCommand objcomm = new SqlCommand("Select Count(*) from UserTab where UserName='" + name + "' and UserPassword ='" + password + "' ", objconn);

        if ((int)objcomm.ExecuteScalar() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public static void ChangePassword(string name, string CurrentPassword, string Newpassword)
    {
        SqlConnection objconn = new SqlConnection(ConnectionString());
        objconn.Open();
        SqlCommand objcomm = new SqlCommand("Update UserTab set UserPassword ='" + Newpassword + "'  where UserName='" + name + "' and UserPassword ='" + CurrentPassword + "' ", objconn);

        objcomm.ExecuteNonQuery();
        objconn.Close();
        objcomm.Dispose();

    }
    public static bool InsertUpdate(string str)
    {
        SqlConnection objconn = new SqlConnection(ConnectionString());        
        SqlCommand objcomm = new SqlCommand(str, objconn);
        objconn.Open();
        objcomm.ExecuteNonQuery();
        objconn.Close();
        objcomm.Dispose();

        return true;
    }

    public DataSet metadata(string qry)
    {
        SqlConnection con = new SqlConnection(ConnectionString());
        SqlDataAdapter da = new SqlDataAdapter(qry, con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    private static int Categorymaxorderid()
    {
        SqlConnection objcon = new SqlConnection(ConnectionString());
        string strcheck = "Select max(COrderId) from CategoryTB";
        SqlCommand objcom1 = new SqlCommand(strcheck, objcon);
        objcon.Open();
        int maxid = (int)objcom1.ExecuteScalar();
        maxid = maxid + 1;
        objcon.Close();
        objcom1.Dispose();
        return maxid;

    }
    
    public static bool categoryInsert(string fullname,int id)
    {
        int maxorderid = Categorymaxorderid();
        SqlConnection objcon = new SqlConnection(ConnectionString());
        //string strcheck = "Select Count(*) from CategoryTB where C_CategoryName='" + fullname + "' and C_CatType="+id+"";
       
        string str = "insert into CategoryTB values('" + fullname + "',0,"+id+","+id+",'Parent','','','','Active',getdate()," + maxorderid + ")";
        objcon.Open();
        //SqlCommand objcom1 = new SqlCommand(strcheck, objcon);
        //if ((int)objcom1.ExecuteScalar() > 0)
        //{
        //    objcon.Close();
        //    objcom1.Dispose();
        //    return false;
        //}
        //else
        //{
            SqlCommand objcomm = new SqlCommand(str, objcon);
            objcomm.ExecuteNonQuery();
            objcon.Close();
            objcomm.Dispose();
            return true;
        //}
    }
    public static bool InkColorInsert(string fullname,string color)
    {

        SqlConnection objcon = new SqlConnection(ConnectionString());
        string strcheck = "Select Count(*) from InkColorMTB where InkColorM_Name='" + fullname + "'";
        string str = "insert into InkColorMTB values('" + fullname + "','" + color + "','Active')";
        objcon.Open();
        SqlCommand objcom1 = new SqlCommand(strcheck, objcon);
        if ((int)objcom1.ExecuteScalar() > 0)
        {
            objcon.Close();
            objcom1.Dispose();
            return false;
        }
        else
        {
            SqlCommand objcomm = new SqlCommand(str, objcon);

            objcomm.ExecuteNonQuery();
            objcon.Close();
            objcomm.Dispose();
            return true;
        }
    }
    public static DataTable GetRecord(string str)
    {
     
            SqlConnection objconn = new SqlConnection(ConnectionString());
            SqlDataAdapter objdata = new SqlDataAdapter(str, objconn);
            DataTable dt = new DataTable();
            objdata.Fill(dt);
            return dt;
      
       
    }

    public static int MaxProductId()
    {
        SqlConnection objcon = new SqlConnection(ConnectionString());
        string str = "select count(*)  from ProductTB ";

        SqlCommand objcomm = new SqlCommand(str, objcon);
        objcon.Open();

        int id = (int)objcomm.ExecuteScalar();
        if (id == 0)
        {

        }
        else
        {
            string str1 = "select max(P_Id) from ProductTB ";
            SqlCommand objcomm1 = new SqlCommand(str1, objcon);
            id = (int)objcomm1.ExecuteScalar();
            objcomm1.Dispose();
        }
        objcon.Close();
        objcomm.Dispose();
        return id;
    }




    public static int MaxAnnualReportId()
    {
        SqlConnection objcon = new SqlConnection(ConnectionString());
        string str = "select count(*)  from AnnualReportTB ";

        SqlCommand objcomm = new SqlCommand(str, objcon);
        objcon.Open();

        int id = (int)objcomm.ExecuteScalar();
        if (id == 0)
        {

        }
        else
        {
            string str1 = "select max(AR_Id) from AnnualReportTB ";
            SqlCommand objcomm1 = new SqlCommand(str1, objcon);
            id = (int)objcomm1.ExecuteScalar();
            objcomm1.Dispose();
        }
        objcon.Close();
        objcomm.Dispose();
        return id;
    }

    public static int MaxResumeId()
    {
        SqlConnection objcon = new SqlConnection(ConnectionString());
        string str = "select count(*)  from ApplyJobsTB ";

        SqlCommand objcomm = new SqlCommand(str, objcon);
        objcon.Open();

        int id = (int)objcomm.ExecuteScalar();
        if (id == 0)
        {

        }
        else
        {
            string str1 = "select max(ApplyJobs_Id) from ApplyJobsTB ";
            SqlCommand objcomm1 = new SqlCommand(str1, objcon);
            id = (int)objcomm1.ExecuteScalar();
            objcomm1.Dispose();
        }
        objcon.Close();
        objcomm.Dispose();
        return id;
    }


    public static int MaxRefillId()
    {
        SqlConnection objcon = new SqlConnection(ConnectionString());
        string str = "select count(*)  from RefillTB ";

        SqlCommand objcomm = new SqlCommand(str, objcon);
        objcon.Open();

        int id = (int)objcomm.ExecuteScalar();
        if (id == 0)
        {

        }
        else
        {
            string str1 = "select max(R_Id) from RefillTB ";
            SqlCommand objcomm1 = new SqlCommand(str1, objcon);
            id = (int)objcomm1.ExecuteScalar();
            objcomm1.Dispose();
        }
        objcon.Close();
        objcomm.Dispose();
        return id;
    }

    public static int MaxProductSeriesId()
    {
        SqlConnection objcon = new SqlConnection(ConnectionString());
        string str = "select count(*)  from PenSeriesTB ";

        SqlCommand objcomm = new SqlCommand(str, objcon);
        objcon.Open();

        int id = (int)objcomm.ExecuteScalar();
        if (id == 0)
        {
            id = 0;
        }
        else
        {
            string str1 = "select max(PSeries_Id) from PenSeriesTB ";
            SqlCommand objcomm1 = new SqlCommand(str1, objcon);
            id = (int)objcomm1.ExecuteScalar();
            objcomm1.Dispose();
        }
        objcon.Close();
        objcomm.Dispose();
        return id;
    }

    public static int MaxRefillSeriesId()
    {
        SqlConnection objcon = new SqlConnection(ConnectionString());
        string str = "select count(*)  from RefillSeriesTB ";

        SqlCommand objcomm = new SqlCommand(str, objcon);
        objcon.Open();

        int id = (int)objcomm.ExecuteScalar();
        if (id == 0)
        {
            id = 0;
        }
        else
        {
            string str1 = "select max(RSeries_Id) from RefillSeriesTB ";
            SqlCommand objcomm1 = new SqlCommand(str1, objcon);
            id = (int)objcomm1.ExecuteScalar();
            objcomm1.Dispose();
        }
        objcon.Close();
        objcomm.Dispose();
        return id;
    }

    public static int MaxVideoId()
    {
        SqlConnection objcon = new SqlConnection(ConnectionString());
        string str = "select count(*)  from VideoTB ";

        SqlCommand objcomm = new SqlCommand(str, objcon);
        objcon.Open();

        int id = (int)objcomm.ExecuteScalar();
        if (id == 0)
        {

        }
        else
        {
            string str1 = "select max(Id) from VideoTB ";
            SqlCommand objcomm1 = new SqlCommand(str1, objcon);
            id = (int)objcomm1.ExecuteScalar();
            objcomm1.Dispose();
        }
        objcon.Close();
        objcomm.Dispose();
        return id;
    }
    public static int MaxGiftingIdeasId()
    {
        SqlConnection objcon = new SqlConnection(ConnectionString());
        string str = "select count(*)  from GiftingIdeasTB ";

        SqlCommand objcomm = new SqlCommand(str, objcon);
        objcon.Open();

        int id = (int)objcomm.ExecuteScalar();
        if (id == 0)
        {

        }
        else
        {
            string str1 = "select max(GI_id) from GiftingIdeasTB ";
            SqlCommand objcomm1 = new SqlCommand(str1, objcon);
            id = (int)objcomm1.ExecuteScalar();
            objcomm1.Dispose();
        }
        objcon.Close();
        objcomm.Dispose();
        return id;
    }

    private static int Categorymaxorderid( string str)
    {
        SqlConnection objcon = new SqlConnection(ConnectionString());  
        SqlCommand objcom1 = new SqlCommand(str, objcon);
        objcon.Open();
        int maxid = (int)objcom1.ExecuteScalar();
        maxid = maxid + 1;
        objcon.Close();
        objcom1.Dispose();
        return maxid;

    }

    public static string IncludeCall(string PageName)
    {

        WebClient MyWebClient = new WebClient();

        //Read web page HTML to byte array
        Byte[] PageHTMLBytes;

        //PageHTMLBytes = MyWebClient.DownloadData("http://accutest.kwebmakerusa.com/accutest/includes/" + PageName);
        PageHTMLBytes = MyWebClient.DownloadData("C:/Inetpub/wwwroot/motwane/includes/" + PageName);
        //PageHTMLBytes = MyWebClient.DownloadData(Server.MapPath("../includes/" + name));
        //  Convert result from byte array to string
        //and display it in TextBox txtPageHTML
        UTF8Encoding oUTF8 = new UTF8Encoding();
        string input = oUTF8.GetString(PageHTMLBytes);
        return input;


    }
    public bool IsNumeric(string strToCheck)
    {
        return Regex.IsMatch(strToCheck, "^\\d+(\\.\\d+)?$");
    }
   

}

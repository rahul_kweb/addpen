﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class enquiry : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        tbid1.Visible = true;
      
        if (!IsPostBack)
        {
            //if (Request.QueryString["pid"] != "" && Request.QueryString["cid"] != "")
            //{
            //    if (c.IsNumeric(Request.QueryString["cid"]) && c.IsNumeric(Request.QueryString["pid"]))
            //    {                   
            //        int pid = Convert.ToInt32(Request.QueryString["pid"]);
            //        int cid = Convert.ToInt32(Request.QueryString["cid"]);
            //    }
            //}          

            lblenquiry.Text = "Enquiry";
       
        }
    }
    protected void btnsubmit_Click(object sender, ImageClickEventArgs e)
    {
        SqlConnection objcon = new SqlConnection(Class1.ConnectionString());
        bool check = false;
        string name = txtname.Text.Replace("'", "''");
        string address = txtaddress.Text.Replace("'", "''");
        string email = txtemail.Text.Replace("'", "''");
        string contact = txtcontact.Text.Replace("'", "''");
        string comments = txtcomments.Text.Replace("'", "''");
        string date = DateTime.Today.ToShortDateString();
        int CategoryId = 0;
        string CategoryName = "";

        if (Request.QueryString["IDid"] == "1")
        {
            CategoryId = 111111;
            CategoryName = "International Division";
            check = true;
        }
        else if (Request.QueryString["ISid"] == "1")
        {
            CategoryId = 222222;
            CategoryName = "Institutional Sales";
            check = true;
        }
        else if (Request.QueryString["pid"] != "" && Request.QueryString["cid"] != "")
        {
            CategoryId = Convert.ToInt32(RBLEnquiryType.SelectedValue);
            CategoryName = RBLEnquiryType.SelectedItem.Text;
            check = true;
        }

        if (check)
        {
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetEnquiryTB", objcon))
            {
                check = false;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Add");
                cmd.Parameters.AddWithValue("@E_CategoryId", CategoryId);
                cmd.Parameters.AddWithValue("@E_CategoryName", CategoryName);
                cmd.Parameters.AddWithValue("@E_Name", name);
                cmd.Parameters.AddWithValue("@E_Address", address);
                cmd.Parameters.AddWithValue("@E_EmailId", email);
                cmd.Parameters.AddWithValue("@E_Contact", contact);
                cmd.Parameters.AddWithValue("@E_Comment", comments);
                cmd.Parameters.AddWithValue("@E_Date", date);
                if (!objcon.State.Equals(ConnectionState.Open))
                {
                    objcon.Open();
                }
                cmd.ExecuteNonQuery();
                check = true;
            }
        }
            //  bool check = Class1.InsertUpdate("insert into EnquiryTB values(111111,'International Division','" + name + "','" + address + "','" + email + "','" + contact + "','" + comments + "','" + date + "')");
        if (check)
        {
            tbid1.Visible = false;
            tbid2.Visible = true;
            lblsubmit.Text = "Your " + lblenquiry.Text + " has been sent sucessfully";
        }
    }
    protected void RBLEnquiryType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(RBLEnquiryType.SelectedValue) == 1)
        {
            lblenquiry.Text = "Enquiry";
        }
        else if (Convert.ToInt32(RBLEnquiryType.SelectedValue) == 2)
        {
            lblenquiry.Text = "Suggestion";
        }
        else if (Convert.ToInt32(RBLEnquiryType.SelectedValue) == 3)
        {
            lblenquiry.Text = "Complaint";
        }
    }
}

﻿<%@ Page Language="C#" MasterPageFile="~/main1.master" AutoEventWireup="true" CodeFile="privacy.aspx.cs" Inherits="privacy" Title="ADD Gel - Privacy Policy" %>

<%@ Register src="app.ascx" tagname="app" tagprefix="uc1" %>

<%@ Register src="about.ascx" tagname="about" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <table width="1000px" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF"
        align="center">
        <tr>
            <td align="left" valign="top">
                <img src="images/spacer.gif" width="10" height="10" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="15" align="left" valign="top">
                            <img src="images/spacer.gif" width="15" height="1" />
                        </td>
                        <td align="left" valign="top">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="23%" align="left" valign="top">
                                        <table border="0" cellspacing="0" cellpadding="0" style="width: 226px">
                                           
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td width="6%" align="left" valign="top">
                                                                &nbsp;
                                                            </td>
                                                            <td width="94%" align="left" valign="top">
                                                                <uc2:about ID="about1" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                
            </table>
        </td>
        <td width="77%" align="left" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" class="style1">
                        &nbsp;
                    </td>
                    <td width="99%" align="left" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="right" valign="top">
                                    <div id="info">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
<td width="1%" align="left" valign="top">
&nbsp;
</td>
<td width="99%" align="left" valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="right" valign="top">
    <table width="716" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="left" valign="top" background="images/probg.gif">
                <table width="716" border="0" align="right" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" valign="top" class="privacy">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td height="45" align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <table width="684" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <img src="images/privacy_banner.jpg" width="684" height="149" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" background="images/probg.gif">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" background="images/probg.gif">
                <table width="684" border="0" align="center" cellpadding="2" cellspacing="2">
                    <tr>
                        <td align="left" valign="top" bgcolor="#f7f5f6" class="textone">
                            <strong>Privacy Policy</strong>
                        </td>
                    </tr>
                    
                    <tr>
                        <td align="left" valign="top" class="textone">
                            <table cellspacing="0" cellpadding="0">
  <tr valign="top" align="left">
    <td colspan="2"><p align="justify">We track two types of information about our users:   personal information that you voluntarily provide to us (&quot;Personally   Identifiable Data&quot;), and information about our users' usage of the system   (&quot;Usage Data&quot;). </p></td>
  </tr>
  <tr valign="top" align="left">
    <td align="middle"><img src="images/spacer.gif" width="10" height="10" /></td>
    <td><img src="images/spacer.gif" width="10" height="10" /></td>
  </tr>
  <tr valign="top" align="left">
    <td align="left">•</td>
    <td><div align="justify">Personally Identifiable Data includes such information as   your name, your e-mail address, your mailing address, and your phone number. For   example, in connection with communications with us, you may voluntarily provide   us your email address or we may ask you for your email address. In the future,   we may ask you for certain additional information. The purpose of these requests   is to enable us to respond to your requests, to communicate with you, to support   or enhance your relationship with us, and improve our services. <br />
            <br />
      Personally Identifiable Data, which you provide to us, will be kept   confidential. We will not sell or transfer your Personally Identifiable Data to   third parties for purposes of development and marketing of products or services   to you. Agents and contractors of ours who are given access to your Personally   Identifiable Data are required to keep this information confidential, and not to   use it for any other purpose other than to carry out the services they are   providing for us. We may enhance or merge your information collected at this   site with data from third parties for purposes of development and marketing   products or services to you. <br />
      <br />
      Circumstances may arise when we are   required to disclose your Personally Identifiable Data to third parties for   purposes other than to support or enhance your relationship with us, such as in   a corporate divestiture or dissolution or in the unlikely event we sell all or a   portion of our business or assets (including our lists containing your personal   information), or if disclosure is required by law or is pertinent to judicial or   governmental investigations or proceedings. </div></td>
  </tr>
  <tr valign="top" align="left">
    <td align="middle"><img src="images/spacer.gif" width="10" height="10" /></td>
    <td><img src="images/spacer.gif" width="10" height="10" /></td>
  </tr>
  <tr valign="top" align="left">
    <td width="2%" align="left">•</td>
    <td width="98%">Usage Data includes any non-personally identifiable   data relating to your use of our services, which we may receive, and store when   you interact with us. This may include, but is not limited to, the frequency of   visitors to our Web site and its individual pages. Like many Web sites, we use   &quot;cookies,&quot; and obtain certain information when a user's Web browser accesses our   Web site. &quot;Cookies&quot; are alphanumeric identifiers that we transfer to a user's   computer's hard drive through the user's Web browser to enable the user to   communicate and interact with our Web site. Usage Data, which we may track about   our users, may be used internally or shared with third parties on an aggregate   basis for development and marketing of our products or services. This data   contains no personal information and cannot be used to gather such information.</td>
  </tr>
  <tr valign="top" align="left">
    <td colspan="2"><img src="images/spacer.gif" width="10" height="10" /></td>
  </tr>
  <tr valign="top" align="left">
    <td colspan="2">Questions regarding this Privacy Notice should be directed to <a href="mailto:info@addpens.com" class="link">info@addpens.com</a>
        <div align="justify">Please review our Disclaimer , which also govern your visit   to our website.</div></td>
  </tr>
</table></td>
                    </tr>
                    
        </table>
    </td>
</tr>
<tr>
    <td align="left" valign="top" background="images/probg.gif" class="aboutusbot">
        &nbsp;
    </td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="left" valign="top">
&nbsp;
</td>
<td align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="12" align="right" valign="top">
                            <img src="images/spacer.gif" width="15" height="1" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>


</asp:Content>


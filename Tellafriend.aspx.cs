﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;
public partial class Tellafriend : System.Web.UI.Page
{
    Class1 c = new Class1();

    protected void Page_Load(object sender, EventArgs e)
    {
        tbid1.Visible = true;
        bool sendBack = true;
        if (!IsPostBack)
        {
            if (Request.QueryString["cid"] != null && Request.QueryString["pid"] != null)
            {

                if (c.IsNumeric(Request.QueryString["cid"]) && c.IsNumeric(Request.QueryString["pid"]))
                {
                    sendBack = false;
                    int b = Convert.ToInt32(Request.QueryString["cid"]);
                    int a = Convert.ToInt32(Request.QueryString["pid"]);

                    if (Convert.ToInt32(Request.QueryString["cid"]) != 0 && Convert.ToInt32(Request.QueryString["pid"]) != 0)
                    {
                        int pid = Convert.ToInt32(Request.QueryString["pid"]);
                        int cid = Convert.ToInt32(Request.QueryString["cid"]);
                    }
                }
            }
            if (sendBack)
            {
                Response.Redirect("index.aspx");
            }            
        }
    }
    protected void btnsubmit_Click(object sender, ImageClickEventArgs e)
    {
        string yname = txtyname.Text.Replace("'", "''");
        string yemail = txtyemailid.Text.Replace("'", "''");
        string frndname = txtfrndname.Text.Replace("'", "''");
        string frndemail = txtfrndemail.Text.Replace("'", "''");


      //  bool check = Class1.InsertUpdate("insert into EnquiryTB values(" + Convert.ToInt32(RBLEnquiryType.SelectedValue) + ",'" + RBLEnquiryType.SelectedItem.Text + "','" + name + "','" + address + "','" + email + "','" + contact + "','" + comments + "','" + date + "')");
        //if (check)
        //{ 
            tbid1.Visible = false;
            tbid2.Visible = true;

            string bodymail = "";

            if (Convert.ToInt32(Request.QueryString["cid"]) == 3)
            {
                bodymail = "Your Friend " + yname + " has sent you a Addgel pen URL<br> <a href=http://www.addpens.com/product_ballrollerdetails.aspx?id=" + Convert.ToInt32(Request.QueryString["pid"]) + ">Click Here To see the page</a> ";
            }
            else if (Convert.ToInt32(Request.QueryString["cid"]) == 4)
            {
                bodymail = "Your Friend " + yname + " has sent you a Addgel pen URL<br> <a href=http://www.addpens.com/product_exclusivdetails.aspx?id=" + Convert.ToInt32(Request.QueryString["pid"]) + ">Click Here To see the page</a> ";
            }
            else if (Convert.ToInt32(Request.QueryString["cid"]) == 5)
            {
                bodymail = "Your Friend " + yname + " has sent you a Addgel pen URL<br> <a href=http://www.addpens.com/product_geldetails.aspx?id=" + Convert.ToInt32(Request.QueryString["pid"]) + ">Click Here To see the page</a> ";
            }
            else if (Convert.ToInt32(Request.QueryString["cid"]) == 6)
            {
                bodymail = "Your Friend " + yname + " has sent you a Addgel pen URL<br> <a href=http://www.addpens.com/product_gelrolloverdetails.aspx?id=" + Convert.ToInt32(Request.QueryString["pid"]) + ">Click Here To see the page</a> ";
            }
            else if (Convert.ToInt32(Request.QueryString["cid"]) == 15)
            {
                bodymail = "Your Friend " + yname + " has sent you a Addgel pen URL<br> <a href=http://www.addpens.com/product_liquiddetail.aspx?id=" + Convert.ToInt32(Request.QueryString["pid"]) + ">Click Here To see the page</a> ";
            }
            else if (Convert.ToInt32(Request.QueryString["cid"]) == 16)
            {
                bodymail = "Your Friend " + yname + " has sent you a Addgel pen URL<br> <a href=http://www.addpens.com/product_premiumdeatils.aspx?id=" + Convert.ToInt32(Request.QueryString["pid"]) + ">Click Here To see the page</a> ";
            }
            else if (Convert.ToInt32(Request.QueryString["cid"]) == 7)
            {
                bodymail = "Your Friend " + yname + " has sent you a Addgel pen URL<br> <a href=http://www.addpens.com/product_ballrollerrefilldetails.aspx?id=" + Convert.ToInt32(Request.QueryString["pid"]) + ">Click Here To see the page</a> ";
            }
            else if (Convert.ToInt32(Request.QueryString["cid"]) == 8)
            {
                bodymail = "Your Friend " + yname + " has sent you a Addgel pen URL<br> <a href=http://www.addpens.com/product_exclusivrefilldetails.aspx?id=" + Convert.ToInt32(Request.QueryString["pid"]) + ">Click Here To see the page</a> ";
            }
            else if (Convert.ToInt32(Request.QueryString["cid"]) == 9)
            {
                bodymail = "Your Friend " + yname + " has sent you a Addgel pen URL<br> <a href=http://www.addpens.com/product_gelrefilldetails.aspx?id=" + Convert.ToInt32(Request.QueryString["pid"]) + ">Click Here To see the page</a> ";
            }
            else if (Convert.ToInt32(Request.QueryString["cid"]) == 10)
            {
                bodymail = "Your Friend " + yname + " has sent you a Addgel pen URL<br> <a href=http://www.addpens.com/product_gelrolloverrefilldetails.aspx?id=" + Convert.ToInt32(Request.QueryString["pid"]) + ">Click Here To see the page</a> ";
            }
            else if (Convert.ToInt32(Request.QueryString["cid"]) == 17)
            {
                bodymail = "Your Friend " + yname + " has sent you a Addgel pen URL<br> <a href=http://www.addpens.com/product_liquidrefilldetail.aspx?id=" + Convert.ToInt32(Request.QueryString["pid"]) + ">Click Here To see the page</a> ";
            }
            
            MailMessage message = new MailMessage();

            message.From = new MailAddress(yemail);

            message.To.Add(frndemail);

            message.Subject = "Your friend " + yname + " has send you a link";

            message.Body = bodymail;

            message.BodyEncoding = System.Text.Encoding.ASCII;

            message.IsBodyHtml = true;

            message.Priority = MailPriority.High;

            SmtpClient smtp = new SmtpClient("relay-hosting.secureserver.net", 25);
            //smtp.Credentials = new System.Net.NetworkCredential("info@priyanshimehta.com", "info123");
            smtp.Send(message);

            lblsubmit.Text = "Over the next few minutes an e-mail will be sent to your friend you've selected to share this website with.";
        //}
    }
}
   

﻿
<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeFile="apply.aspx.cs" Inherits="apply" Title="Untitled Page" %>

<%@ Register src="download.ascx" tagname="download" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="1000px" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
      <tr>
        <td align="left" valign="top"><img src="images/spacer.gif" width="10" height="10" /></td>
      </tr>
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="15" align="left" valign="top"><img src="images/spacer.gif" width="15" height="1" /></td>
                 
                  <td  align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="23%" align="left" valign="top">
                          <uc1:download ID="download1" runat="server" />
                        </td>
                      <td width="77%" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1%" align="left" valign="top">&nbsp;</td>
                          <td width="99%" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="right" valign="top"><table width="716" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif"><table width="716" border="0" align="right" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td align="left" valign="top" class="careers"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td height="45" align="left" valign="top">&nbsp;</td>
                                          </tr>
                                          <tr>
                                            <td align="left" valign="top"><table width="684" border="0" align="center" cellpadding="0" cellspacing="0">
                                                <tr>
                                                  <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                      <td align="left" valign="top"><img src="images/careers.jpg" width="686" height="151" /></td>
                                                    </tr>
                                                  </table></td>
                                                </tr>
                                                
                                            </table></td>
                                          </tr>
                                      </table></td>
                                    </tr>
                                    
                                    
                                  </table></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif">&nbsp;</td>
                                </tr>
                                
                                
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif" class="careersbot">
                                  <table>
                                  <tr>
                                  <td>
                                  
                                  </td>
                                  <td>
                                  <table width="496px">
                                  <tr>
                                  <td>
                                  
                                  
                                  
                                  
                                  
                                  <table width="684" border="0" align="center" cellpadding="2" cellspacing="2">
                                    <tr>
                                      <td align="left" valign="top" class="heading">Apply</td>
                                    </tr><tr>
                                      <td align="left" valign="top" ><img src="images/spacer.gif" width="15" height="5" /></td>
                                    </tr><tr>
                                      <td align="left" valign="top" class="textone">Fields mark with <span class="heading_red">*</span> are mandatory.</td>
                                    </tr>                                    
                                    <tr>
                                      <td align="left" valign="top" class="textone">
                                          <table width="70%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td align="left" valign="top">
									 
									  <table width="100%" border="0" cellpadding="3" cellspacing="3">
                                       
                                          <tr>
										  
                                            <td width="15%" align="left" valign="top" bgcolor="#FFFFFF" class="textone">Name<span class="heading_red">*</span></td>
                                            <td width="2%" align="left" valign="top" bgcolor="#FFFFFF" class="textone">:</td>
                                            <td width="75%" height="25" align="left" valign="top" bgcolor="#FFFFFF">
                                                <asp:TextBox ID="txtname" runat="server" CssClass="select"></asp:TextBox>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                    ErrorMessage="Field cannot be blank !" ControlToValidate="txtname" 
                                                    ValidationGroup="1"></asp:RequiredFieldValidator>
                                            &nbsp;</td>
                                          </tr>

                                          <tr>
                                            <td align="left" valign="top" bgcolor="#FFFFFF" class="textone">Email Id <span class="heading_red">*</span></td>
                                            <td align="left" valign="top" bgcolor="#FFFFFF" class="textone">:</td>
                                            <td height="25" align="left" valign="top" bgcolor="#FFFFFF">
                                                <asp:TextBox ID="txtemail" runat="server" CssClass="select"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                    ControlToValidate="txtemail" ErrorMessage="Field cannot be blank !" 
                                                    ValidationGroup="1"></asp:RequiredFieldValidator>
                                                                                                                                                <br />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                                    ControlToValidate="txtemail" ErrorMessage="Improper email id" 
                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                                    ValidationGroup="1"></asp:RegularExpressionValidator>
                                                                                                                                                </td>
                                          </tr>
                                          <tr>
                                            <td align="left" valign="top" bgcolor="#FFFFFF" class="textone">Resume<span class="heading_red">*</span></td>
                                            <td align="left" valign="top" bgcolor="#FFFFFF" class="textone">:</td>
                                            <td height="25" align="left" valign="top" bgcolor="#FFFFFF">
                                                <asp:FileUpload ID="FileUpload1" runat="server" />&nbsp;&nbsp;<asp:RequiredFieldValidator 
                                                    ID="RequiredFieldValidator3" runat="server" 
                                                    ControlToValidate="FileUpload1" ErrorMessage="Select your resume here" 
                                                    ValidationGroup="1"></asp:RequiredFieldValidator><br />
                                                <asp:RegularExpressionValidator ID="REVDocFile" runat="server" 
                                                    ControlToValidate="FileUpload1" ErrorMessage="Select Doc File Only" 
                                                    ValidationExpression=".*\.(doc)" ValidationGroup="1"></asp:RegularExpressionValidator>
                                              </td>
                                          </tr>
                                          <tr>
                                            <td align="left" valign="top" bgcolor="#FFFFFF" class="textone">&nbsp;</td>
                                            <td align="left" valign="top" bgcolor="#FFFFFF" class="textone">&nbsp;</td>
                                            <td height="25" align="left" valign="top" bgcolor="#FFFFFF">
                                                <asp:ImageButton ID="ImageButton1" runat="server" width="76" height="26" 
                                                  ImageUrl="~/images/submit.gif" ValidationGroup="1" 
                                                    onclick="ImageButton1_Click"/></td>
                                          </tr>
                                      </table>
									  </form>
									  </td>
                                    </tr>
                                </table></td>
                                    </tr>
                                    <tr>
                                      <td align="left" valign="top" class="textone">&nbsp;</td>
                                    </tr>
                                  </table></td>
                                </tr>
                                  </table>
                                  </td>
                                </tr>
                                  </table>
                                  </td>
                                </tr>
                                
                              </table></td>
                            </tr>
                          </table></td>
                        </tr>
                      </table></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top">&nbsp;</td>
                      <td align="left" valign="top">&nbsp;</td>
                    </tr>
                  </table></td>
                  <td width="12"  align="right" valign="top"><img src="images/spacer.gif" width="15" height="1" /></td>
                </tr>
                
          </table></td>
      </tr>
    </table>
</asp:Content>


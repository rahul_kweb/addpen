﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class ButterSoftSeriesdetails : System.Web.UI.Page
{
    public string zoomimg = string.Empty;
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        bool sendBack = true;
        if (Request.QueryString["id"] != null)
        {
            string qs = Request.QueryString["id"].Trim();
            if (c.IsNumeric(qs))
            {
                int id = Convert.ToInt32(qs);

                sendBack = ProductBindById(id);
                if (!sendBack)
                {
                    sendBack = bind_photo(id);

                    if (!sendBack)
                    {
                        if (Request.QueryString["subid"] != null)
                        {
                            string qs1 = Request.QueryString["subid"].Trim();
                            if (c.IsNumeric(qs1))
                            {
                                int pid = Convert.ToInt32(qs1);
                                sendBack = ProductSubImageBindById(pid);
                            }
                            else
                            {
                                sendBack = true;
                            }
                        }
                    }
                }
            }
        }
        if (sendBack)
        {
            Response.Redirect("product_gelpen1.aspx");
        }
    }

    private bool ProductSubImageBindById(int pid)
    {
        bool sendBack = false;
        //DataTable dt = Class1.GetRecord("select * from ProductImageGalleryTB where ProductImage_Id=" + pid + "");
        DataTable dt = Class1.GetRecord("EXEC GetProductImageGalleryTB 'GetByImageId'," + pid);
        if (dt.Rows.Count > 0)
            Zoomimage.Src = "Addgel/adminImage/" + dt.Rows[0]["Product_ZoomImage"].ToString();
        //  Zoomimage.Src = "Admin/adminImage/" + dt.Rows[0]["P_Banner"].ToString();
        else
            sendBack = true;
        return sendBack;
    }
    private bool ProductBindById(int pid)
    {
        bool sendBack = false;
        //DataTable dt = Class1.GetRecord("select * from ProductTB where P_Id=" + pid + "");
        DataTable dt = Class1.GetRecord("EXEC GetProductTB 'GetById'," + pid);
        if (dt.Rows.Count > 0)
        {

            Zoomimage.Src = "Addgel/adminImage/" + dt.Rows[0]["P_Banner"].ToString();
            lbldescription.Text = dt.Rows[0]["P_Description"].ToString();
            lblhead.Text = dt.Rows[0]["P_Head"].ToString();
            Page.Title = "ADD Gel - GelPen - " + dt.Rows[0]["P_Name"].ToString();
            if (dt.Rows[0]["P_Features"].ToString() == "")
            {
            }
            else
            {
                string propertiesnew = dt.Rows[0]["P_Features"].ToString();

                char[] separator = new char[] { '#' };
                string[] prolist = propertiesnew.Split(separator);


                lblfeatures.Text = "";
                string body = "";
                body = body + "<table width='100%' border='0' cellspacing='1' cellpadding='1'>";
                for (int j = 0; j <= prolist.Length - 1; j++)
                {
                    body = body + "<tr>";
                    body = body + "<td width='2%' align='left' valign='middle'><img src='images/bullet12.gif' width='7' height='8' /></td>";
                    body = body + " <td width='100%' align='left' valign='top' class='textone'>" + prolist[j] + "</td>";
                    //body = body + " <td align='left' valign='top' class='textone'>&nbsp;</td>";
                    body = body + "</tr>";
                }


                //string color = dt.Rows[0]["P_Color"].ToString();
                //char[] colorseparator = new char[] { ',' };
                //string[] colorlist = color.Split(colorseparator);


                //body = body + " <tr>";
                //body = body + " <td width='2%' align='left' valign='middle'><img src='images/bullet12.gif' width='7' height='8' /></td>";
                //body = body + " <td width='40%' align='left' valign='top'>Ink Colours - <table><tr>";


                //for (int i = 0; i <= colorlist.Length - 1; i++)
                //{
                //    DataTable dtcolor = Class1.GetRecord("select * from InkColorMTB where InkColorM_id=" + colorlist[i] + "");
                //    string colorper = dtcolor.Rows[0]["InkColorM_Code"].ToString();
                //    body = body + " <td width='10%' bgcolor='" + colorper + "'/>&nbsp;</td>";
                //}

                //body = body + "</tr></table></td><td align='left' valign='top' class='textone'>&nbsp;</td>";
                //body = body + " </tr>";




                string color = dt.Rows[0]["P_Color"].ToString();
                char[] colorseparator = new char[] { ',' };
                string[] colorlist = color.Split(colorseparator);
                body = body + " <tr>";
                body = body + " <td width='2%' align='left' valign='middle'><img src='images/bullet12.gif' width='7' height='8' /></td>";
                //   body = body + " <td width='40%' cellpadding='5' align='left' valign='middle'>";

                body = body + " <td  width='4%' /><span class='textone'> Ink Colors - </span>";


                for (int i = 0; i <= colorlist.Length - 1; i++)
                {
                    //DataTable dtcolor = Class1.GetRecord("select * from InkColorMTB where InkColorM_id=" + colorlist[i] + "");
                    DataTable dtcolor = Class1.GetRecord("EXEC GetInkColorMTB 'Get_By_InkColorM_id'," + colorlist[i]);


                    string colorper = dtcolor.Rows[0]["InkColorM_Code"].ToString();
                    body = body + " <span style='background-color:" + colorper + "; width:20px;height:4px' />&nbsp;&nbsp;&nbsp;</span>";
                }

                // body = body + "</tr></table></td> <td width='58%' align='left' valign='middle'>&nbsp;</td>";
                body = body + " </td></tr>";

                string refillid = dt.Rows[0]["P_RefileId"].ToString();

                if (refillid != "")
                {
                    char[] refillseparator = new char[] { ',' };
                    string[] refilllist = refillid.Split(refillseparator);

                    //DataTable dtpen = Class1.GetRecord("select * from ProductTB");

                    string z = "";
                    body = body + "<tr>";
                    body = body + "<td align='left' valign='middle'><img src='images/bullet12.gif' width='7' height='8' /></td>";
                    body = body + "<td align='left' valign='top'><span class='textone'> Refill : </span>";
                    for (int i = 0; i <= refilllist.Length - 1; i++)
                    {
                        //DataTable dtpen = Class1.GetRecord("select * from RefillTB where R_Id=" + refilllist[i] + "");
                        DataTable dtpen = Class1.GetRecord("EXEC GETRefillTB 'Get_By_R_Id'," + refilllist[i]);
                        z = dtpen.Rows[0]["R_Id"].ToString();

                        //if (refillid.Contains(z) == true)
                        //{
                        if (i == refilllist.Length - 1)
                        {
                            int id = Convert.ToInt32(z);
                            body = body + "  <span class='sitemap'><a href='jetflow_refilldetail.aspx?id=" + id + "'> <span class='red'>" + dtpen.Rows[0]["R_Name"].ToString() + "</span> </a> </span>";
                        }
                        else if (i != refilllist.Length - 1)
                        {
                            int id = Convert.ToInt32(z);
                            body = body + "  <span class='sitemap'><a href='jetflow_refilldetail.aspx?id=" + id + "'> <span class='red'>" + dtpen.Rows[0]["R_Name"].ToString() + "</span> </a> , </span>";
                        }

                        //}

                        // body = body + " <td width='10%' bgcolor=" + colorlist[i] + "/>&nbsp;</td>";

                        z = "";
                    }
                    body = body + "</td></tr>";
                }




                body = body + " </table>";
                lblfeatures.Text = body;
            }

            //DataTable dt1 = Class1.GetRecord("select * from ProductImageGalleryTB where ProductId=" + pid + "");
            //// zoomimg = dt1.Rows[0]["Product_ZoomImage"].ToString();
            //DLProduct.DataSource = dt1;
            //DLProduct.DataBind();
        }
        else
        {
            sendBack = true;

        }
        return sendBack;
    }


    //protected void datalist_command(object source, DataListCommandEventArgs e)
    //{
    //    if (e.CommandName == "Large1")
    //    {
    //        Label linkButton1 = (Label)e.Item.FindControl("lblzoom1");
    //        Zoomimage.Src = "~/Admin/adminImage/" + linkButton1.Text;
    //        zoomimg = linkButton1.Text;
    //    }
    //}


    public bool bind_photo(int prodid)
    {
        bool sendBack = false;
        string picture = "";
        //DataTable dt1 = Class1.GetRecord("select * from ProductImageGalleryTB where productid=" + prodid + "");
        DataTable dt1 = Class1.GetRecord("EXEC GetProductImageGalleryTB 'GetByProductId',0," + prodid);
        // zoomimg = dt1.Rows[0]["Product_ZoomImage"].ToString();
        if (dt1.Rows.Count > 0)
        {

            for (int i = 0; dt1.Rows.Count - 1 >= i; i++)
            {
                //style='float: none; vertical-align:'middle'; left: 0px; '

                picture += " <div class='panel' style='float: none; vertical-align:'middle'; left: 0px;'  >";

                picture += " 	  <a id='aimages' target='' name='aimages'  href='ButterSoftSeriesdetails.aspx?id=" + Request.QueryString["id"] + "&subid=" + dt1.Rows[i]["ProductImage_Id"].ToString() + "' > ";

                //picture += " 	  <a href=admin/adminImage/" + dt1.Rows[i]["Product_zoomimage"].ToString() + " rel=thumbnail> ";

                picture += " 	<img id='thumbimg'  src='Addgel/adminImage/" + dt1.Rows[i]["Product_ThumbnailImage"].ToString() + "' border='0' alt=''   ></a>";


                picture += " 	</div>";
                //  picture += "   <img src='admin/adminImage/" + dt1.Rows[i]["Product_ThumbnailImage"].ToString() + "' alt='' width='244' height='122' />&nbsp;&nbsp;  <a href='#' onclick='show_image('" + dt1.Rows[i]["Product_ThumbnailImage"].ToString() + "')'> ";
            }

            lblpictures.Text = picture;
        }
        else
        {
            sendBack = true;
        }
        return sendBack;
    }
    protected void btnenquiry_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("enquiry.aspx?cid=5&pid=" + Convert.ToInt32(Request.QueryString["id"]) + "");
    }
    protected void btntellafriend_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("Tellafriend.aspx?cid=5&pid=" + Convert.ToInt32(Request.QueryString["id"]) + "");
    }

}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_ProductDetailsupdate : System.Web.UI.Page
{
    public string global_ink = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    int id = Convert.ToInt32(Request.QueryString["id"]);
                    BindProductperbyid(id);
                    HiddenField1.Value = id.ToString();
                    Bindinkcolor();
                    bind_second_list();

                    DataTable dt = Class1.GetRecord("select * from ProductTB where P_Id=" + id + "");
                    if (dt.Rows[0]["P_RefileId"].ToString() != "")
                    {
                        bind_Refill_second_list();
                    }
                    RefillCategoryBind();
                    DDLRefilecategory.Items.Insert(0, new ListItem("-----------Select-----------"));
                    //Currentinkcolor();
                }
            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }
    }
    private void RefillCategoryBind()
    {
        DataTable dt = Class1.GetRecord("select * from CategoryTB where C_CatType=2 and C_ParentId=2");
        DDLRefilecategory.DataSource = dt;
        DDLRefilecategory.DataBind();
    }

    private void Bindinkcolor()
    {
        string colorname = hfinkcolor.Value;     
        DataTable dt = Class1.GetRecord("select * from InkColorMTB where InkColorM_id not in (" + colorname + ")");
        ListBox1.DataSource = dt;
        ListBox1.DataBind();
        DataTable dtcolor = Class1.GetRecord("select * from ProductTB where P_Id=" + Convert.ToInt32(Request.QueryString["id"]) + "");
        string color = dtcolor.Rows[0]["P_Color"].ToString();        
    }

    private void bind_second_list()
    {
        string colorname = hfinkcolor.Value;        
        DataTable dtrefill = Class1.GetRecord("select * from InkColorMTB where InkColorM_id IN (" + colorname + ")");
                  
        lstcolorupdate.DataValueField = "InkColorM_id";
        lstcolorupdate.DataTextField = "InkColorM_Name";

        lstcolorupdate.DataSource = dtrefill;
        lstcolorupdate.DataBind();         


    }


    private void bind_Refill_First_list()
    {
        DataTable dtrefill = Class1.GetRecord("select * from CategoryTB ");
        
        lstrefillupdate.DataValueField = "R_Id";
        lstrefillupdate.DataTextField = "R_Name";

        lstrefillupdate.DataSource = dtrefill;
        lstrefillupdate.DataBind();


    }

    private void bind_Refill_second_list()
    {
        string colorname = hfrefillid.Value;
        DataTable dtrefill = Class1.GetRecord("select * from RefillTB where R_Id IN (" + colorname + ")");
        
        lstrefillupdate.DataValueField = "R_Id";
        lstrefillupdate.DataTextField = "R_Name";

        lstrefillupdate.DataSource = dtrefill;
        lstrefillupdate.DataBind();


    }
    
    protected void btnnewline_Click(object sender, EventArgs e)
    {
        txtfeature.Text = txtfeature.Text + "#";
        txtfeature.Focus();
    }

    protected void DDLRefilecategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        string colorname = hfrefillid.Value;
        if (DDLRefilecategory.SelectedItem.Text != "-----------Select-----------")
        { 
            if (colorname != "")
            {
            DataTable dt = Class1.GetRecord("select * from CategoryTB where C_ParentId=" + Convert.ToInt32(DDLRefilecategory.SelectedValue) + " and C_Pid not in("+colorname+") ");
           
                lstrefill.DataSource = dt;
                lstrefill.DataBind();
            }
            else
            {
                DataTable dt = Class1.GetRecord("select * from CategoryTB where C_ParentId=" + Convert.ToInt32(DDLRefilecategory.SelectedValue) + "");

                lstrefill.DataSource = dt;
                lstrefill.DataBind();
            }
        }
        else
        {
            Response.Write("<script>alert('Please select refill category')</script>");
            lstrefill.Items.Clear();
        }
    }
    private void BindProductperbyid(int id)
    {
        DataTable dt = Class1.GetRecord("select * from ProductTB where P_Id="+id+"");


        txtproductname.Text = dt.Rows[0]["P_Name"].ToString();
        txthead.Text = dt.Rows[0]["P_Head"].ToString();
        txtdescription.Text = dt.Rows[0]["P_Description"].ToString();
        txtfeature.Text = dt.Rows[0]["P_Features"].ToString();
        txtprices.Text = dt.Rows[0]["P_Prices"].ToString();
       
        Image1.ImageUrl = "~/Addgel/AdminImage/" + dt.Rows[0]["P_Thumbnail"].ToString();
        Image2.ImageUrl = "~/Addgel/AdminImage/" + dt.Rows[0]["P_Large"].ToString();
        Image3.ImageUrl = "~/Addgel/AdminImage/" + dt.Rows[0]["P_Zoom"].ToString();
        Image4.ImageUrl = "~/Addgel/AdminImage/" + dt.Rows[0]["P_Banner"].ToString();

        hfrefillid.Value = dt.Rows[0]["P_RefileId"].ToString();
        hfinkcolor.Value = dt.Rows[0]["P_Color"].ToString();
   
        hfbannerimg.Value = dt.Rows[0]["P_Banner"].ToString();
        hfthumbimg.Value = dt.Rows[0]["P_Thumbnail"].ToString();
        hflargeimg.Value = dt.Rows[0]["P_Large"].ToString();
        hfzoomimg.Value = dt.Rows[0]["P_Zoom"].ToString();
        hfpdf.Value = dt.Rows[0]["P_PDF"].ToString();
        HFSearch.Value = dt.Rows[0]["CategoryName"].ToString();
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        string probanner = "";
        string prthumnail = "";
        string prolarge = "";
        string prozoom = "";
        string propdf = "";
        
        string sqlvalue ="";
        for (int i = 0; i < lstcolorupdate.Items.Count; i++)
        {
            //if (ListBox1.Items[i].Selected == true)
            //{
            if (i == lstcolorupdate.Items.Count - 1)
                {
                    sqlvalue += lstcolorupdate.Items[i].Value;
                }
                else
                {
                    sqlvalue += lstcolorupdate.Items[i].Value + " , ";
                }
            //}
            //else
            //{
            //    sqlvalue = hfinkcolor.Value;
            //}
        }

        string refillid = "";
            for (int i = 0; i < lstrefillupdate.Items.Count; i++)
            {
                //if (lstrefill.Items[i].Selected == true)
                //{
                if (i == lstrefillupdate.Items.Count - 1)
                    {
                        refillid += lstrefillupdate.Items[i].Value;
                    }
                    else
                    {
                        refillid += lstrefillupdate.Items[i].Value + " , ";
                    }
                //}               
            }  
        if (FUBannerimg.HasFile == true)
        {
            probanner = "Addpenbanner" + HiddenField1.Value + "" + System.IO.Path.GetExtension(FUBannerimg.FileName);
            FUBannerimg.SaveAs(Server.MapPath("AdminImage\\" + probanner + ""));
        }
        else
        {
            probanner = hfbannerimg.Value;
        }
        if (FUthumbnail.HasFile == true)
        {
            prthumnail = "Addpenthum" + HiddenField1.Value + "" + System.IO.Path.GetExtension(FUthumbnail.FileName);
            FUthumbnail.SaveAs(Server.MapPath("AdminImage\\" + prthumnail + ""));
        }
        else
        {
            prthumnail = hfthumbimg.Value;
        }

        if (FUlarge.HasFile == true)
        {
            prolarge = "Addpenlarge" + HiddenField1.Value + "" + System.IO.Path.GetExtension(FUlarge.FileName);

            FUlarge.SaveAs(Server.MapPath("AdminImage\\" + prolarge + ""));
        }
        else
        {
            prolarge = hflargeimg.Value;
        }


        if (FUzoom.HasFile == true)
        {
            prozoom = "Addpenzoom" + HiddenField1.Value + "" + System.IO.Path.GetExtension(FUzoom.FileName);

            FUzoom.SaveAs(Server.MapPath("AdminImage\\" + prozoom + ""));
        }
        else
        {
            prozoom = hfzoomimg.Value;  
        }


        if (FUpdf.HasFile == true)
        {
            propdf = "Addpenpdf" + HiddenField1.Value + "" + System.IO.Path.GetExtension(FUpdf.FileName);
            FUpdf.SaveAs(Server.MapPath("DocFile\\" + propdf + ""));
        }
        else
        {
            propdf = hfpdf.Value;
        }

        string search = HFSearch.Value + "," + txtproductname.Text.Replace("'", "''");


        bool check = Class1.InsertUpdate("update ProductTB set P_Name='" + txtproductname.Text.Replace("'", "''") + "',P_Head='" + txthead.Text.Replace("'", "''") + "',P_Description='" + txtdescription.Text.Replace("'", "''") + "',P_Features='" + txtfeature.Text.Replace("'", "''") + "',P_RefileId='" + refillid + "',P_Color='" + sqlvalue + "',P_Prices='" + txtprices.Text.Replace("'", "''") + "',P_search='" + search + "',P_Banner='" + probanner + "',P_Thumbnail='" + prthumnail + "' ,P_Large='" + prolarge + "',P_Zoom='" + prozoom + "',P_PDF='" + propdf + "' where P_Id=" + Convert.ToInt32(Request.QueryString["id"]) + "");
        
        lblupdate.Text = "Updated Successfully";
    }
    protected void btninsertrefillid_Click(object sender, EventArgs e)
    {

    }
    protected void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
        //int id =Convert.ToInt32(ListBox1.SelectedValue);      
        //DataTable dt = Class1.GetRecord("select * from ProductTB where P_Id="+Convert.ToInt32(HiddenField1.Value)+"");
        //string z = "";
        //z = dt.Rows[0]["P_RefileId"].ToString();

        //if (id == Convert.ToInt32(z))
        //{ 
        
        //}
        
    }
    protected void lstcolorupdate_SelectedIndexChanged(object sender, EventArgs e)
    {     

       // lstcolorupdate.Items.Remove(lstcolorupdate.SelectedItem);
    }
    protected void btnLeft_Click(object sender, EventArgs e)
    {
        try
        {
            ListItemCollection lsc = new ListItemCollection();
            for (int i = 0; i < lstrefillupdate.Items.Count; i++)
            {
                if (lstrefillupdate.Items[i].Selected == true)
                {

                    string value = lstrefillupdate.Items[i].Value;
                    string text = lstrefillupdate.Items[i].Text;
                    ListItem lst = new ListItem();
                    lst.Text = text;
                    lst.Value = value;
                    lstrefill.Items.Add(lst);
                    lsc.Add(lst);
                    lstrefillupdate.Items.Remove(lstrefillupdate.Items[i]);
                }
            }
            foreach (ListItem ls in lsc)
            {
                lstrefillupdate.Items.Remove(ls);
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }

    }
    protected void btnRight_Click(object sender, EventArgs e)
    {
        try
        {
            ListItemCollection lsc = new ListItemCollection();
            for (int i = 0; i <= lstrefill.Items.Count - 1; i++)
            {
                if (lstrefill.Items[i].Selected == true)
                {

                    string value = lstrefill.Items[i].Value;
                    string text = lstrefill.Items[i].Text;
                    ListItem lst = new ListItem();
                    lst.Text = text;
                    lst.Value = value;
                    lstrefillupdate.Items.Add(lst);
                    lsc.Add(lst);
                    lstrefill.Items.Remove(lstrefill.Items[i]);
                }
            }
            foreach (ListItem ls in lsc)
            {
                lstrefill.Items.Remove(ls);
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }

    }
    protected void btnLeftAll_Click(object sender, EventArgs e)
    {
        try
        {
            ListItemCollection lsc = new ListItemCollection();
            for (int i = 0; i <= lstrefillupdate.Items.Count - 1; i++)
            {
                string value = lstrefillupdate.Items[i].Value;
                string text = lstrefillupdate.Items[i].Text;
                ListItem lst = new ListItem();
                lst.Text = text;
                lst.Value = value;
                lstrefill.Items.Add(lst);
                lsc.Add(lst);
                // list1.Items.Remove(list1.Items[i]);

            }
            foreach (ListItem ls in lsc)
            {
                lstrefillupdate.Items.Remove(ls);
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }

    }
    protected void btnRightAll_Click(object sender, EventArgs e)
    {
        try
        {
            ListItemCollection lsc = new ListItemCollection();
            for (int i = 0; i < lstrefill.Items.Count; i++)
            {
                string value = lstrefill.Items[i].Value;
                string text = lstrefill.Items[i].Text;
                ListItem lst = new ListItem();
                lst.Text = text;
                lst.Value = value;
                lstrefillupdate.Items.Add(lst);
                lsc.Add(lst);
                // list2.Items.Remove(list2.Items[i]);

            }
            foreach (ListItem ls in lsc)
            {
                lstrefill.Items.Remove(ls);
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            ListItemCollection lsc = new ListItemCollection();
            for (int i = 0; i < lstcolorupdate.Items.Count; i++)
            {
                if (lstcolorupdate.Items[i].Selected == true)
                {

                    string value = lstcolorupdate.Items[i].Value;
                    string text = lstcolorupdate.Items[i].Text;
                    ListItem lst = new ListItem();
                    lst.Text = text;
                    lst.Value = value;
                    ListBox1.Items.Add(lst);
                    lsc.Add(lst);
                    lstcolorupdate.Items.Remove(lstcolorupdate.Items[i]);
                }
            }
            foreach (ListItem ls in lsc)
            {
                lstcolorupdate.Items.Remove(ls);
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        try
        {
            ListItemCollection lsc = new ListItemCollection();
            for (int i = 0; i <= ListBox1.Items.Count - 1; i++)
            {
                if (ListBox1.Items[i].Selected == true)
                {

                    string value = ListBox1.Items[i].Value;
                    string text = ListBox1.Items[i].Text;
                    ListItem lst = new ListItem();
                    lst.Text = text;
                    lst.Value = value;
                    lstcolorupdate.Items.Add(lst);
                    lsc.Add(lst);
                    ListBox1.Items.Remove(ListBox1.Items[i]);
                }
            }
            foreach (ListItem ls in lsc)
            {
                ListBox1.Items.Remove(ls);
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        try
        {
            ListItemCollection lsc = new ListItemCollection();
            for (int i = 0; i <= lstcolorupdate.Items.Count - 1; i++)
            {
                string value = lstcolorupdate.Items[i].Value;
                string text = lstcolorupdate.Items[i].Text;
                ListItem lst = new ListItem();
                lst.Text = text;
                lst.Value = value;
                ListBox1.Items.Add(lst);
                lsc.Add(lst);
                // list1.Items.Remove(list1.Items[i]);

            }
            foreach (ListItem ls in lsc)
            {
                lstcolorupdate.Items.Remove(ls);
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        try
        {
            ListItemCollection lsc = new ListItemCollection();
            for (int i = 0; i < ListBox1.Items.Count; i++)
            {
                string value = ListBox1.Items[i].Value;
                string text = ListBox1.Items[i].Text;
                ListItem lst = new ListItem();
                lst.Text = text;
                lst.Value = value;
                lstcolorupdate.Items.Add(lst);
                lsc.Add(lst);
                // list2.Items.Remove(list2.Items[i]);

            }
            foreach (ListItem ls in lsc)
            {
                ListBox1.Items.Remove(ls);
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }



    protected void btnup_Click(object sender, EventArgs e)
    {
        int SelectedIndex = lstcolorupdate.SelectedIndex;

        if (SelectedIndex == -1) // nothing selected
            return;
        if (SelectedIndex == 0) // already at top of list  
            return;

        ListItem Temp;
        Temp = lstcolorupdate.SelectedItem;

        lstcolorupdate.Items.Remove(lstcolorupdate.SelectedItem);
        lstcolorupdate.Items.Insert(SelectedIndex - 1, Temp);
    }
    protected void btndown_Click(object sender, EventArgs e)
    {
        int SelectedIndex = lstcolorupdate.SelectedIndex;

        if (SelectedIndex == -1)  // nothing selected
            return;
        if (SelectedIndex == lstcolorupdate.Items.Count - 1)  // already at top of list            
            return;

        ListItem Temp;
        Temp = lstcolorupdate.SelectedItem;

        lstcolorupdate.Items.Remove(lstcolorupdate.SelectedItem);
        lstcolorupdate.Items.Insert(SelectedIndex + 1, Temp);
    }
}

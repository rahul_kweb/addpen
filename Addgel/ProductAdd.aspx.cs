﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_ProductAdd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            if (!IsPostBack)
            {
                CategoryBind();
                Bindinkcolor();
                DDLCategory.Items.Insert(0, new ListItem("-----------Select-----------"));
                RefillCategoryBind();
                DDLRefilecategory.Items.Insert(0, new ListItem("-----------Select-----------"));
            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }

    }
    private void CategoryBind()
    {
        DataTable dt = Class1.GetRecord("select * from CategoryTB where C_CatType=1 and C_ParentId=1");
        DDLCategory.DataSource = dt;
        DDLCategory.DataBind();
    }
    private void RefillCategoryBind()
    {
        DataTable dt = Class1.GetRecord("select * from CategoryTB where C_CatType=2 and C_ParentId=2");
        DDLRefilecategory.DataSource = dt;
        DDLRefilecategory.DataBind();
    }

    private void Bindinkcolor()
    {
        DataTable dt = Class1.GetRecord("select * from InkColorMTB");
        ListBox1.DataSource = dt;
        ListBox1.DataBind();      
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        int productid = Class1.MaxProductId() + 1;

        string extensionI1 = System.IO.Path.GetExtension(FUthumbnail.FileName);
        string extensionI2 = System.IO.Path.GetExtension(FUlarge.FileName);
        string extensionI3 = System.IO.Path.GetExtension(FUzoom.FileName);
        string extensionI4 = System.IO.Path.GetExtension(FUpdf.FileName);
        string extensionI5 = System.IO.Path.GetExtension(FUBannerimg.FileName);

        string proname = txtproductname.Text.Replace("'", "''");
        string prohead = txthead.Text.Replace("'", "''");
        string prodisc = txtdescription.Text.Replace("'", "''");
        string profeature = txtfeature.Text.Replace("'", "''");
        string proprice = txtprices.Text.Replace("'", "''");
        string search = DDLCategory.SelectedItem.Text + "," + txtproductname.Text.Replace("'", "''");

        string sqlvalue = "";       
        for (int i = 0; i < ListBox1.Items.Count; i++)
        {
            if (ListBox1.Items[i].Selected == true)
            {
                if (i == ListBox1.Items.Count - 1)
                {
                    sqlvalue += ListBox1.Items[i].Value;
                }
                else
                {
                    sqlvalue += ListBox1.Items[i].Value + ",";
                }
            }                
        }

        string sqlvalue1 =sqlvalue;
        if (sqlvalue1 != "")
        {
            if (sqlvalue1.Substring(sqlvalue1.Length - 1, 1) == ",")
            {
                sqlvalue1 = sqlvalue1.Substring(0, sqlvalue1.Length - 1);
            }
        }
        string refillid = "";        
        for (int i = 0; i < lstrefill.Items.Count; i++)
        {
            if (lstrefill.Items[i].Selected == true)
            {                
               if (i == lstrefill.Items.Count - 1)
               {
                 refillid += lstrefill.Items[i].Value;
               }
               else
               {
                 refillid += lstrefill.Items[i].Value + ",";
               }              
            }
        }
        string refillid1 = refillid;
        if (refillid1 != "")
        {
            if (refillid1.Substring(refillid1.Length - 1, 1) == ",")
            {
                refillid1 = refillid1.Substring(0, refillid1.Length - 1);
            }
        }


        string prthumnail=string.Empty;
        string prolarge=string.Empty;
        string prozoom=string.Empty;
        string propdf=string.Empty;
        string probanner = string.Empty;
        
        if (extensionI1 != "")
        {
            prthumnail = "Addpenthum" + productid + "" + extensionI1;
        }
        else
        {
            prthumnail = "";
        }
        if (extensionI2 != "")
        {
            prolarge = "Addpenlarge" + productid + "" + extensionI2;
        }
        else
        {
            prolarge = "";
        }

        if (extensionI3 != "")
        {
            prozoom = "Addpenzoom" + productid + "" + extensionI3;
        }
        else
        {
            prozoom = "";
        }

        if (extensionI4 != "")
        {
            propdf = "Addpenpdf" + productid + "" + extensionI4;
        }
        else
        {
            propdf = "";
        }

        if (extensionI5 != "")
        {
            probanner = "Addpenbanner" + productid + "" + extensionI5;
        }
        else
        {
            probanner = "";
        }

        if (DDLCategory.SelectedItem.Text != "-----------Select-----------")
        {

            string date = DateTime.Today.ToShortDateString();
            bool check = Class1.InsertUpdate("insert into ProductTB values('" + proname + "','" + prohead + "','" + prodisc + "','" + profeature + "','" + DDLCategory.SelectedItem.Text + "','" + Convert.ToInt32(DDLCategory.SelectedValue) + "','" + refillid1 + "','" + sqlvalue1 + "','" + search + "','" + probanner + "','" + prthumnail + "','" + prolarge + "','" + prozoom + "','" + propdf + "','Active','Active','" + proprice + "','" + date + "')");

            int productid2 = Class1.MaxProductId();
            bool chkmultiimg = Class1.InsertUpdate("insert into ProductImageGalleryTB values(" + productid2 + ",'" + prthumnail + "','" + prozoom + "',getdate())");

            int pid = Convert.ToInt32(DDLCategory.SelectedValue);
            string navigation = string.Empty;
            if (pid == 3)
            {
                navigation = "product_ballrollerdetails.aspx";
            }
            else if (pid == 4)
            {
                navigation = "product_exclusivdetails.aspx";
            }
            else if (pid == 5)
            {
                navigation = "product_geldetails.aspx";
            }
            else if (pid == 6)
            {
                navigation = "product_gelrolloverdetails.aspx";
            }
            else if (pid == 15)
            {
                navigation = "product_liquiddetail.aspx";
            }
            else if (pid == 16)
            {
                navigation = "product_premiumdeatils.aspx";
            }
            int productid1 = Class1.MaxProductId();
            
            bool categoryproname = Class1.InsertUpdate("insert into CategoryTB values('" + proname + "'," + productid1 + "," + Convert.ToInt32(DDLCategory.SelectedValue) + ",1,'Child','','" + navigation + "','','Active','" + date + "',000)");
            if (extensionI1 != "")
            {

                FUthumbnail.SaveAs(Server.MapPath("adminImage\\" + prthumnail + ""));
            }

            if (extensionI2 != "")
            {

                FUlarge.SaveAs(Server.MapPath("adminImage\\" + prolarge + ""));
            }

            if (extensionI3 != "")
            {

                FUzoom.SaveAs(Server.MapPath("adminImage\\" + prozoom + ""));
            }

            if (extensionI4 != "")
            {

                FUpdf.SaveAs(Server.MapPath("DocFile\\" + propdf + ""));
            }
            if (extensionI5 != "")
            {
                FUBannerimg.SaveAs(Server.MapPath("adminImage\\" + probanner + ""));
            }
            DDLCategory.SelectedIndex = 0;
            DDLRefilecategory.SelectedIndex = 0;
            txtproductname.Text = "";
            txthead.Text = "";
            txtdescription.Text = "";
            txtfeature.Text = "";
            txtprices.Text = "";
            Response.Write("<script>alert('Pen product added sucessfully')</script>");
        }
        else
        {
            Response.Write("<script>alert('Please select category')</script>");
            lstrefill.Items.Clear();
        }
               
    }

    protected void btnnewline_Click(object sender, EventArgs e)
    {
        txtfeature.Text = txtfeature.Text + "#";
        txtfeature.Focus();
    }
    protected void DDLRefilecategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLRefilecategory.SelectedItem.Text != "-----------Select-----------")
        {
            DataTable dt = Class1.GetRecord("select * from CategoryTB where C_ParentId=" + Convert.ToInt32(DDLRefilecategory.SelectedValue) + "");

            if (dt.Rows.Count > 0)
            {
                lstrefill.DataSource = dt;
                lstrefill.DataBind();
               
            }
            else
            {
                lstrefill.Items.Clear();
            }
        }
        else
        {
            Response.Write("<script>alert('Please select refill category')</script>");
            lstrefill.Items.Clear();
        }
    }
}

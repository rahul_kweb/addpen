﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_SeriesDetailsupdate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    int id = Convert.ToInt32(Request.QueryString["id"]);

                    BindProductperbyid(id);
                    HiddenField1.Value = id.ToString();
                }
            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }
    }

    private void BindProductperbyid(int id)
    {
        DataTable dt = Class1.GetRecord("select * from PenSeriesTB where PSeries_Id=" + id + "");

        txthead.Text = dt.Rows[0]["PSeries_Head"].ToString();
        txtdescription.Text = dt.Rows[0]["PSeries_Description"].ToString();

        Image1.ImageUrl = "~/Addgel/AdminImage/" + dt.Rows[0]["PSeries_BannerImage"].ToString();
        Image2.ImageUrl = "~/Addgel/AdminImage/" + dt.Rows[0]["PSeries_BannerFooterImage"].ToString();
        Image3.ImageUrl = "~/Addgel/AdminImage/" + dt.Rows[0]["PSeries_LogoImage"].ToString();

        hfbannerimg.Value = dt.Rows[0]["PSeries_BannerImage"].ToString();
        hfbannerfooter.Value = dt.Rows[0]["PSeries_BannerFooterImage"].ToString();
        hflogo.Value = dt.Rows[0]["PSeries_LogoImage"].ToString();

    }
    protected void btnadd_Click(object sender, EventArgs e)
    {
        string prohead = txthead.Text.Replace("'", "''");
        string prodescription = txtdescription.Text.Replace("'", "''");
        string seriesbanner = "";
        string seriesbannerfooter = "";
        string serieslogo = "";

        if (FUlarge.HasFile == true)
        {
            seriesbanner = "AddpenSeriesBannner" + HiddenField1.Value + "" + System.IO.Path.GetExtension(FUlarge.FileName);
            FUlarge.SaveAs(Server.MapPath("adminImage\\" + seriesbanner + ""));
        }
        else
        {
            seriesbanner = hfbannerimg.Value;
        }
        if (FUBannerFooter.HasFile == true)
        {
            seriesbannerfooter = "AddpenSeriesBannerFooter" + HiddenField1.Value + "" + System.IO.Path.GetExtension(FUBannerFooter.FileName);
            FUBannerFooter.SaveAs(Server.MapPath("adminImage\\" + seriesbannerfooter + ""));
        }
        else
        {
            seriesbannerfooter = hfbannerfooter.Value;
        }
        if (FULogo.HasFile == true)
        {
            serieslogo = "AddpenSeriesLogo" + HiddenField1.Value + "" + System.IO.Path.GetExtension(FULogo.FileName);
            FULogo.SaveAs(Server.MapPath("adminImage\\" + serieslogo + ""));
        }
        else
        {
            serieslogo = hflogo.Value;
        }
        bool check = Class1.InsertUpdate("update PenSeriesTB set PSeries_Head='" + prohead + "',PSeries_Description='" + prodescription + "',PSeries_BannerImage='" + seriesbanner + "',PSeries_BannerFooterImage='" + seriesbannerfooter + "',PSeries_LogoImage='" + serieslogo + "' where PSeries_Id=" + Convert.ToInt32(Request.QueryString["id"]) + "");
        if (check)
        {
            BindProductperbyid(Convert.ToInt32(Request.QueryString["id"]));
            lbladd.Text = "Updated Successfully";
        }

    }
    //protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    //{
    //    bool check = Class1.InsertUpdate("update PenSeriesTB set  PSeries_BannerImage='' where PSeries_Id=" + Convert.ToInt32(Request.QueryString["id"]) + "");
    //    if (check)
    //    {
    //        BindProductperbyid(Convert.ToInt32(Request.QueryString["id"]));
    //    }
    //}
    //protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    //{
    //    bool check = Class1.InsertUpdate("update PenSeriesTB set  PSeries_BannerFooterImage='' where PSeries_Id=" + Convert.ToInt32(Request.QueryString["id"]) + "");
    //    if (check)
    //    {
    //        BindProductperbyid(Convert.ToInt32(Request.QueryString["id"]));
    //    }
    //}
    protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
    {
        bool check = Class1.InsertUpdate("update PenSeriesTB set  PSeries_LogoImage='' where PSeries_Id=" + Convert.ToInt32(Request.QueryString["id"]) + "");
        if (check)
        {
            BindProductperbyid(Convert.ToInt32(Request.QueryString["id"]));
        }
    }
}

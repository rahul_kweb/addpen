<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="BusinessPartnerListAdmin.aspx.cs" Inherits="Admin_BusinessPartnerListAdmin" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%">
<tr>
<td align="center">
Business Partners List <hr />
</td>
</tr>
<tr>
<td>
<br />
</td>
</tr>
<tr>
<td align="center" style="height: 27px">
    &nbsp;
    <asp:RadioButtonList ID="RBLStatus" runat="server" AutoPostBack="True" 
        RepeatDirection="Horizontal" 
        onselectedindexchanged="RBLStatus_SelectedIndexChanged">
        <asp:ListItem Selected="True">Active</asp:ListItem>
        <asp:ListItem>InActive</asp:ListItem>
    </asp:RadioButtonList>
    </td>
    </tr>
    <tr>
    <td>
       
    <asp:DropDownList ID="DDLNatureOfPartners" runat="server" Height="22px" 
            Width="209px" AutoPostBack="True" DataTextField="BSP_NatureOfPartnership" 
            onselectedindexchanged="DDLNatureOfPartners_SelectedIndexChanged">
        <asp:ListItem Value="1">Distribution</asp:ListItem>
        <asp:ListItem Value="2">Retailer</asp:ListItem>
        <asp:ListItem Value="3">Export</asp:ListItem>
        <asp:ListItem Value="4">OnLine Sales</asp:ListItem>
        <asp:ListItem Value="5">Bulk Buying</asp:ListItem>
        <asp:ListItem Value="6">Franchisee</asp:ListItem>
    </asp:DropDownList>
</td>
</tr>
<tr>
<td align="center">
    <asp:Label ID="lblupdate" runat="server" Text=""></asp:Label>
</td>
</tr>
<tr>
<td colspan="2">
    <asp:GridView ID="GVBusinessPartners" Width="100%" runat="server" 
        AllowPaging="True" AutoGenerateColumns="False" 
        onpageindexchanging="GVBusinessPartners_PageIndexChanging" PageSize="30" 
        onrowdeleting="GVBusinessPartners_RowDeleting">
        <Columns>
            <asp:BoundField DataField="BSP_Id" HeaderText="ID" />
            <asp:TemplateField Visible="False">
                <EditItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("BSP_Id") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblid" runat="server" Text='<%# Bind("BSP_Id") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="BSP_Date" DataFormatString="{0:M-dd-yyyy}" 
                HeaderText="Date" />
            <asp:BoundField DataField="BSP_ContactPerson" HeaderText="Contact Person" />
            <asp:BoundField DataField="BSP_Designation" HeaderText="Designation" />
            <asp:BoundField DataField="BSP_CompanyName" HeaderText="Company Name" />
            <asp:BoundField DataField="BSP_Telephone" HeaderText="Telephone" />
            <asp:BoundField DataField="BSP_NatureOfPartnership" 
                HeaderText="Natur eOf Partnership" />
            <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:CheckBox ID="chkstatus" runat="server" 
                        Checked='<%# Eval("BSP_Status").ToString().Equals("Active") %>' 
                        oncheckedchanged="chkstatus_CheckedChanged" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:HyperLinkField DataNavigateUrlFields="BSP_Id" 
                DataNavigateUrlFormatString="BusinessPartnersDetails.aspx?id={0}" 
                Text="more..." />
            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" />
        </Columns>
    </asp:GridView>
</td>
</tr>
<tr>
<td align="center">
    <asp:Button ID="btnapply" runat="server" Text="Apply Change" 
        onclick="btnapply_Click" />
</td>
</tr>
</table>
</asp:Content>


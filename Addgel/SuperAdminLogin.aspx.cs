﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

public partial class Admin_SuperAdminLogin : System.Web.UI.Page
{
    bool[] rowChanged;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            string un = Session["Check"].ToString();
            string pass = Session["password"].ToString();

            DataTable supadmindt = Class1.GetRecord("select * from UserTab where UserID=1 ");
            string supusername = supadmindt.Rows[0]["UserName"].ToString();
            string suppass = supadmindt.Rows[0]["UserPassword"].ToString();

            DataTable dt = Class1.GetRecord("select * from UserTab where UserName='"+Session["Check"].ToString()+"' and UserPassword ='"+Session["password"].ToString()+"'");
            string username=dt.Rows[0]["UserName"].ToString();
            string password=dt.Rows[0]["UserPassword"].ToString();
            if (username == supusername && password == suppass)
            {
                supadmintb.Visible = true;
                admintb.Visible = false;
                lblupdate.Text = "";
                lbladd.Text = "";
                rowChanged = new bool[30];
                if (!IsPostBack)
                {
                    AdminBind("Active");


                }
            }
            else
            {
                supadmintb.Visible = false;
                admintb.Visible = true;
                        
            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }
    }

    private void AdminBind(string status)
    {
        //string decode = base64Decode(txtdecrypt.Text.Trim());
        //lbldecrypt.Text = decode;

        DataTable dt = Class1.GetRecord("select * from UserTab where UserStatus='"+status+"' and UserID not in(1)  order by UserID");
        GVAdmin.DataSource = dt;
        GVAdmin.DataBind();


    }
    protected void btnapply_Click(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsPostBack)
            {
                string IdList = "";
                int totalRows = 30;
                for (int r = 0; r < totalRows; r++)
                {
                    if (rowChanged[r])
                    {
                        GridViewRow thisGridViewRow = GVAdmin.Rows[r];


                        Label LId = (Label)thisGridViewRow.FindControl("lblid");
                        int NewId = Convert.ToInt32(LId.Text);

                        IdList = IdList + " " + NewId;

                        CheckBox chkdeal = (CheckBox)thisGridViewRow.FindControl("chkstatus");
                        string status = "InActive";

                        if (chkdeal.Checked)
                        {
                            status = "Active";
                        }

                        //int groupid = Convert.ToInt32(rbStatus.SelectedValue);
                        bool check = Class1.InsertUpdate("update UserTab set UserStatus='" + status + "' where UserID =" + NewId + "");

                    }
                }



                AdminBind(RBLStatus.SelectedItem.Text);
                if (IdList == "")
                {
                    lblupdate.Text = "No Change Occur";
                }
                else
                {
                    lblupdate.Text = "ID" + IdList + " Have been Updated";
                }
             }
        }
        catch (Exception ex)
        {
            lblupdate.Text = ex.Message;
        }
    }
    protected void chkstatus_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox thisTextBox = (CheckBox)sender;
            GridViewRow thisGridViewRow = (GridViewRow)thisTextBox.Parent.Parent;
            int row = thisGridViewRow.RowIndex;
            rowChanged[row] = true;
        }
        catch (Exception ex)
        {
            lblupdate.Text = ex.Message.ToString();
        }
    }

    protected void RBLStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        AdminBind(RBLStatus.SelectedItem.Text);

    }
    protected void GVAdmin_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(GVAdmin.Rows[e.RowIndex].Cells[0].Text);
        bool check = Class1.InsertUpdate("Delete from UserTab where UserID=" + id + "");
        AdminBind(RBLStatus.SelectedItem.Text);
    }
    protected void GVAdmin_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GVAdmin.EditIndex = e.NewEditIndex;
        AdminBind(RBLStatus.SelectedItem.Text);
    }
    protected void GVAdmin_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GVAdmin.EditIndex = -1;
        AdminBind(RBLStatus.SelectedItem.Text);
    }
    protected void GVAdmin_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string pk = ((Label)GVAdmin.Rows[GVAdmin.EditIndex].FindControl("Label1")).Text;

        string username = ((TextBox)GVAdmin.Rows[GVAdmin.EditIndex].FindControl("TextBox1")).Text;
        string password = ((TextBox)GVAdmin.Rows[GVAdmin.EditIndex].FindControl("TextBox2")).Text;
        string encode = base64Encode(password);
        string str = "update UserTab set UserName='" + username + "',UserPassword='" + encode + "' where UserID=" + pk + "";
        bool check = Class1.InsertUpdate(str);
        if (check == true)
        {
            GVAdmin.EditIndex = -1;
            AdminBind(RBLStatus.SelectedItem.Text);
        }
    }
    protected void btnadd_Click(object sender, EventArgs e)
    {
        string username = txtusername.Text.Replace("'", "''");
        string password = txtpassword.Text.Replace("'", "''");

        string encode = base64Encode(password);
        //EncriptionDecryption objencrypt = new EncriptionDecryption();
        //string encrypt= objencrypt.Encrypt(password);
        bool check = Class1.InsertUpdate("insert into UserTab values('" + username + "','" +encode+ "','Admin','InActive')");
        if (check)
        {
            txtusername.Text = "";
            txtpassword.Text = "";
            lbladd.Text = "<strong> New Admin Added Sucessfully</strong>";
        }
        AdminBind(RBLStatus.SelectedItem.Text);
    }
    private string base64Encode(string sData)
    {
        try
        {
            byte[] encData_byte = new byte[sData.Length];

            encData_byte = System.Text.Encoding.UTF8.GetBytes(sData);

            string encodedData = Convert.ToBase64String(encData_byte);

            return encodedData;

        }
        catch (Exception ex)
        {
            throw new Exception("Error in base64Encode" + ex.Message);
        }
    }
    public string base64Decode(string sData)
    {

        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(sData);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }









    
}

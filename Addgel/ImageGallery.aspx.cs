﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_ImageGallery : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            ImagesGallery();
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }
    }
    private void ImagesGallery()
    {
        DataTable dt = Class1.GetRecord("SELECT * FROM ProductImageGalleryTB where ProductId=" + Convert.ToInt32(Request.QueryString["id"]) +"");

        string[] name = new string[dt.Rows.Count];
        for (int i = 0; i <= dt.Rows.Count - 1; i++)
        {
            name[i] = "~/Addgel/AdminImage/" + dt.Rows[i]["Product_ZoomImage"].ToString();


            dt.Rows[i]["Product_ZoomImage"] = name[i];           

        }

        GVGallery.DataSource = dt;
        GVGallery.DataBind();
    }
    protected void GVGallery_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(GVGallery.Rows[e.RowIndex].Cells[0].Text);
        bool check = Class1.InsertUpdate("Delete from ProductImageGalleryTB where ProductImage_Id=" + id + "");

        ImagesGallery();
    }
}

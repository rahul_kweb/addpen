﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="VedioDetailsAdmin.aspx.cs" Inherits="Admin_VedioDetailsAdmin" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id=tbidvideolist runat="server" width="100%">
    <tr>
    <td style="width: 92px">
    Title
    </td>
    <td>
        <asp:Label ID="lbltitle1" runat="server" Text=""></asp:Label>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
     
    <tr>
    <td style="width: 92px">
    Description
    </td>
    <td>
    <asp:Label ID="lbldescription1" runat="server" Text=""></asp:Label>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
     
    <tr>
    <td style="width: 92px">
    URL
    </td>
    <td>
    <asp:Label ID="lblurl1" runat="server" Text=""></asp:Label>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
     
    <tr>
    <td style="width: 92px">
    Embed Code
    </td>
    <td>
    <asp:Label ID="lblembedcode1" runat="server" Text=""></asp:Label>
    </td>
    </tr>
    <tr>
<td colspan="2">
&nbsp;
</td>
</tr>

     <tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
<tr>
<td>
Download
</td>
<td>
    <asp:HyperLink ID="hypvideodownload" runat="server">Download</asp:HyperLink>
   </td> 
</tr>
<tr>
<td>
Image
</td>
<td>
    <asp:Image ID="Image1" runat="server" Height="100px" Width="100px" />
   </td> 
</tr>
<tr>
<td>
&nbsp;
</td>
<td>
    <asp:Button ID="btnedit" runat="server" Text="Edit" onclick="btnedit_Click" />
</td>
</tr>
    </table>
    <table id="tbidupdatevideo" runat="server" width="100%">
<tr>
<td colspan="2" align="center">
    <asp:Label ID="lblupdate" runat="server" Text=""></asp:Label>
</td>
</tr>
<tr>
<td style="width: 134px">
Title
</td>
<td>
    <asp:TextBox ID="txttitle" runat="server" Height="17px" Width="259px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
        ControlToValidate="txttitle" ErrorMessage="Field cannot be blank" 
        ValidationGroup="1"></asp:RequiredFieldValidator>
</td>
</tr>
<tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
<tr>
<td style="width: 134px">
Description
</td>
<td>
    <asp:TextBox ID="txtdescription" TextMode="MultiLine" runat="server" Height="83px" 
        Width="259px"></asp:TextBox>
</td>
</tr>
<tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
<tr>
<td style="width: 134px">
URL 
</td>
<td>
    <asp:TextBox ID="txturl" TextMode="MultiLine" runat="server" Height="52px" 
        Width="259px"></asp:TextBox>
</td>
</tr>
<tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
<tr>
<td style="width: 134px">
Embed Code
</td>
<td>
 <asp:TextBox ID="txtembedcode" TextMode="MultiLine" runat="server" Height="91px" 
        Width="259px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
        ControlToValidate="txtembedcode" ErrorMessage="Field cannot be blank" 
        ValidationGroup="1"></asp:RequiredFieldValidator>
</td>
</tr>
<tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
<tr>
<td style="width: 134px">
    Upload Video
</td>
<td>
    <asp:FileUpload ID="FUVideo" runat="server" />
</td>
</tr>

<tr>
<td style="width: 134px">
    Upload Image
</td>
<td>
    <asp:FileUpload ID="FUImage" runat="server" />
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
     ControlToValidate="FUImage" Display="None" 
            ErrorMessage="Please Add Only Image "            ValidationExpression="^.+\.((jpg)|(gif)|(jpeg)|(png)|(bmp))$"> </asp:RegularExpressionValidator>
    <br />
</td>
</tr>
<tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
<tr>
<td colspan="2" align="center">
    <asp:Button ID="btnaddvedio" runat="server" Text="Update" 
        onclick="btnaddvedio_Click" ValidationGroup="1"/>
</td>
</tr>
<tr>
<td colspan="2">
&nbsp;
</td>
</tr>
<tr>
<td colspan="2">
 <asp:HiddenField ID="HiddenField1" runat="server" />
                <asp:HiddenField ID="hfvideo" runat="server" />
    <asp:HiddenField ID="hfimage" runat="server" />
    <br />
</td>
</tr>
<tr>
<td colspan="2" align="center">
    &nbsp;</td>
</tr>

</table>
</asp:Content>


﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="JobsOpeningAddAdmin.aspx.cs" Inherits="Admin_JobsOpeningAddAdmin" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%">
<tr>
<td colspan="2" align="center">

    </td>
</tr>
<tr>
<td colspan="2" align="center">
  <strong>  <asp:Label ID="lbladd" runat="server" Text=""></asp:Label></strong>
</td>
</tr>
<tr>
<td style="width: 148px">
    Post Vacant&nbsp; *</td>
<td>
    <asp:TextBox ID="txttitle" runat="server" Width="439px"></asp:TextBox>
</td>
</tr>
<tr>
<td style="width: 148px">
&nbsp;Company Profile *</td>
<td>
<asp:TextBox ID="txtprimaryskills" runat="server" Width="439px" Height="100px" 
        TextMode="MultiLine"></asp:TextBox>
</td>
</tr>
<tr>
<td style="width: 148px">
Locations&nbsp; *
</td>
<td>
<asp:TextBox ID="txtlocation" runat="server" Width="439px"></asp:TextBox>
</td>
</tr>
<tr>
<td style="width: 148px">
    Job Description/<br />
    Responsibility&nbsp;&nbsp;&nbsp;&nbsp; *</td>
<td>
<asp:TextBox ID="txtdescription" runat="server" Height="100px" TextMode="MultiLine" 
        Width="439px"></asp:TextBox>
</td>
</tr>
<tr>
<td style="width: 148px">
    Desired profile of<br />
    candidate</td>
<td>
<asp:TextBox ID="txtskills" runat="server" Height="100px" TextMode="MultiLine" 
        Width="439px"></asp:TextBox>
</td>
</tr>
<tr>
<td style="width: 148px">
Experience&nbsp; *
</td>
<td>
<asp:TextBox ID="txtexperience" runat="server" Width="439px"></asp:TextBox>
</td>
</tr>
<tr>
<td style="width: 148px">
Salary&nbsp; *</td>
<td>
<asp:TextBox ID="txtsalary" runat="server" Width="439px"></asp:TextBox>
</td>
</tr>
<tr>
<td style="width: 148px">
    Number Of Vacancy
</td>
<td>
<asp:TextBox ID="txtrequirement" runat="server" 
        Width="439px"></asp:TextBox>
</td>
</tr>
<tr>
<td colspan="2" align="center">
    <asp:Button ID="btnadd" runat="server" Text="ADD Jobs" onclick="btnadd_Click" />
</td>
</tr>
</table>
</asp:Content>


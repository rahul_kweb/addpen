﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_CategoryReorderAdmmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            lblreodercategroy.Text = "";
            if (!IsPostBack)
            {
                BindCategory(1);
            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }
    }
    private void BindCategory(int id)
    {
        DataTable dt = Class1.GetRecord("select * from CategoryTB where C_ParentId=" + id + " and C_CatType=" + id + " order by COrderId");
        ListBox1.DataSource = dt;
        ListBox1.DataBind();
    }
    protected void btnup_Click(object sender, EventArgs e)
    {        
        int SelectedIndex = ListBox1.SelectedIndex;

        if (SelectedIndex == -1) // nothing selected
            return;
        if (SelectedIndex == 0) // already at top of list  
            return;

        ListItem Temp;
        Temp = ListBox1.SelectedItem;

        ListBox1.Items.Remove(ListBox1.SelectedItem);
        ListBox1.Items.Insert(SelectedIndex - 1, Temp);
    }
    protected void btndown_Click(object sender, EventArgs e)
    {        
        int SelectedIndex = ListBox1.SelectedIndex;

        if (SelectedIndex == -1)  // nothing selected
            return;
        if (SelectedIndex == ListBox1.Items.Count - 1)  // already at top of list            
            return;

        ListItem Temp;
        Temp = ListBox1.SelectedItem;

        ListBox1.Items.Remove(ListBox1.SelectedItem);
        ListBox1.Items.Insert(SelectedIndex + 1, Temp);
    }
 
  
    protected void btnsave_Click(object sender, EventArgs e)
    {
        for (int i = 0; i <= ListBox1.Items.Count - 1; i++)
        {
            int cid = (Convert.ToInt32(ListBox1.Items[i].Value));
            bool check = Class1.InsertUpdate("update CategoryTB set COrderId=" + i + " where C_Id= " + cid + "");
        }
        lblreodercategroy.Text = "Re-Order Category Sucessfully";
    }
    protected void RBLReorder_SelectedIndexChanged(object sender, EventArgs e)
    {  
        BindCategory(Convert.ToInt32(RBLReorder.SelectedValue));
    }
}

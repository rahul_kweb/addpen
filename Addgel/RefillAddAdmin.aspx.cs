﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_RefillAddAdmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            if (!IsPostBack)
            {
                CategoryBind();
                Bindinkcolor();
                DDLCategory.Items.Insert(0, new ListItem("-----------Select-----------"));
                PenCategoryBind();
                DDLRefilecategory.Items.Insert(0, new ListItem("-----------Select-----------"));
            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }
    }
    private void CategoryBind()
    {
        DataTable dt = Class1.GetRecord("select * from CategoryTB where C_CatType=2 and C_ParentId=2");
        DDLCategory.DataSource = dt;
        DDLCategory.DataBind();
    }

    private void PenCategoryBind()
    {
        DataTable dt = Class1.GetRecord("select * from CategoryTB where C_CatType=1 and C_ParentId=1");
        DDLRefilecategory.DataSource = dt;
        DDLRefilecategory.DataBind();
    }

    private void Bindinkcolor()
    {
        DataTable dt = Class1.GetRecord("select * from InkColorMTB");
        ListBox1.DataSource = dt;
        ListBox1.DataBind();
        //DataTable dt=Class1.GetRecord("select * from InkColorMTB");
        //string newproductbody="";   


        //for (int i = 0; i <= dt.Rows.Count-1 ; i++)
        //{            
        //    newproductbody = newproductbody + " <input id=Checkbox"+i+" value='" + dt.Rows[i]["InkColorM_Code"].ToString() + "' name=''  type='checkbox' />" + dt.Rows[i]["InkColorM_Name"].ToString() + " &nbsp;";
        //}


        // lblinkcolor.Text = newproductbody;
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        int productid = Class1.MaxRefillId() + 1;

        string extensionI1 = System.IO.Path.GetExtension(FUthumbnail.FileName);
        string extensionI2 = System.IO.Path.GetExtension(FUlarge.FileName);
        string extensionI3 = System.IO.Path.GetExtension(FUzoom.FileName);
        string extensionI4 = System.IO.Path.GetExtension(FUpdf.FileName);
        string extensionI5 = System.IO.Path.GetExtension(FUBannerimg.FileName);

        string proname = txtproductname.Text.Replace("'", "''");
        string prohead = txthead.Text.Replace("'", "''");
        string prodisc = txtdescription.Text.Replace("'", "''");
        string profeature = txtfeature.Text.Replace("'", "''");
        string proprice = txtprices.Text.Replace("'", "''");
        string search = DDLCategory.SelectedItem.Text + "," + txtproductname.Text.Replace("'", "''");


        string sqlvalue = "";
       
        
            for (int i = 0; i < ListBox1.Items.Count; i++)
            {
                if (ListBox1.Items[i].Selected == true)
                {
                    if (i == ListBox1.Items.Count - 1)
                    {
                        sqlvalue += ListBox1.Items[i].Value;
                    }
                    else
                    {
                        sqlvalue += ListBox1.Items[i].Value + ",";
                    }
                }
            }

            string sqlvalue1 = sqlvalue;
             if (sqlvalue1 !="")
             {
                 if (sqlvalue1.Substring(sqlvalue1.Length - 1, 1) == ",")
                 {
                     sqlvalue1 = sqlvalue1.Substring(0, sqlvalue1.Length - 1);
                 }
             }
        

        string penid = "";
        for (int i = 0; i < lstpen.Items.Count; i++)
        {
            if (lstpen.Items[i].Selected == true)
            {
                if (i == lstpen.Items.Count - 1)
                {
                    penid += lstpen.Items[i].Value;
                }
                else
                {
                    penid += lstpen.Items[i].Value + ",";
                }
            }
        }

        string penid1 = penid;
        if (penid1 != "")
        {
            if (penid1.Substring(penid1.Length - 1, 1) == ",")
            {
                penid1 = penid1.Substring(0, penid1.Length - 1);
            }
        }
                        

        string prthumnail = string.Empty;
        string prolarge = string.Empty;
        string prozoom = string.Empty;
        string propdf = string.Empty;
        string probanner = string.Empty;


        if (extensionI1 != "")
        {
            prthumnail = "refillthum" + productid + "" + extensionI1;
        }
        else
        {
            prthumnail = "";
        }
        if (extensionI2 != "")
        {
            prolarge = "refilllarge" + productid + "" + extensionI2;
        }
        else
        {
            prolarge = "";
        }

        if (extensionI3 != "")
        {
            prozoom = "refillzoom" + productid + "" + extensionI3;
        }
        else
        {
            prozoom = "";
        }

        if (extensionI4 != "")
        {
            propdf = "refillpdf" + productid + "" + extensionI4;
        }
        else
        {
            propdf = "";
        }

        if (extensionI5 != "")
        {
            probanner = "refillbanner" + productid + "" + extensionI5;
        }
        else
        {
            probanner = "";
        }

        string date = DateTime.Today.ToShortDateString();
        bool check = Class1.InsertUpdate("insert into RefillTB values('" + proname + "','" + prohead + "','" + prodisc + "','" + profeature + "','" + DDLCategory.SelectedItem.Text + "'," + Convert.ToInt32(DDLCategory.SelectedValue) + ",'" + penid1 + "','" + sqlvalue1 + "','" + search + "','" + probanner + "','" + prthumnail + "','" + prolarge + "','" + prozoom + "','" + propdf + "','Active','Active','" + proprice + "','" + date + "')");

        int productid2 = Class1.MaxRefillId();
        bool chkmultiimg = Class1.InsertUpdate("insert into RefillImageGalleryTB values(" + productid2 + ",'" + prthumnail + "','" + prozoom + "','" + date + "')");



        int pid = Convert.ToInt32(DDLCategory.SelectedValue);
        string navigation = string.Empty;
        if (pid == 7)
        {
            navigation = "product_ballrollerrefilldetails.aspx";
        }
        else if (pid == 8)
        {
            navigation = "product_exclusivrefilldetails.aspx";
        }
        else if (pid == 9)
        {
            navigation = "product_gelrefilldetails.aspx";
        }
        else if (pid == 10)
        {
            navigation = "product_gelrolloverrefilldetails.aspx";
        }
        else if (pid == 17)
        {
            navigation = "product_liquidrefilldetail.aspx";
        }


        int productid1 = Class1.MaxRefillId();

        bool categoryproname = Class1.InsertUpdate("insert into CategoryTB values('" + proname + "'," + productid1 + "," + Convert.ToInt32(DDLCategory.SelectedValue) + ",2,'Child','','" + navigation + "','','Active','" + date + "',000)");

        if (extensionI1 != "")
        {

            FUthumbnail.SaveAs(Server.MapPath("adminImage\\" + prthumnail + ""));
        }

        if (extensionI2 != "")
        {

            FUlarge.SaveAs(Server.MapPath("adminImage\\" + prolarge + ""));
        }

        if (extensionI3 != "")
        {

            FUzoom.SaveAs(Server.MapPath("adminImage\\" + prozoom + ""));
        }

        if (extensionI4 != "")
        {

            FUpdf.SaveAs(Server.MapPath("DocFile\\" + propdf + ""));
        }
        if (extensionI5 != "")
        {
            FUBannerimg.SaveAs(Server.MapPath("adminImage\\" + probanner + ""));
        }

        txtproductname.Text = "";
        txthead.Text = "";
        txtdescription.Text = "";
        txtfeature.Text = "";
        txtprices.Text = "";
        DDLCategory.SelectedIndex = 0;
        Response.Write("<script>alert('Refill product added sucessfully')</script>");
        
    }

    protected void btnnewline_Click(object sender, EventArgs e)
    {
        txtfeature.Text = txtfeature.Text + "#";
        txtfeature.Focus();
    }
    protected void DDLRefilecategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLRefilecategory.SelectedItem.Text != "-----------Select-----------")
        {
            DataTable dt = Class1.GetRecord("select * from CategoryTB where C_ParentId=" + Convert.ToInt32(DDLRefilecategory.SelectedValue) + "");

            if (dt.Rows.Count > 0)
            {
                lstpen.DataSource = dt;
                lstpen.DataBind();

            }
            else
            {
                lstpen.Items.Clear();
            }
        }
        else
        {
            Response.Write("<script>alert('Please select pen category')</script>");
            lstpen.Items.Clear();
        }
    }
}

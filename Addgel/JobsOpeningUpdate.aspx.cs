﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_JobsOpeningUpdate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    int id = Convert.ToInt32(Request.QueryString["id"]);
                    BindJobsPerId(id);
                }
            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }

    }
    private void BindJobsPerId(int id)
    {
        DataTable dt = Class1.GetRecord("select * from JobsTB where Jobs_id=" + id + "");
        txttitle.Text = dt.Rows[0]["Jobs_Title"].ToString();
        txtprimaryskills.Text = dt.Rows[0]["Jobs_PrimarySkills"].ToString();
        txtlocation.Text = dt.Rows[0]["Jobs_Location"].ToString();
        txtdescription.Text = dt.Rows[0]["Jobs_Description"].ToString();
        txtskills.Text = dt.Rows[0]["Jobs_Skills"].ToString();
        txtexperience.Text = dt.Rows[0]["Jobs_Experience"].ToString();
        txtsalary.Text = dt.Rows[0]["Jobs_Salary"].ToString();
        txtrequirement.Text = dt.Rows[0]["Jobs_Requirement"].ToString();

        txttitle.ReadOnly = true;
        txtprimaryskills.ReadOnly = true;
        txtlocation.ReadOnly = true;
        txtdescription.ReadOnly = true;
        txtskills.ReadOnly = true;
        txtexperience.ReadOnly = true;
        txtsalary.ReadOnly = true;
        txtrequirement.ReadOnly = true;
        btnupdate.Visible = false;
    }
    protected void btnedit_Click(object sender, EventArgs e)
    {
        btnupdate.Visible = true;
        btnedit.Visible = false;

        txttitle.ReadOnly = false;
        txtprimaryskills.ReadOnly = false;
        txtlocation.ReadOnly = false;
        txtdescription.ReadOnly = false;
        txtskills.ReadOnly = false;
        txtexperience.ReadOnly = false;
        txtsalary.ReadOnly = false;
        txtrequirement.ReadOnly = false;      
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        string title = txttitle.Text.Replace("'", "''");
        string primaryskill = txtprimaryskills.Text.Replace("'", "''");
        string location = txtlocation.Text.Replace("'", "''");
        string description = txtdescription.Text.Replace("'", "''");
        string skills = txtskills.Text.Replace("'", "''");
        string Experience = txtexperience.Text.Replace("'", "''");
        string salary = txtsalary.Text.Replace("'", "''");
        string requirement = txtrequirement.Text.Replace("'", "''");
        string date = DateTime.Today.ToShortDateString();

        bool check = Class1.InsertUpdate("update JobsTB set Jobs_Title='" + title + "',Jobs_PrimarySkills='" + primaryskill + "',Jobs_Location='" + location + "',Jobs_Description='" + description + "',Jobs_Skills='" + skills + "',Jobs_Experience='" + Experience + "',Jobs_Salary='" + salary + "',Jobs_Requirement='" + requirement + "',Jobs_Date='" + date + "' where Jobs_id=" + Convert.ToInt32(Request.QueryString["id"]) + "");
        lblupdate.Text = "Jobs with title " + title + " updated successfully";
    }
}

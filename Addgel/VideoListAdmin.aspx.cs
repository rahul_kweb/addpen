﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_VideoListAdmin : System.Web.UI.Page
{
    bool[] rowChanged;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            lblupdate.Text = "";
            rowChanged = new bool[10];
            if (!IsPostBack)
            {
                BindGVVedio("Active");
            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }
    }
    private void BindGVVedio(string status)
    {
        DataTable dt = Class1.GetRecord("select * from VideoTB where Status='"+status+"'");
        GVVedio.DataSource = dt;
        GVVedio.DataBind();
    }

    protected void GVVedio_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(GVVedio.Rows[e.RowIndex].Cells[0].Text);
        bool check = Class1.InsertUpdate("delete from VideoTB where Id="+id+"");
        BindGVVedio(DDLStatus.SelectedItem.Text);
    }
    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            RadioButtonList thisTextBox = (RadioButtonList)sender;
            GridViewRow thisGridViewRow = (GridViewRow)thisTextBox.Parent.Parent;
            int row = thisGridViewRow.RowIndex;
            rowChanged[row] = true;
        }
        catch (Exception ex)
        {
            lblupdate.Text = ex.Message.ToString();
        }
    }
    protected void GVVedio_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GVVedio.PageIndex = e.NewPageIndex;
        BindGVVedio(DDLStatus.SelectedItem.Text);
    }
    protected void DDLStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGVVedio(DDLStatus.SelectedItem.Text);
    }
    protected void btnapplychange_Click(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsPostBack)
            {
                string IdList = "";
                int totalRows = 10;
                for (int r = 0; r < totalRows; r++)
                {
                    if (rowChanged[r])
                    {
                        GridViewRow thisGridViewRow = GVVedio.Rows[r];


                        Label LId = (Label)thisGridViewRow.FindControl("Label1");
                        int NewId = Convert.ToInt32(LId.Text);

                        IdList = IdList + " " + NewId;

                        RadioButtonList rbStatus = (RadioButtonList)thisGridViewRow.FindControl("RadioButtonList1");

                        string ChangeStatus = rbStatus.Text.Trim();
                        //bool check = New.NewsUpdate(ChangeStatus, NewId);
                        
                        bool check =Class1.InsertUpdate("update VideoTB set Status='" + ChangeStatus + "',Date=getdate() where ID=" + NewId+"");
                       
                    }
                }

                BindGVVedio(DDLStatus.SelectedItem.Text);
                if (IdList == "")
                {
                    lblupdate.Text = "No Change Occur";
                }
                else
                {
                    lblupdate.Text = "ID" + IdList + " Have been Updated";
                }

            }
        }
        catch (Exception ex)
        {
            lblupdate.Text = ex.Message.ToString();
        }
    }
}

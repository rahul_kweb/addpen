﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="SeriesDetailsupdate.aspx.cs" Inherits="Admin_SeriesDetailsupdate" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%">
       <tr>
<td colspan="2" align="center">
    <b>Pen Series Add   </b><hr />
</td>
</tr>   
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="lbladd" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        
          <tr>
            <td style="width: 124px">
                Series Head
                </td>
            <td>
                <asp:TextBox ID="txthead" runat="server" Height="21px" Width="313px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                    ControlToValidate="txthead" ErrorMessage="Field cannot be blank" 
                    ValidationGroup="1"></asp:RequiredFieldValidator>
                </td>
        </tr>
        <tr>
            <td style="width: 130px">
                Description</td>
            <td>
                <asp:TextBox ID="txtdescription" TextMode="MultiLine" runat="server" 
                    Height="200px" Width="400px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="txtdescription" ErrorMessage="Field cannot be blank" 
                    ValidationGroup="1"></asp:RequiredFieldValidator>
                </td>
        </tr>
        
        
        
        
        <tr>
        <td style="width: 130px; height: 33px;">
           Banner Image</td>
        <td style="height: 33px">
            <asp:FileUpload ID="FUlarge" runat="server" />
            <asp:RegularExpressionValidator 
ID="RegularExpressionValidator2" runat="server" ControlToValidate="FUlarge" Display="None" 
 ErrorMessage="Please Add Only Image " ValidationExpression="^.+\.((jpg)|(gif)|(jpeg)|(png)|(bmp))$">
</asp:RegularExpressionValidator>
        </td>
        </tr>
        
        
        
        <tr>
        <td style="width: 130px; height: 33px;">
          Banner Footer Image</td>
        <td style="height: 33px">
            <asp:FileUpload ID="FUBannerFooter" runat="server" />
            <asp:RegularExpressionValidator 
ID="RegularExpressionValidator1" runat="server" ControlToValidate="FUBannerFooter" Display="None" 
 ErrorMessage="Please Add Only Image " ValidationExpression="^.+\.((jpg)|(gif)|(jpeg)|(png)|(bmp))$">
</asp:RegularExpressionValidator>
        </td>
        </tr>
        
        <tr>
        <td style="width: 130px; height: 33px;">
            Logo</td>
        <td style="height: 33px">
            <asp:FileUpload ID="FULogo" runat="server" />
            <asp:RegularExpressionValidator 
ID="RegularExpressionValidator3" runat="server" ControlToValidate="FULogo" Display="None" 
 ErrorMessage="Please Add Only Image " ValidationExpression="^.+\.((jpg)|(gif)|(jpeg)|(png)|(bmp))$">
</asp:RegularExpressionValidator>
        </td>
        </tr>
        
        
        
        <tr>
            <td colspan="2" align="center">
                <asp:Button ID="btnadd" runat="server" Text="Update" onclick="btnadd_Click" 
                    style="height: 26px" ValidationGroup="1" />
                    
                      </td>
        </tr>
        <tr>
        <td colspan="2">
        <br />
        </td>
        </tr>
        <tr>
        <td colspan="2">
        <table>
        <tr>
        <td>
          <asp:Image ID="Image1" runat="server" /></td>
          <td>
              <%--<asp:ImageButton ID="ImageButton1" runat="server" OnClientClick="javascript : return confirm('Do you really want to \ndelete the image?');"

                  ImageUrl="~/images/icon_delete.gif" Width="20px" onclick="ImageButton1_Click"  />--%> </td>
          </tr>
          <tr>
          <td>
            <asp:Image ID="Image2" runat="server" /></td>
            <td>
              <%--<asp:ImageButton ID="ImageButton2" runat="server" OnClientClick="javascript : return confirm('Do you really want to \ndelete the image?');"

                    ImageUrl="~/images/icon_delete.gif" Width="20px" onclick="ImageButton2_Click" />--%> </td>
            </tr>
            <tr>
            <td>
            <asp:Image ID="Image3" runat="server" />
        </td>
        <td>
              <asp:ImageButton ID="ImageButton3" runat="server" OnClientClick="javascript : return confirm('Do you really want to \ndelete the image?');"

                  ImageUrl="~/images/icon_delete.gif" Width="20px" onclick="ImageButton3_Click" /> </td>
        </tr>
        </table>
          
        </td>
        </tr>
        <tr>
        <td colspan="2">
        
            <asp:HiddenField ID="HiddenField1" runat="server" />
            <asp:HiddenField ID="hfbannerimg" runat="server" />
            <asp:HiddenField ID="hfbannerfooter" runat="server" />
            <asp:HiddenField ID="hflogo" runat="server" />
            <br />
        
        </td>
        </tr>
    </table>
</asp:Content>


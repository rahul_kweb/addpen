﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_ApplyJobsListAdmin : System.Web.UI.Page
{
    bool[] rowChanged;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {

            LblApplicationMessage.Text = "";
            rowChanged = new bool[10];
            if (!IsPostBack)
            {
                JobApplicationBind("UnCheck");
                BtnMode.Text = "Non-Edit Mode";
                BtnSave.Visible = false;
            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }

    }

    private void JobApplicationBind(string status)
    {
        GVJobApplication.DataSource = Class1.GetRecord("select * from ApplyJobsTB where ApplyJobs_Status='" + status + "'");
        GVJobApplication.DataBind();
        if (GVJobApplication.Rows.Count == 0)
        {
            BtnMode.Text = "Non-Edit Mode";
            BtnSave.Visible = false;

        }


    }

    protected void GVJobApplication_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GVJobApplication.PageIndex = e.NewPageIndex;
        JobApplicationBind(DDLStatus.SelectedItem.Text.Trim());

    }
    protected void BtnMode_Click(object sender, EventArgs e)
    {
        JobApplicationBind(DDLStatus.SelectedItem.Text.Trim());

        if (BtnMode.Text == "Edit Mode")
        {
            BtnMode.Text = "Non-Edit Mode";
            BtnSave.Visible = false;
        }
        else
        {
            if (GVJobApplication.Rows.Count > 0)
            {
                BtnMode.Text = "Edit Mode";
                BtnSave.Visible = true;
            }
            else
            {
                LblApplicationMessage.Text = "No Records";
            }
        }

    }
    protected void DDLStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLStatus.SelectedItem.Text == "All")
        {
            GVJobApplication.DataSource = Class1.GetRecord("select * from ApplyJobsTB");
            GVJobApplication.DataBind();
            BtnMode.Visible = false;
        }
        else
        {
            JobApplicationBind(DDLStatus.SelectedItem.Text.Trim());
            BtnMode.Visible = true;
        }

    }
    protected void BtnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            string IdList = "";
            int totalRows = 10;
            for (int r = 0; r < totalRows; r++)
            {
                if (rowChanged[r])
                {
                    GridViewRow thisGridViewRow = GVJobApplication.Rows[r];


                    Label LId = (Label)thisGridViewRow.FindControl("LblId");
                    int ApplicationId = Convert.ToInt32(LId.Text);

                    IdList = IdList + " " + ApplicationId;

                    DropDownList DStatus = (DropDownList)thisGridViewRow.FindControl("DDLChangeStatus");

                    string ChangeStatus = DStatus.Text.Trim();

                    bool update = Class1.InsertUpdate("update ApplyJobsTB set ApplyJobs_Status='" + ChangeStatus + "' where ApplyJobs_Id=" + ApplicationId+"");
                    
                }
            }

            JobApplicationBind(DDLStatus.SelectedItem.Text.Trim());
            if (IdList == "")
            {
                LblApplicationMessage.Text = "No Change Occur";
            }
            else
            {
                LblApplicationMessage.Text = "ID" + IdList + " Have been Updated";
            }

        }

    }
    protected void DDLChangeStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList thisTextBox = (DropDownList)sender;
        GridViewRow thisGridViewRow = (GridViewRow)thisTextBox.Parent.Parent;
        int row = thisGridViewRow.RowIndex;
        rowChanged[row] = true;
    }
    protected void GVJobApplication_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            String FilePath;
            String FileName;
            FileName = ((HyperLink)e.Row.Cells[5].FindControl("HLFileName")).Text;
            FilePath = "~/Addgel/DocFile/" + FileName;
            ((HyperLink)e.Row.Cells[5].FindControl("HLFileName")).NavigateUrl = FilePath;

        }


    }


    protected void GVJobApplication_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(GVJobApplication.Rows[e.RowIndex].Cells[0].Text);
        bool check = Class1.InsertUpdate("Delete from ApplyJobsTB where ApplyJobs_Id=" + id + "");

        JobApplicationBind(DDLStatus.SelectedItem.Text.Trim());
    }
}

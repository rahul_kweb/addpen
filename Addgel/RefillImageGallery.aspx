﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="RefillImageGallery.aspx.cs" Inherits="Admin_RefillImageGallery" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%">
<tr>
<td align="center"><b>
Refill Images Gallery</b> <hr />
</td>
</tr>
<tr>
<td>
    <asp:GridView ID="GVGallery" runat="server" AutoGenerateColumns="False" 
        EmptyDataText="No Images Found" onrowdeleting="GVGallery_RowDeleting" 
       >
        <Columns>
            <asp:BoundField DataField="RefillImage_Id" ReadOnly="True">
                <ControlStyle Height="50px" Width="50px" BackColor="Black" 
                    BorderColor="Black" />
                <ItemStyle Height="50px" Width="50px" BackColor="Black" BorderColor="Black" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Images">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" 
                        Text='<%# Eval("Refill_ZoomImage") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" 
                        ImageUrl='<%# Eval("Refill_ZoomImage") %>' />
                </ItemTemplate>
                <ControlStyle Height="300px" Width="300px" />
                <ItemStyle Height="300px" Width="300px" />
            </asp:TemplateField>
            <asp:CommandField ShowDeleteButton="True" />
        </Columns>
    </asp:GridView>
</td>

</tr>
</table>
</asp:Content>


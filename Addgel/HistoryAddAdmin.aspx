﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" EnableViewState="false" ValidateRequest="false" AutoEventWireup="true" CodeFile="HistoryAddAdmin.aspx.cs" Inherits="HistoryAddAdmin" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%">
<tr>
<td align="center" colspan="2">
Add History <hr />
</td>
</tr>
<tr>
<td colspan="2">
<br />

</td>
</tr>
<tr>
<td colspan="2" align="center">
    <asp:Label ID="lbladd" runat="server" Text=""></asp:Label>

</td>
</tr>
<tr>
<td width="100px">
Year :- 
</td>
<td>
    <asp:TextBox ID="txtyear" Width="200px" runat="server"></asp:TextBox>
</td>
</tr>
<tr>
<td width="100px">
Description :- 
</td>
<td>
    <asp:TextBox ID="txtdescription" Width="200px" Height="200px"  runat="server" TextMode="MultiLine"></asp:TextBox>
</td>
</tr>
<tr>
<td colspan="2" align="center">
    <asp:Button ID="btnadd" runat="server" Text="Add History" 
        onclick="btnadd_Click" />
</td>
</tr>
</table>
</asp:Content>


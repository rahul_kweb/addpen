﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_GiftingDeatilsUpdate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            if (!IsPostBack)
            {
                BindGiftPerById();
            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }
    }
    private void BindGiftPerById()
    {
        DataTable dt = Class1.GetRecord("select * from GiftingIdeasTB where GI_id=" + Request.QueryString["id"] + "");
        txtproductname.Text = dt.Rows[0]["GI_Title"].ToString();
        txtdescription.Text = dt.Rows[0]["GI_Description"].ToString();
        Image1.ImageUrl = "~/Addgel/AdminImage/" + dt.Rows[0]["GI_Thumbnail"].ToString();
        Image2.ImageUrl = "~/Addgel/AdminImage/" + dt.Rows[0]["GI_Zoom"].ToString();
        hfthumbimg.Value = dt.Rows[0]["GI_Thumbnail"].ToString();
        hfzoomimg.Value = dt.Rows[0]["GI_Zoom"].ToString();
        
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
         
         string prthumnail = "";       
        string prozoom = "";    
               
        
        if (FUthumbnail.HasFile == true)
        {
            prthumnail = "GiftingThumnail" + HiddenField1.Value + "" + System.IO.Path.GetExtension(FUthumbnail.FileName);
            FUthumbnail.SaveAs(Server.MapPath("adminImage\\" + prthumnail + ""));
        }
        else
        {
            prthumnail = hfthumbimg.Value;
        }

        if (FUzoom.HasFile == true)
        {
            prozoom = "GiftingZoom" + HiddenField1.Value + "" + System.IO.Path.GetExtension(FUzoom.FileName);

            FUzoom.SaveAs(Server.MapPath("adminImage\\" + prozoom + ""));
        }
        else
        {
            prozoom = hfzoomimg.Value;
        }


        bool check = Class1.InsertUpdate("update GiftingIdeasTB set GI_Title='" + txtproductname.Text.Replace("'", "''") + "',GI_Description='" + txtdescription.Text.Replace("'", "''") + "',GI_Thumbnail='" + prthumnail + "' ,GI_Zoom='" + prozoom + "' where GI_id=" + Convert.ToInt32(Request.QueryString["id"]) + "");
        
        lblupdate.Text = "Updated Successfully";
        BindGiftPerById();
    
    }
}

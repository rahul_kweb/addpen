﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_BusinessPartnerListAdmin : System.Web.UI.Page
{
    bool[] rowChanged;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
        lblupdate.Text = "";
        rowChanged = new bool[10];
        if (!IsPostBack)
        {
            BusinessPartnersBind("Active", "Distribution");

        }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }

    }
    private void BusinessPartnersBind(string status,string natureofpartners)
    {
        DataTable dt = Class1.GetRecord("select * from BusinessPartnerTB where BSP_Status='" + status + "' and BSP_NatureOfPartnership='"+natureofpartners+"'");
        GVBusinessPartners.DataSource = dt;
        GVBusinessPartners.DataBind();


    }
    protected void GVBusinessPartners_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GVBusinessPartners.PageIndex = e.NewPageIndex;
        BusinessPartnersBind(RBLStatus.SelectedItem.Text,DDLNatureOfPartners.SelectedItem.Text);
    }
    protected void btnapply_Click(object sender, EventArgs e)
    {
        try
        {

            if (Page.IsPostBack)
            {
                string IdList = "";
                int totalRows = 10;
                for (int r = 0; r < totalRows; r++)
                {
                    if (rowChanged[r])
                    {
                        GridViewRow thisGridViewRow = GVBusinessPartners.Rows[r];


                        Label LId = (Label)thisGridViewRow.FindControl("lblid");
                        int NewId = Convert.ToInt32(LId.Text);

                        IdList = IdList + " " + NewId;

                        CheckBox chkdeal = (CheckBox)thisGridViewRow.FindControl("chkstatus");
                        string status = "InActive";

                        if (chkdeal.Checked)
                        {
                            status = "Active";
                        }

                        //int groupid = Convert.ToInt32(rbStatus.SelectedValue);
                        bool check = Class1.InsertUpdate("update BusinessPartnerTB set BSP_Status='" + status + "' where BSP_Id =" + NewId + "");

                    }
                }



                BusinessPartnersBind(RBLStatus.SelectedItem.Text, DDLNatureOfPartners.SelectedItem.Text);
                if (IdList == "")
                {
                    lblupdate.Text = "No Change Occur";
                }
                else
                {
                    lblupdate.Text = "ID" + IdList + " Have been Updated";
                }




            }
        }
        catch (Exception ex)
        {
            lblupdate.Text = ex.Message;
        }
    }
    protected void chkstatus_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox thisTextBox = (CheckBox)sender;
            GridViewRow thisGridViewRow = (GridViewRow)thisTextBox.Parent.Parent;
            int row = thisGridViewRow.RowIndex;
            rowChanged[row] = true;
        }
        catch (Exception ex)
        {
            lblupdate.Text = ex.Message.ToString();
        }
    }
    protected void RBLStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        BusinessPartnersBind(RBLStatus.SelectedItem.Text, DDLNatureOfPartners.SelectedItem.Text);
    }
    protected void GVBusinessPartners_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(GVBusinessPartners.Rows[e.RowIndex].Cells[0].Text);
        bool check = Class1.InsertUpdate("Delete from BusinessPartnerTB where BSP_Id=" + id + "");

        BusinessPartnersBind(RBLStatus.SelectedItem.Text, DDLNatureOfPartners.SelectedItem.Text);
    }
    protected void DDLNatureOfPartners_SelectedIndexChanged(object sender, EventArgs e)
    {
        BusinessPartnersBind(RBLStatus.SelectedItem.Text, DDLNatureOfPartners.SelectedItem.Text);
    }
}

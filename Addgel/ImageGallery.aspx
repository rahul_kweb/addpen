﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="ImageGallery.aspx.cs" Inherits="Admin_ImageGallery" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%">
<tr>
<td align="center"><b>
Images Gallery</b> <hr />
</td>
</tr>
<tr>
<td>
    <asp:GridView ID="GVGallery" runat="server" AutoGenerateColumns="False" 
        EmptyDataText="No Images Found" onrowdeleting="GVGallery_RowDeleting" 
       >
        <Columns>
            <asp:BoundField DataField="ProductImage_Id" ReadOnly="True">
                <ControlStyle Height="50px" Width="50px" BackColor="Black" 
                    BorderColor="Black" />
                <ItemStyle Height="50px" Width="50px" BackColor="Black" BorderColor="Black" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Images">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" 
                        Text='<%# Eval("Product_ZoomImage") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" 
                        ImageUrl='<%# Eval("Product_ZoomImage") %>' />
                </ItemTemplate>
                <ControlStyle Height="300px" Width="300px" />
                <ItemStyle Height="300px" Width="300px" />
            </asp:TemplateField>
            <asp:CommandField ShowDeleteButton="True" />
        </Columns>
    </asp:GridView>
</td>

</tr>
</table>
</asp:Content>


﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="EnquiryListAdmin.aspx.cs" Inherits="Admin_EnquiryListAdmin" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript">
        var TotalChkBx;
        var Counter;

        window.onload = function() {
            //Get total no. of CheckBoxes in side the GridView.
        TotalChkBx = parseInt('<%= this.GVEnquiry.Rows.Count %>');

            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }

        function HeaderClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl =
       document.getElementById('<%= this.GVEnquiry.ClientID %>');
            var TargetChildControl = "chkBxSelect";

            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");

            //Checked/Unchecked all the checkBoxes in side the GridView.
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' &&
                Inputs[n].id.indexOf(TargetChildControl, 0) >= 0)
                Inputs[n].checked = CheckBox.checked;

            //Reset Counter
            Counter = CheckBox.checked ? TotalChkBx : 0;
        }

        function ChildClick(CheckBox, HCheckBox) {
            //get target control.
            var HeaderCheckBox = document.getElementById(HCheckBox);

            //Modifiy Counter; 
            if (CheckBox.checked && Counter < TotalChkBx)
                Counter++;
            else if (Counter > 0)
                Counter--;

            //Change state of the header CheckBox.
            if (Counter < TotalChkBx)
                HeaderCheckBox.checked = false;
            else if (Counter == TotalChkBx)
                HeaderCheckBox.checked = true;
        }
</script>
<table width="100%">
<tr>
<td>

</td>
</tr>
<tr>
<td>

</td>
</tr>
<tr>
<td align="center">
    <asp:DropDownList ID="DDLEnquiryCatList" runat="server" AutoPostBack="True" 
        DataTextField="E_CategoryName" DataValueField="E_CategoryId" Height="22px" 
        onselectedindexchanged="DDLEnquiryCatList_SelectedIndexChanged" Width="149px">
        <asp:ListItem Value="1">Enquiry</asp:ListItem>
        <asp:ListItem Value="2">Suggestion</asp:ListItem>
        <asp:ListItem Value="3">Complaint</asp:ListItem>
        <asp:ListItem Value="001">International Division</asp:ListItem>
        <asp:ListItem Value="002">Institutional Sales</asp:ListItem>
    </asp:DropDownList>
    </td>
</tr>
    <tr>
<td>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Search By&nbsp; Name &nbsp;&nbsp;&nbsp;

    <asp:TextBox ID="txtsearch" runat="server"></asp:TextBox>
    &nbsp;&nbsp;
    <asp:Button ID="btnsearch" runat="server" Text="Search" 
        onclick="btnsearch_Click" />
</td>
</tr>

<tr>
<td>
    <asp:GridView ID="GVEnquiry" runat="server" Width="100%" AllowPaging="True" 
        AutoGenerateColumns="False" onpageindexchanging="GVEnquiry_PageIndexChanging" 
        PageSize="30" EmptyDataText="NO RECORD FOUND">
        <Columns>
            <asp:TemplateField HeaderText="ID">
                <EditItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("E_Id") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("E_Id") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="E_Name" HeaderText="Name" />
            <asp:BoundField DataField="E_Address" HeaderText="Address" />
            <asp:BoundField DataField="E_EmailId" HeaderText="Email-Id" />
            <asp:BoundField DataField="E_Contact" HeaderText="Contact" />
            <asp:BoundField DataField="E_Comment" HeaderText="Comment" />
            <asp:BoundField DataField="E_Date" HeaderText="Date" />
            <asp:TemplateField HeaderText="Delete">
                <HeaderTemplate>
                    <asp:CheckBox ID="chkBxHeader" runat="server" 
                        onclick="javascript:HeaderClick(this);" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="chkBxSelect" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </td>
</tr>
<tr>
<td align="center">
<asp:Button ID="btndeletemulti" runat="server" Text=" Delete Selected" 
            onclick="btndeletemulti_Click" />
</td>
</tr>
</table>
</asp:Content>


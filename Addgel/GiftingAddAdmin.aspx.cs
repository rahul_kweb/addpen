﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_GiftingAddAdmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
         if (Session["Check"] != null)
        {
        lbladd.Text = "";
        }
         else
         {
             Response.Redirect("HQAGP.aspx");
         }
    }
    protected void btnadd_Click(object sender, EventArgs e)
    {
        int productid = Class1.MaxGiftingIdeasId() + 1;

        string extensionI1 = System.IO.Path.GetExtension(FUthumbnail.FileName);
      
        string extensionI3 = System.IO.Path.GetExtension(FUzoom.FileName);
     

        string proname = txtproductname.Text.Replace("'", "''");
        string prodisc = txtdescription.Text.Replace("'", "''");
        string prthumnail = "GiftingThumnail" + productid + "" + extensionI1;       
        string prozoom = "GiftingZoom" + productid + "" + extensionI3;
        string date = DateTime.Today.ToShortDateString();

        bool chkadd = Class1.InsertUpdate("insert into GiftingIdeasTB values('" + proname + "','" + prodisc + "','" + prthumnail + "','" + prozoom + "','Active','"+date+"')");
        if (FUthumbnail != null)
        {

            FUthumbnail.SaveAs(Server.MapPath("adminImage\\" + prthumnail + ""));
        }
       
        if (FUzoom != null)
        {

            FUzoom.SaveAs(Server.MapPath("adminImage\\" + prozoom + ""));
        }

        lbladd.Text = "Gift " + txtproductname.Text + " Added Succesfully";
        txtproductname.Text = "";
        txtdescription.Text = "";
    }
}

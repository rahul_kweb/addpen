﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;

public partial class Admin_cms : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            Label1.Text = "";
            Editor1.Width = 900;
            Editor1.Height = 500;

            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    int ID = Convert.ToInt32(Request.QueryString["ID"].ToString());
                    contentget(ID);


                }
            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }
    }

    private void contentget(int id)
    {
        try
        {
            if (id == 1)
            {
                Editor1.Content = Class1.IncludeCall("aboutus.htm");
                LblHead.Text = "About Us";

            }
            else if (id == 2)
            {
                Editor1.Content = Class1.IncludeCall("addcorporationprofile.htm");
                LblHead.Text = "ADD Corporation Profile";
            }
            else if (id == 3)
            {
                Editor1.Content = Class1.IncludeCall("companyvision.htm");
                LblHead.Text = "Company Vision";
            }
            else if (id == 4)
            {
                Editor1.Content = Class1.IncludeCall("approach.htm");
                LblHead.Text = "Approach";
            }
            else if (id == 5)
            {
                Editor1.Content = Class1.IncludeCall("whychooseus.htm");
                LblHead.Text = "Why choose us?";
            }
            else if (id == 6)
            {
                Editor1.Content = Class1.IncludeCall("theteam.htm");
                LblHead.Text = "The team";
            }
            else if (id == 7)
            {
                Editor1.Content = Class1.IncludeCall("corporatesales.htm");
                LblHead.Text = "Corporate Sales";
            }
            else if (id == 8)
            {
                Editor1.Content = Class1.IncludeCall("internationalsales.htm");
                LblHead.Text = "International Sales ";
            }
            else if (id == 9)
            {
                Editor1.Content = Class1.IncludeCall("aftersalesservices.htm");
                LblHead.Text = "After Sales Services ";
            }            
            
        }
        catch (Exception ex)
        {
            Label1.Text = ex.Message.ToString();
        }


    }
    protected void BtnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString["id"] != null)
            {
                int id = Convert.ToInt32(Request.QueryString["ID"].ToString());
                string name = "";
                if (id == 1)
                {
                    name = "aboutus.htm";
                }
                else if (id == 2)
                {
                    name = "addcorporationprofile.htm";
                }
                else if (id == 3)
                {
                    name = "companyvision.htm";
                }
                else if (id == 4)
                {
                    name = "approach.htm";
                }
                else if (id == 5)
                {
                    name = "whychooseus.htm";
                }
                else if (id == 6)
                {
                    name = "theteam.htm";
                }
                else if (id == 7)
                {
                    name = "corporatesales.htm";
                }
                else if (id == 8)
                {
                    name = "internationalsales.htm";
                }
                else if (id == 9)
                {
                    name = "aftersalesservices.htm";
                }
               

                //  FileStream fs = File.OpenWrite("E:\\Aashish\\Current Project\\accutest 8-2\\new_accutest\\includes\\"+name,FileMode.Create);
                //  using (FileStream fs = new FileStream("http://accutest.kwebmakerusa.com/includes/" + name, FileMode.Create)) ;

                FileStream fs = new FileStream(Server.MapPath("../includes/" + name), FileMode.Create);
                StreamWriter writer = new StreamWriter(fs, Encoding.UTF8);
                //  BinaryWriter writer = new BinaryWriter(fs, Encoding.UTF8);
                writer.Write(Editor1.Content.ToString());
                //  writer.Write(Editor1.Content.Replace("<tbody>","").Replace("</tbody>",""));
                writer.Close();
                fs.Close();
                Label1.Text = "Change Done";
            }
        }
        catch (Exception ex)
        {
            Label1.Text = ex.Message.ToString();
        }
    }
}

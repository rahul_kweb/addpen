﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_SeriesListAdmin : System.Web.UI.Page
{
    bool[] rowChanged;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            lblupdate.Text = "";
            rowChanged = new bool[10];
            if (!IsPostBack)
            {
                ProductBind("Active", 3);
                CategoryBind();
            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }
    }
    private void ProductBind(string status, int id)
    {
        DataTable dt = Class1.GetRecord("select * from PenSeriesTB where PSeries_Status='" + status + "' and PSeries_PId=" + id + "");
        GVProduct.DataSource = dt;
        GVProduct.DataBind();


    }
    private void CategoryBind()
    {
        DataTable dt = Class1.GetRecord("select * from CategoryTB where C_ParentId=1 and C_CatType=1");
        DDLCategory.DataSource = dt;
        DDLCategory.DataBind();
    }
    protected void RBLStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        ProductBind(RBLStatus.SelectedValue, Convert.ToInt32(DDLCategory.SelectedValue));
    }
    protected void DDLCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        ProductBind(RBLStatus.SelectedValue, Convert.ToInt32(DDLCategory.SelectedValue));
    }
    protected void btnapply_Click(object sender, EventArgs e)
    {
        try
        {

            if (Page.IsPostBack)
            {
                string IdList = "";
                int totalRows = 10;
                for (int r = 0; r < totalRows; r++)
                {
                    if (rowChanged[r])
                    {
                        GridViewRow thisGridViewRow = GVProduct.Rows[r];


                        Label LId = (Label)thisGridViewRow.FindControl("lblid");
                        int NewId = Convert.ToInt32(LId.Text);

                        IdList = IdList + " " + NewId;

                        CheckBox chkdeal = (CheckBox)thisGridViewRow.FindControl("chkstatus");
                        string status = "InActive";

                        if (chkdeal.Checked)
                        {
                            status = "Active";
                        }

                        //int groupid = Convert.ToInt32(rbStatus.SelectedValue);
                        bool check = Class1.InsertUpdate("update PenSeriesTB set PSeries_Status='" + status + "' where PSeries_Id =" + NewId + "");

                    }
                }



                ProductBind(RBLStatus.SelectedValue, Convert.ToInt32(DDLCategory.SelectedValue));
                if (IdList == "")
                {
                    lblupdate.Text = "No Change Occur";
                }
                else
                {
                    lblupdate.Text = "ID" + IdList + " Have been Updated";
                }




            }
        }
        catch (Exception ex)
        {
            lblupdate.Text = ex.Message;
        }
    }
    protected void chkstatus_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox thisTextBox = (CheckBox)sender;
            GridViewRow thisGridViewRow = (GridViewRow)thisTextBox.Parent.Parent;
            int row = thisGridViewRow.RowIndex;
            rowChanged[row] = true;
        }
        catch (Exception ex)
        {
            lblupdate.Text = ex.Message.ToString();
        }
    }
  
    
    protected void btndeletemulti_Click(object sender, EventArgs e)
    {
        bool[] rowChanged = new bool[GVProduct.Rows.Count];
        for (int r = 0; r < GVProduct.Rows.Count; r++)
        {
            GridViewRow thisGridViewRow = GVProduct.Rows[r];
            Label LId = (Label)thisGridViewRow.FindControl("lblid");
            int NewId = Convert.ToInt32(LId.Text);
            CheckBox rbStatus = (CheckBox)thisGridViewRow.FindControl("chkBxSelect");
            if (rbStatus.Checked)
            {
                bool check = Class1.InsertUpdate("Delete from PenSeriesTB where PSeries_Id=" + NewId + "");
            }
        }
        ProductBind(RBLStatus.SelectedValue, Convert.ToInt32(DDLCategory.SelectedValue));
    }

    protected void GVEnquiry_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow &&
   (e.Row.RowState == DataControlRowState.Normal ||
    e.Row.RowState == DataControlRowState.Alternate))
        {
            CheckBox chkBxSelect = (CheckBox)e.Row.Cells[1].FindControl("chkBxSelect");
            CheckBox chkBxHeader = (CheckBox)this.GVProduct.HeaderRow.FindControl("chkBxHeader");
            chkBxSelect.Attributes["onclick"] = string.Format
                                                   (
                                                      "javascript:ChildClick(this,'{0}');",
                                                      chkBxHeader.ClientID
                                                   );
        }
    }
}

﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="GiftingAddAdmin.aspx.cs" Inherits="Admin_GiftingAddAdmin" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%">
       <tr>
<td colspan="2" align="center">
<b> Gifting Add   </b><hr />
</td>
</tr>
    
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="lbladd" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        
        <tr>
            <td style="width: 124px">
                Title
                </td>
            <td>
                <asp:TextBox ID="txtproductname" runat="server" Height="22px" Width="398px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtproductname" ErrorMessage="Field cannot be blank" 
                    ValidationGroup="1"></asp:RequiredFieldValidator>
                </td>
        </tr>
        <tr>
            <td style="width: 124px">
                Description</td>
            <td>
                <asp:TextBox ID="txtdescription" TextMode="MultiLine" runat="server" 
                    Height="64px" Width="399px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="txtdescription" ErrorMessage="Field cannot be blank" 
                    ValidationGroup="1"></asp:RequiredFieldValidator>
                </td>
        </tr>
        
        
        <tr>
        <td>
        Thumbnail      
        </td>
        <td>
            <asp:FileUpload ID="FUthumbnail" runat="server" />
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
            ControlToValidate="FUthumbnail" Display="None" 
            ErrorMessage="Please Add Only  Image in Thumnail" 
            ValidationExpression="^.+\.((jpg)|(gif)|(jpeg)|(png)|(bmp))$"></asp:RegularExpressionValidator>

        </td>
        </tr>
        
        
        <tr>
        <td>
        Zoom
        </td>
        <td>
            <asp:FileUpload ID="FUzoom" runat="server" />
             <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
            ControlToValidate="FUzoom" Display="None" 
            ErrorMessage="Please Add Only  Image in Zoom" 
            ValidationExpression="^.+\.((jpg)|(gif)|(jpeg)|(png)|(bmp))$"></asp:RegularExpressionValidator>
        </td>
        </tr>
        
        
        <tr>
            <td colspan="2" align="center">
                <asp:Button ID="btnadd" runat="server" Text="Save" onclick="btnadd_Click" 
                     ValidationGroup="1"/>  </td>
        </tr>
    </table>
</asp:Content>


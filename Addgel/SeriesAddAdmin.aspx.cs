﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_SeriesAddAdmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            if (!IsPostBack)
            {
                CategoryBind();
                DDLCategory.Items.Insert(0, new ListItem("-----------Select-----------"));
            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }
    }
    private void CategoryBind()
    {
        DataTable dt = Class1.GetRecord("select * from CategoryTB where C_ParentId=1 and C_CatType=1");
        DDLCategory.DataSource = dt;
        DDLCategory.DataBind();
    }
    

    protected void btnadd_Click(object sender, EventArgs e)
    {
        int productid = Class1.MaxProductSeriesId() + 1;

        string extensionI1 = System.IO.Path.GetExtension(FUlarge.FileName);
        string extensionI2 = System.IO.Path.GetExtension(FUBannerFooter.FileName);
        string extensionI3 = System.IO.Path.GetExtension(FULogo.FileName);


        string prthumnail = string.Empty;
        string prolarge = string.Empty;
        string prozoom = string.Empty;

        if (extensionI1 != "")
        {
            prthumnail = "AddpenSeriesBannner" + productid + "" + extensionI1;
        }
        else
        {
            prthumnail = "";
        }
        if (extensionI2 != "")
        {
            prolarge = "AddpenSeriesBannerFooter" + productid + "" + extensionI2;
        }
        else
        {
            prolarge = "";
        }

        if (extensionI3 != "")
        {
            prozoom = "AddpenSeriesLogo" + productid + "" + extensionI3;
        }
        else
        {
            prozoom = "";
        }

        string prohead = txthead.Text.Replace("'", "''");      
        string prodisc = txtdescription.Text.Replace("'", "''");      
      
       
        string date=DateTime.Today.ToShortDateString();
        bool check = Class1.InsertUpdate("insert into PenSeriesTB values('" + DDLCategory.SelectedItem.Text.Trim() + "'," + Convert.ToInt32(DDLCategory.SelectedValue) + ",'" + prohead + "','" + prodisc + "','" + prthumnail+ "','" + prolarge + "','" + prozoom + "','Active','" + date + "')");

        if (extensionI1 != "")
        {

            FUlarge.SaveAs(Server.MapPath("adminImage\\" + prthumnail + ""));
        }

        if (extensionI2 != "")
        {

            FUBannerFooter.SaveAs(Server.MapPath("adminImage\\" + prolarge + ""));
        }

        if (extensionI3 != "")
        {

            FULogo.SaveAs(Server.MapPath("adminImage\\" + prozoom + ""));
        }
        DDLCategory.SelectedIndex = 0;
        txthead.Text = "";
        txtdescription.Text = "";
        lbladd.Text = "Save Sucessfully";
    }
}

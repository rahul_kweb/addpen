﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_VedioDetailsAdmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            if (!IsPostBack)
            {
                //btnaddvedio.Visible = false;
                tbidupdatevideo.Visible = false;
                int id = Convert.ToInt32(Request.QueryString["id"]);
                HiddenField1.Value = id.ToString();
                Bindpervedio(id);
            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }   
    }
    private void Bindpervedio(int id)
    {
        DataTable dt = Class1.GetRecord("select * from VideoTB where Id=" + Convert.ToInt32(Request.QueryString["id"]) + "");
        lbltitle1.Text = dt.Rows[0]["Title"].ToString();
        lbldescription1.Text = dt.Rows[0]["Description"].ToString();
        lblurl1.Text = dt.Rows[0]["URL"].ToString();
        lblembedcode1.Text = dt.Rows[0]["Embedcode"].ToString();
        //string newproductbody = "";
        //newproductbody = newproductbody + "<a href='~/Addgel/video/" + dt.Rows[0]["VideoDownload"].ToString() + "'>Download</a>";
        //lblhyplink.Text = newproductbody;
        hypvideodownload.NavigateUrl = "~/Addgel/video/" + dt.Rows[0]["VideoDownload"].ToString();
        Image1.ImageUrl = "~/Addgel/AdminImage/" + dt.Rows[0]["videoImage"].ToString();
        hfimage.Value = dt.Rows[0]["videoImage"].ToString();
        hfvideo.Value = dt.Rows[0]["VideoDownload"].ToString();
    }

    protected void btnedit_Click(object sender, EventArgs e)
    {
        btnedit.Visible = false;
        tbidvideolist.Visible = false;
        tbidupdatevideo.Visible = true;

        DataTable dt = Class1.GetRecord("select * from VideoTB where Id=" + Convert.ToInt32(Request.QueryString["id"]) + "");
        txttitle.Text = dt.Rows[0]["Title"].ToString();
        txtdescription.Text = dt.Rows[0]["Description"].ToString();
        txturl.Text = dt.Rows[0]["URL"].ToString();
        txtembedcode.Text = dt.Rows[0]["EmbedCode"].ToString();
        
    }
    protected void btnaddvedio_Click(object sender, EventArgs e)
    {
        
        string title = txttitle.Text.Replace("'", "''");
        string description = txtdescription.Text.Replace("'", "''");
        string url = txturl.Text.Replace("'", "''");
        string embedcode = txtembedcode.Text.Replace("'", "''");       
        string date = DateTime.Today.ToShortDateString();

        string prthumnail = "";
        if (FUImage.HasFile == true)
        {
            prthumnail = "Addpenvideoimage" + HiddenField1.Value + "" + System.IO.Path.GetExtension(FUImage.FileName);
            FUImage.SaveAs(Server.MapPath("adminImage\\" + prthumnail + ""));
        }
        else
        {
            prthumnail = hfimage.Value;
        }


        string video = "";
        if (FUVideo.HasFile == true)
        {
            video = "Addpenvideo" + HiddenField1.Value + "" + System.IO.Path.GetExtension(FUVideo.FileName);
            FUVideo.SaveAs(Server.MapPath("adminImage\\" + video + ""));
        }
        else
        {
            video = hfvideo.Value;
        }


        bool check = Class1.InsertUpdate("Update VideoTB set Title='" + title + "',Description='" + description + "',Url='" + url + "',EmbedCode='" + embedcode + "',VideoDownload='" + video + "',videoImage='" + prthumnail + "',Date='" + date + "' where Id=" + Convert.ToInt32(Request.QueryString["id"]) + "");
              
        lblupdate.Text = "Vedio Updated Successfully";
        btnedit.Visible = true;
        tbidvideolist.Visible = true;
        tbidupdatevideo.Visible = false;
        Response.Redirect("VideoListAdmin.aspx");
    }
}

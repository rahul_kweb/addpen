﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="JobsOpeningListAdmin.aspx.cs" Inherits="Admin_JobsOpeningListAdmin" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%">
<tr>
<td colspan="2">

</td>
</tr>
<tr>
<td>
    <asp:RadioButtonList ID="RBLStatus" runat="server" RepeatDirection="Horizontal" 
        AutoPostBack="True" onselectedindexchanged="RBLStatus_SelectedIndexChanged">
        <asp:ListItem Selected="True">Active</asp:ListItem>
        <asp:ListItem>InActive</asp:ListItem>
    </asp:RadioButtonList>
</td>
<td valign="middle">
    <asp:TextBox ID="txtsearch" runat="server"></asp:TextBox>
    <asp:Button ID="btnsearch" runat="server" Text="Search" 
        onclick="btnsearch_Click" />
</td>
</tr>
<tr>
<td colspan="2" align="center">
    <asp:Label ID="lblupdate" runat="server" Text=""></asp:Label>
<br />
</td>
</tr>
<tr>
<td colspan="2">
    <asp:GridView ID="GVJobs" Width="100%" runat="server" 
        AutoGenerateColumns="False" EmptyDataText="NO RECORD FOUND" 
        onrowdeleting="GVJobs_RowDeleting">
        <Columns>
            <asp:BoundField DataField="Jobs_id" HeaderText="Id" />
            <asp:TemplateField HeaderText="Id" Visible="False">
                <EditItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Jobs_id") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblid" runat="server" Text='<%# Bind("Jobs_id") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Jobs_PrimarySkills" HeaderText="Primary Skills" />
            <asp:BoundField DataField="Jobs_Experience" HeaderText="Experience" />
            <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:CheckBox ID="chkstatus" runat="server" 
                        Checked='<%# Eval("Jobs_Status").ToString().Equals("Active") %>' 
                        oncheckedchanged="chkstatus_CheckedChanged" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:HyperLinkField DataNavigateUrlFields="Jobs_id" 
                DataNavigateUrlFormatString="JobsOpeningUpdate.aspx?id={0}" Text="more..." />
            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" />
        </Columns>
    </asp:GridView>
</td>
</tr>
<tr>
<td colspan="2" align="center">
    <asp:Button ID="btnapply" runat="server" Text="Apply changes" 
        onclick="btnapply_Click" />
</td>
</tr>
</table>
</asp:Content>


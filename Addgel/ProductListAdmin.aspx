﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="ProductListAdmin.aspx.cs" Inherits="Admin_ProductListAdmin" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
        var TotalChkBx;
        var Counter;

        window.onload = function() {
            //Get total no. of CheckBoxes in side the GridView.
        TotalChkBx = parseInt('<%= this.GVProduct.Rows.Count %>');

            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }

        function HeaderClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl =
       document.getElementById('<%= this.GVProduct.ClientID %>');
            var TargetChildControl = "chkBxSelect";

            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");

            //Checked/Unchecked all the checkBoxes in side the GridView.
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' &&
                Inputs[n].id.indexOf(TargetChildControl, 0) >= 0)
                Inputs[n].checked = CheckBox.checked;

            //Reset Counter
            Counter = CheckBox.checked ? TotalChkBx : 0;
        }

        function ChildClick(CheckBox, HCheckBox) {
            //get target control.
            var HeaderCheckBox = document.getElementById(HCheckBox);

            //Modifiy Counter; 
            if (CheckBox.checked && Counter < TotalChkBx)
                Counter++;
            else if (Counter > 0)
                Counter--;

            //Change state of the header CheckBox.
            if (Counter < TotalChkBx)
                HeaderCheckBox.checked = false;
            else if (Counter == TotalChkBx)
                HeaderCheckBox.checked = true;
        }
</script>
<table width="100%">
<tr>
<td align="center">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:RadioButtonList ID="RBLStatus" runat="server" AutoPostBack="True" 
        RepeatDirection="Horizontal" 
        onselectedindexchanged="RBLStatus_SelectedIndexChanged">
        <asp:ListItem Selected="True">Active</asp:ListItem>
        <asp:ListItem>InActive</asp:ListItem>
    </asp:RadioButtonList>
</td>
<td>
    <asp:DropDownList ID="DDLCategory" runat="server" AutoPostBack="True" 
        Height="20px" Width="194px" DataTextField="C_CategoryName" 
        DataValueField="C_Id" onselectedindexchanged="DDLCategory_SelectedIndexChanged">
    </asp:DropDownList>
</td>
</tr>
<tr>
<td colspan="2" align="center">
    <asp:Label ID="lblupdate" runat="server" Text=""></asp:Label>
</td>
</tr>
<tr>
<td colspan="2">
    <asp:GridView ID="GVProduct" runat="server" Width="100%" 
        AutoGenerateColumns="False" EmptyDataText="NO RECORD FOUND">
        <Columns>
            <asp:TemplateField HeaderText="ID">
                <EditItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("P_Id") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblid" runat="server" Text='<%# Bind("P_Id") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:ImageField DataImageUrlField="P_Thumbnail" 
                DataImageUrlFormatString="~/Addgel/AdminImage/{0}" HeaderText="Image">
            </asp:ImageField>
            <asp:BoundField DataField="P_Name" HeaderText="Name" />
            <asp:BoundField DataField="P_Description" HeaderText="Description" />
            <asp:BoundField DataField="P_Prices" HeaderText="Prices" />
            <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:CheckBox ID="chkstatus" runat="server" 
                        Checked='<%# Eval("P_Status").ToString().Equals("Active") %>' oncheckedchanged="chkstatus_CheckedChanged" 
                         />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Just Launched" Visible="False">
                <ItemTemplate>
                    <asp:CheckBox ID="chknew" runat="server" 
                        Checked='<%# Eval("P_JustLaunched").ToString().Equals("Active") %>' oncheckedchanged="chknew_CheckedChanged" 
                         />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:HyperLinkField DataNavigateUrlFields="P_Id" 
                DataNavigateUrlFormatString="ProductDetailsupdate.aspx?id={0}" Text="Edit" />
            <asp:HyperLinkField DataNavigateUrlFields="P_Id" 
                DataNavigateUrlFormatString="ProductImages.aspx?id={0}" Text="Add Photo" />
            <asp:HyperLinkField DataNavigateUrlFields="P_Id" 
                DataNavigateUrlFormatString="ImageGallery.aspx?id={0}" HeaderText="Image" 
                Text="View" />
            <asp:TemplateField HeaderText="Delete">
                <HeaderTemplate>
                    <asp:CheckBox ID="chkBxHeader" runat="server" 
                        onclick="javascript:HeaderClick(this);" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="chkBxSelect" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</td>
</tr>
<tr>
<td colspan="2" align="center">
    <asp:Button ID="btnapply" runat="server" Text="Apply Change" 
        onclick="btnapply_Click" />
        <%--<asp:Button ID="btnlaunched" runat="server" Text="Make Just Launched" OnClick="btnlaunched_Click" />--%>
        <asp:Button ID="btndeletemulti" runat="server" Text=" Delete Selected" 
            onclick="btndeletemulti_Click" />
</td>
</tr>

</table>
</asp:Content>


﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_VideoAddAdmin : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            lblsave.Text = "";
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }
    }
    protected void btnaddvedio_Click(object sender, EventArgs e)
    {
        int videoid = Class1.MaxVideoId() + 1;

        string title = txttitle.Text.Replace("'", "''");
        string description = txtdescription.Text.Replace("'", "''");
        string url = txturl.Text.Replace("'", "''");
        string embedcode = txtembedcode.Text.Replace("'", "''");
        string extensionI1 = System.IO.Path.GetExtension(FUVideo.FileName);
        string date = DateTime.Today.ToShortDateString();

        string video = "Addpenvideo" + videoid + "" + extensionI1;
        string extensionI2 = System.IO.Path.GetExtension(FUImage.FileName);

        string prthumnail = string.Empty;

        if (extensionI2 != "")
        {
            prthumnail = "Addpenvideoimage" + videoid + "" + extensionI2;
        }
        else
        {
            prthumnail = "";
        }

        bool check = Class1.InsertUpdate("insert into VideoTB values('" + title + "','" + description + "','" + url + "','" + embedcode + "','" + video + "','" + prthumnail + "','InActive','" + date + "')");
             
        if (FUVideo != null)
        {
            FUVideo.SaveAs(Server.MapPath("video\\" + video + ""));
        }

        if (extensionI2 != "")
        {

            FUImage.SaveAs(Server.MapPath("adminImage\\" + prthumnail + ""));
        }


        txttitle.Text = "";
        txtdescription.Text = "";
        txturl.Text = "";
        txtembedcode.Text = "";
        lblsave.Text = "Vedio Added Successfully";

    }
}

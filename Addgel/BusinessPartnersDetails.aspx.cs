﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_BusinessPartnersDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            BindBusinesspartnerById();
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }
    }
    private void BindBusinesspartnerById()
    {
        DataTable dt = Class1.GetRecord("select * from BusinessPartnerTB where BSP_Id=" + Request.QueryString["id"].ToString() + "");
        lbltitle.Text = dt.Rows[0]["BSP_Title"].ToString();
        lblcontactperson.Text = dt.Rows[0]["BSP_ContactPerson"].ToString();
        lbldesignation.Text = dt.Rows[0]["BSP_Designation"].ToString();
        lblcompanyname.Text = dt.Rows[0]["BSP_CompanyName"].ToString();
        lbladdress.Text = dt.Rows[0]["BSP_Address"].ToString();
        lblcity.Text = dt.Rows[0]["BSP_City"].ToString();
        lblcountry.Text = dt.Rows[0]["BSP_Country"].ToString();
        lblzipcode.Text = dt.Rows[0]["BSP_ZipCode"].ToString();
        lbltelno.Text = dt.Rows[0]["BSP_Telephone"].ToString();
        lblfaxno.Text = dt.Rows[0]["BSP_FaxNo"].ToString();
        lblemailid.Text = dt.Rows[0]["BSP_EmailId"].ToString();
        lblnatureofpartnership.Text = dt.Rows[0]["BSP_NatureOfPartnership"].ToString();
    }
}

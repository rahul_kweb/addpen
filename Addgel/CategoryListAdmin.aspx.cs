﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_CategoryListAdmin : System.Web.UI.Page
{
    bool[] rowChanged;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            lblupdate.Text = "";
            rowChanged = new bool[30];
            if (!IsPostBack)
            {
                CategoryBind("Active", 1,1);
                DDLCategory.Items.Insert(0, "------EMPTY-----");

            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }

    }


    private void RefillCategoryBind()
    {
        DataTable dt = Class1.GetRecord("select * from CategoryTB where C_CatType=2 and C_ParentId=2");
        DDLCategory.DataSource = dt;
        DDLCategory.DataBind();
    }

    protected void DDLRefilecategory_SelectedIndexChanged(object sender, EventArgs e)
    {
       
        if (DDLCategory.SelectedItem.Text != "-----------Select-----------")
        {
            DataTable dt = Class1.GetRecord("select * from CategoryTB where C_ParentId=" + Convert.ToInt32(DDLCategory.SelectedValue) + " and C_CatType=" + Convert.ToInt32(DDLSelectSeries.SelectedValue) + "");

                GVCategory.DataSource = dt;
                GVCategory.DataBind();
        }          
    
        else
        {
            Response.Write("<script>alert('Please select category')</script>");
           
        }
    }


    protected void DDLSelectSeries_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable objtb = Class1.GetRecord("select * from CategoryTB where C_CatType=" + Convert.ToInt32(DDLSelectSeries.SelectedValue) + " and C_ParentId=" + Convert.ToInt32(DDLSelectSeries.SelectedValue) + "");
        DDLCategory.DataSource = objtb;
        DDLCategory.DataBind();
        DDLCategory.Items.Insert(0, "-----------Select-----------");

        DataTable dt = Class1.GetRecord("select * from CategoryTB where C_CatType=" + Convert.ToInt32(DDLSelectSeries.SelectedValue) + " and C_ParentId=" + Convert.ToInt32(DDLSelectSeries.SelectedValue) + "");
        if (dt.Rows.Count > 0)
        {
            GVCategory.DataSource = dt;
            GVCategory.DataBind();
        }       
    }
    private void CategoryBind(string status,int id,int parentid)
    {
        DataTable dt = Class1.GetRecord("select * from CategoryTB where C_Status='" + status + "' and C_CatType=" + id + " and C_ParentId=" + parentid+ " order by C_Id");
        GVCategory.DataSource = dt;
        GVCategory.DataBind();


    }
    protected void btnapply_Click(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsPostBack)
            {
                string IdList = "";
                int totalRows = 30;
                for (int r = 0; r < totalRows; r++)
                {
                    if (rowChanged[r])
                    {
                        GridViewRow thisGridViewRow = GVCategory.Rows[r];


                        Label LId = (Label)thisGridViewRow.FindControl("lblid");
                        int NewId = Convert.ToInt32(LId.Text);

                        IdList = IdList + " " + NewId;

                        CheckBox chkdeal = (CheckBox)thisGridViewRow.FindControl("chkstatus");
                        string status = "InActive";

                        if (chkdeal.Checked)
                        {
                            status = "Active";
                        }

                        //int groupid = Convert.ToInt32(rbStatus.SelectedValue);
                        bool check = Class1.InsertUpdate("update CategoryTB set C_Status='" + status + "' where C_Id =" + NewId + "");

                    }
                }



                CategoryBind(RBLStatus.SelectedValue, Convert.ToInt32(DDLSelectSeries.SelectedValue), Convert.ToInt32(DDLCategory.SelectedValue));
                if (IdList == "")
                {
                    lblupdate.Text = "No Change Occur";
                }
                else
                {
                    lblupdate.Text = "ID" + IdList + " Have been Updated";
                }




            }
        }
        catch (Exception ex)
        {
            lblupdate.Text = ex.Message;
        }
    }
    protected void chkstatus_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox thisTextBox = (CheckBox)sender;
            GridViewRow thisGridViewRow = (GridViewRow)thisTextBox.Parent.Parent;
            int row = thisGridViewRow.RowIndex;
            rowChanged[row] = true;
        }
        catch (Exception ex)
        {
            lblupdate.Text = ex.Message.ToString();
        }
    }
    protected void RBLStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLCategory.SelectedItem.Text != "------EMPTY-----" && DDLCategory.SelectedItem.Text != "-----------Select-----------")
        {
            CategoryBind(RBLStatus.SelectedValue, Convert.ToInt32(DDLSelectSeries.SelectedValue), Convert.ToInt32(DDLCategory.SelectedValue));
        }

        else 
        {
            Response.Write("<script>alert('Please select category')</script>");
        }
        
    }
    protected void GVCategory_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(GVCategory.Rows[e.RowIndex].Cells[0].Text);
        bool check = Class1.InsertUpdate("Delete from CategoryTB where C_PId=" + id + "");
        if (check)
        {
            if (Convert.ToInt32(DDLSelectSeries.SelectedValue) == 1)
            {
                bool chkpen = Class1.InsertUpdate("Delete from ProductTB where P_Id=" + id + "");
            }
            else 
            {
                bool chkpen = Class1.InsertUpdate("Delete from RefillTB where R_Id=" + id + "");
            }
        }
        CategoryBind(RBLStatus.SelectedValue, Convert.ToInt32(DDLSelectSeries.SelectedValue), Convert.ToInt32(DDLCategory.SelectedValue));
    }
    protected void GVCategory_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GVCategory.EditIndex = e.NewEditIndex;
        CategoryBind(RBLStatus.SelectedValue, Convert.ToInt32(DDLSelectSeries.SelectedValue), Convert.ToInt32(DDLCategory.SelectedValue));
    }
    protected void GVCategory_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GVCategory.EditIndex = -1;
        CategoryBind(RBLStatus.SelectedValue, Convert.ToInt32(DDLSelectSeries.SelectedValue), Convert.ToInt32(DDLCategory.SelectedValue));
    }
    protected void GVCategory_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string pk = ((Label)GVCategory.Rows[GVCategory.EditIndex].FindControl("Label1")).Text;

        string catname = ((TextBox)GVCategory.Rows[GVCategory.EditIndex].FindControl("TextBox1")).Text;
        string str = "update CategoryTB set C_CategoryName='" + catname + "' where C_Id=" + pk + "";
        bool check = Class1.InsertUpdate(str);
        if (check == true)
        {
            GVCategory.EditIndex = -1;
            CategoryBind(RBLStatus.SelectedValue, Convert.ToInt32(DDLSelectSeries.SelectedValue), Convert.ToInt32(DDLCategory.SelectedValue));
        }
    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

public partial class Admin_Admin : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            binddatetime();
        }
    }
    protected void ImageButtonLogout_Click(object sender, ImageClickEventArgs e)
    {
        bool datecheck = Class1.InsertUpdate("update UserTab set UserLogout=getdate() where UserID=1");
        Session["Check"] = null;
        Session["password"] = null;
        Session["datetime"] = null;
        Response.Write("<script language = javascript>window.open('HQAGP.aspx','_parent');</script>");
    }
    private void binddatetime()
    { 
        DataTable dt =(DataTable)Session["datetime"];
        lbllogin.Text = dt.Rows[0]["UserLogin"].ToString();
        lbllogout.Text = dt.Rows[0]["UserLogout"].ToString();
    }
}

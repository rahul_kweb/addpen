<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="ProductAdd.aspx.cs" Inherits="Admin_ProductAdd" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript" language="javascript">
    function Deselect() {
        document.getElementById('<%= ListBox1.ClientID %>').selectedIndex = -1;
    }  
</script> 
<table width="100%">
       <tr>
<td colspan="2" align="center">
<b> Product Add   </b><hr />
</td>
</tr>   
        <tr>
            <td align="center" colspan="2" style="height: 25px">
                <asp:Label ID="lbladd" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
        <td>
        Category&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
            <asp:DropDownList ID="DDLCategory" runat="server" Height="20px" Width="188px" 
                DataTextField="C_CategoryName" DataValueField="C_Id">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ControlToValidate="DDLCategory" ErrorMessage="Please select category" 
                InitialValue="0" ValidationGroup="1"></asp:RequiredFieldValidator>
        </td>
        </tr>
        <tr>
            <td style="width: 124px">
                Product Name
                </td>
            <td>
                <asp:TextBox ID="txtproductname" runat="server" Height="21px" Width="313px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="txtproductname" ErrorMessage="Field cannot be blank" 
                    ValidationGroup="1"></asp:RequiredFieldValidator>
                </td>
        </tr>
        <tr>
            <td style="width: 124px">
                Product Head
                </td>
            <td>
                <asp:TextBox ID="txthead" runat="server" Height="21px" Width="313px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                    ControlToValidate="txthead" ErrorMessage="Field cannot be blank" 
                    ValidationGroup="1"></asp:RequiredFieldValidator>
                </td>
        </tr>
        <tr>
            <td style="width: 124px">
                Description</td>
            <td>
                <asp:TextBox ID="txtdescription" TextMode="MultiLine" runat="server" 
                    Height="130px" Width="313px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="txtdescription" ErrorMessage="Field cannot be blank" 
                    ValidationGroup="1"></asp:RequiredFieldValidator>
                </td>
        </tr>
        
        <tr>
            <td style="width: 124px">
                Features<br />
                <asp:Button ID="btnnewline" runat="server" Text="New Line" 
                    onclick="btnnewline_Click" />
                                      </td>
            <td>
                <asp:TextBox ID="txtfeature" TextMode="MultiLine" runat="server" 
                    Height="130px" Width="313px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                    ControlToValidate="txtfeature" ErrorMessage="Field cannot be blank" 
                    ValidationGroup="1"></asp:RequiredFieldValidator>
                </td>
        </tr>
        <tr>
        <td>
        Refill Category
        </td>
        <td>
          <asp:DropDownList ID="DDLRefilecategory" Height="20px" Width="188px" runat="server"
             DataTextField="C_CategoryName" DataValueField="C_Id" AutoPostBack="True" 
                onselectedindexchanged="DDLRefilecategory_SelectedIndexChanged">
            </asp:DropDownList>   
           
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                ControlToValidate="DDLRefilecategory" ErrorMessage="Please select category" 
                InitialValue="0" ValidationGroup="1"></asp:RequiredFieldValidator>
        </td>
        </tr>
        <tr>
        <td>
        Refill
        </td>
        <td>
           
            <br />
            <asp:ListBox ID="lstrefill" runat="server" Width="160px" Height="128px"
                                            SelectionMode="Multiple" 
                DataTextField="C_CategoryName" DataValueField="C_PId"></asp:ListBox>
        </td>
        </tr>
        <tr>
        <td>
        Ink Color
        </td>
        <td>
            <asp:ListBox ID="ListBox1" runat="server" Width="160px" Height="150px"
                                            SelectionMode="Multiple" 
                DataTextField="InkColorM_Name" DataValueField="InkColorM_id"></asp:ListBox>
        </td>
        </tr>
        <%--<tr>            
        <td>
        Ink Color 
        <td>            
            <asp:Label ID="lblinkcolor" runat="server" Text=""></asp:Label>           
        </td>     
        </tr>--%>
        <tr>
        <td>
        Prices
        </td>
        <td>
            <asp:TextBox ID="txtprices" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                ControlToValidate="txtprices" ErrorMessage="Field cannot be blank" 
                ValidationGroup="1"></asp:RequiredFieldValidator>
        </td>
        </tr>
        
        <tr>
        <td>
        Banner      
            Image</td>
        <td>
            <asp:FileUpload ID="FUBannerimg" runat="server" />
            <asp:RegularExpressionValidator 

ID="RegularExpressionValidator4" runat="server" 
            ControlToValidate="FUBannerimg" Display="None" 
            ErrorMessage="Please Add Only Image " 
            

ValidationExpression="^.+\.((jpg)|(gif)|(jpeg)|(png)|(bmp))$">

</asp:RegularExpressionValidator>
        </td>
        </tr>
        
        <tr>
        <td>
        Thumbnail      
          </td>
        <td>
            <asp:FileUpload ID="FUthumbnail" runat="server" />
            <asp:RegularExpressionValidator 

ID="RegularExpressionValidator1" runat="server" 
            ControlToValidate="FUthumbnail" Display="None" 
            ErrorMessage="Please Add Only Image " 
            

ValidationExpression="^.+\.((jpg)|(gif)|(jpeg)|(png)|(bmp))$">

</asp:RegularExpressionValidator>
        </td>
        </tr>
        
        <tr>
        <td>
        Large
        </td>
        <td>
            <asp:FileUpload ID="FUlarge" runat="server" />
            <asp:RegularExpressionValidator 
ID="RegularExpressionValidator2" runat="server" 
            ControlToValidate="FUlarge" Display="None" 
            ErrorMessage="Please Add Only Image "          

ValidationExpression="^.+\.((jpg)|(gif)|(jpeg)|(png)|(bmp))$">

</asp:RegularExpressionValidator>
        </td>
        </tr>
        <tr>
        <td>
        Zoom
        </td>
        <td>
            <asp:FileUpload ID="FUzoom" runat="server" />
            <asp:RegularExpressionValidator 
ID="RegularExpressionValidator3" runat="server" 
            ControlToValidate="FUzoom" Display="None" 
            ErrorMessage="Please Add Only Image "          

ValidationExpression="^.+\.((jpg)|(gif)|(jpeg)|(png)|(bmp))$">
</asp:RegularExpressionValidator>
        </td>
        </tr>
        
        <tr>
        <td>
        PDF
        </td>
        <td>
            <asp:FileUpload ID="FUpdf" runat="server" />
            <asp:RegularExpressionValidator ID="REPDF" runat="server" 

Display="None" ErrorMessage="Select PDF Only"
        ValidationExpression=".*\.(pdf)"  

ControlToValidate="FUpdf"></asp:RegularExpressionValidator>
        </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button ID="btnadd" runat="server" Text="Save" onclick="btnadd_Click" 
               ValidationGroup="1"    />
                    
                      </td>
        </tr>
    </table>

</asp:Content>


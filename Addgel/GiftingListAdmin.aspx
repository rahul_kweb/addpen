﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="GiftingListAdmin.aspx.cs" Inherits="Admin_GiftingListAdmin" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%">
       <tr>
<td align="center">
<b> Gift Listing   </b><hr />
</td>
</tr>
    
        <tr>
            <td align="center">
                <asp:Label ID="lblupdate" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
<td align="center">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:RadioButtonList ID="RBLStatus" runat="server" AutoPostBack="True" 
        RepeatDirection="Horizontal" 
        onselectedindexchanged="RBLStatus_SelectedIndexChanged">
        <asp:ListItem Selected="True">Active</asp:ListItem>
        <asp:ListItem>InActive</asp:ListItem>
    </asp:RadioButtonList>
</td>
</tr>
        <tr>
            <asp:GridView ID="GVGifting" runat="server" AllowPaging="True" Width="100%"
                AutoGenerateColumns="False" 
                onpageindexchanging="GVGifting_PageIndexChanging" 
                onrowdeleting="GVGifting_RowDeleting" EmptyDataText="NO RECORD FOUND">
                <Columns>
                    <asp:BoundField DataField="GI_id" HeaderText="ID" />
                    <asp:TemplateField Visible="False">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("GI_id") %>'></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblid" runat="server" Text='<%# Bind("GI_id") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="GI_Date" HeaderText="Date" />
                    <asp:BoundField DataField="GI_Title" HeaderText="Title" />
                    <asp:BoundField DataField="GI_Description" HeaderText="Description" />
                    <asp:ImageField DataImageUrlField="GI_Thumbnail" 
                        DataImageUrlFormatString="~/Addgel/AdminImage/{0}">
                    </asp:ImageField>
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkstatus" runat="server" 
                                Checked='<%# Eval("GI_Status").ToString().Equals("Active") %>' 
                                oncheckedchanged="chkstatus_CheckedChanged" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:HyperLinkField DataNavigateUrlFields="GI_id" 
                        DataNavigateUrlFormatString="GiftingDeatilsUpdate.aspx?id={0}" Text="Edit" />
                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" />
                </Columns>
            </asp:GridView>
        </tr>
        <tr>
    <td align="center">
    <asp:Button ID="btnapply" runat="server" Text="Apply Change" 
        onclick="btnapply_Click" />
        </td></tr>
        </table>
</asp:Content>


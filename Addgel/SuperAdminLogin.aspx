﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="SuperAdminLogin.aspx.cs" Inherits="Admin_SuperAdminLogin" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="supadmintb" runat="server" width="100%">
<tr>
<td align="center"><strong>
Super Administrator</strong> <hr />
</td>
</tr>
<tr>
<td>
<br />
</td>
</tr>
<tr>
<td >
             
    <asp:RadioButtonList ID="RBLStatus" runat="server" AutoPostBack="True" 
        RepeatDirection="Horizontal" 
        onselectedindexchanged="RBLStatus_SelectedIndexChanged">
        <asp:ListItem Selected="True">Active</asp:ListItem>
        <asp:ListItem>InActive</asp:ListItem>
    </asp:RadioButtonList>
</td>
</tr>
<tr>
<td align="center">
    <asp:Label ID="lblupdate" runat="server" Text=""></asp:Label>
</td>
</tr>
<tr>
<td>
    <asp:GridView ID="GVAdmin" runat="server" Width="100%" 
        AutoGenerateColumns="False" EmptyDataText="NO RECORD FOUND" 
        onrowcancelingedit="GVAdmin_RowCancelingEdit" 
        onrowdeleting="GVAdmin_RowDeleting" onrowediting="GVAdmin_RowEditing" 
        onrowupdating="GVAdmin_RowUpdating">
        <Columns>
            <asp:BoundField DataField="UserID" HeaderText="ID" ReadOnly="True" />
            <asp:TemplateField HeaderText="Id" Visible="False">
                <EditItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("UserID") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblid" runat="server" Text='<%# Bind("UserID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Username">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("UserName") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="UserPassword">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("UserPassword") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("UserPassword") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:CheckBox ID="chkstatus" runat="server" 
                        Checked='<%# Eval("UserStatus").ToString().Equals("Active") %>' 
                        oncheckedchanged="chkstatus_CheckedChanged" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" />
            <asp:CommandField HeaderText="Edit" ShowEditButton="True" />
        </Columns>
    </asp:GridView>
</td>
</tr>
<tr>
<td align="center">
    <asp:Button ID="btnapply" runat="server" Text="Apply Change" 
        onclick="btnapply_Click" />
</td>
</tr>

<tr>
<td>
<br />
</td>
</tr>
<tr>
<td>
<br />
</td>
</tr>
<tr>
<td>
<table width="100%">
<tr>
<td align="center" colspan="2">
<strong>Add New Admin</strong>
</td>
</tr>
<tr>
<td colspan="2" align="left">
    <asp:Label ID="lbladd" runat="server" Text=""></asp:Label>
</td>
</tr>
<tr>
<td width="88px">
UserName - 
</td>
<td>
    <asp:TextBox ID="txtusername" runat="server"></asp:TextBox> 
</td>
</tr>
<tr>
<td width="88px">
Password - 
</td>
<td>
    <asp:TextBox ID="txtpassword" runat="server" TextMode="Password"></asp:TextBox> 
</td>
</tr>
<tr>
<td colspan="2">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnadd" runat="server" Text=" Add " onclick="btnadd_Click" />
</td>
</tr>
</table>
</td>
</tr>
</table>
<table id="admintb" runat="server" width="100%">
<tr>
<td align="center"><strong>
You have no authority to access this super admin page</strong>
</td>
</tr>
</table>
</asp:Content>


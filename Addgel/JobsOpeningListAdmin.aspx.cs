﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_JobsOpeningListAdmin : System.Web.UI.Page
{
    bool[] rowChanged;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            lblupdate.Text = "";
            rowChanged = new bool[10];
            if (!IsPostBack)
            {
                JobsBind("Active");
            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }

    }

    private void JobsBind(string status)
    {
        DataTable dt = Class1.GetRecord("select * from JobsTB where Jobs_Status='" + status + "'");
        GVJobs.DataSource = dt;
        GVJobs.DataBind();


    }
    protected void btnapply_Click(object sender, EventArgs e)
    {
        try
        {

            if (Page.IsPostBack)
            {
                string IdList = "";
                int totalRows = 10;
                for (int r = 0; r < totalRows; r++)
                {
                    if (rowChanged[r])
                    {
                        GridViewRow thisGridViewRow = GVJobs.Rows[r];


                        Label LId = (Label)thisGridViewRow.FindControl("lblid");
                        int NewId = Convert.ToInt32(LId.Text);

                        IdList = IdList + " " + NewId;

                        CheckBox chkdeal = (CheckBox)thisGridViewRow.FindControl("chkstatus");
                        string status = "InActive";

                        if (chkdeal.Checked)
                        {
                            status = "Active";
                        }

                        //int groupid = Convert.ToInt32(rbStatus.SelectedValue);
                        bool check = Class1.InsertUpdate("update JobsTB set Jobs_Status='" + status + "' where Jobs_id =" + NewId + "");

                    }
                }



                 JobsBind(RBLStatus.SelectedValue);
                if (IdList == "")
                {
                    lblupdate.Text = "No Change Occur";
                }
                else
                {
                    lblupdate.Text = "ID" + IdList + " Have been Updated";
                }




            }
        }
        catch (Exception ex)
        {
            lblupdate.Text = ex.Message;
        }
    }
    protected void chkstatus_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox thisTextBox = (CheckBox)sender;
            GridViewRow thisGridViewRow = (GridViewRow)thisTextBox.Parent.Parent;
            int row = thisGridViewRow.RowIndex;
            rowChanged[row] = true;
        }
        catch (Exception ex)
        {
            lblupdate.Text = ex.Message.ToString();
        }
    }
    protected void RBLStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        JobsBind(RBLStatus.SelectedValue);
    }
    protected void GVJobs_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(GVJobs.Rows[e.RowIndex].Cells[0].Text);

        bool check = Class1.InsertUpdate("Delete from JobsTB where Jobs_id=" + id + "");
        JobsBind(RBLStatus.SelectedValue);
    }
    private void BindWithSearch(string search)
    {
        DataTable dt = Class1.GetRecord("select * from JobsTB where Jobs_PrimarySkills like '%"+search+"%' or Jobs_Skills  like '%" + search + "%'");
        GVJobs.DataSource = dt;
        GVJobs.DataBind();

    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        BindWithSearch(txtsearch.Text.Replace("'", "''"));
    }
}

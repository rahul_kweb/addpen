﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="BusinessPartnersDetails.aspx.cs" Inherits="Admin_BusinessPartnersDetails" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%">
<tr>
<td colspan="2">
Business Partners Details<hr />
</td>
</tr>
<tr>
<td colspan="2">
<br />
</td>
</tr>
<tr>
<td style="width: 169px">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Title :
</td>
<td>
    <asp:Label ID="lbltitle" runat="server" Text=""></asp:Label>
</td>
</tr>
<tr>
<td style="width: 169px">
&nbsp;&nbsp;
Contact Person :
</td>
<td>
 <asp:Label ID="lblcontactperson" runat="server" Text=""></asp:Label>
</td>
</tr>
<tr>
<td style="width: 169px">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Designation :
</td>
<td>
 <asp:Label ID="lbldesignation" runat="server" Text=""></asp:Label>
</td>
</tr>
<tr>
<td style="width: 169px">
&nbsp;
Company Name :
</td>
<td>
 <asp:Label ID="lblcompanyname" runat="server" Text=""></asp:Label>
</td>
</tr>
<tr>
<td style="width: 169px">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Address :
</td>
<td>
 <asp:Label ID="lbladdress" runat="server" Text=""></asp:Label>
</td>
</tr>
<tr>
<td style="width: 169px">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
City :
</td>
<td>
 <asp:Label ID="lblcity" runat="server" Text=""></asp:Label>
</td>
</tr>
<tr>
<td style="width: 169px">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Country :
</td>
<td>
 <asp:Label ID="lblcountry" runat="server" Text=""></asp:Label>
</td>
</tr>
<tr>
<td style="width: 169px">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Zip Code :
</td>
<td>
 <asp:Label ID="lblzipcode" runat="server" Text=""></asp:Label>
</td>
</tr>
<tr>
<td style="width: 169px">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Tel No. :
</td>
<td>
 <asp:Label ID="lbltelno" runat="server" Text=""></asp:Label>
</td>
</tr>
<tr>
<td style="width: 169px">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Fax No. :
</td>
<td>
 <asp:Label ID="lblfaxno" runat="server" Text=""></asp:Label>
</td>
</tr>
<tr>
<td style="width: 169px">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
E-Mail :
</td>
<td>
 <asp:Label ID="lblemailid" runat="server" Text=""></asp:Label>
</td>
</tr>
<tr>
<td style="width: 169px">
Nature of Partnership :
</td>
<td>
 <asp:Label ID="lblnatureofpartnership" runat="server" Text=""></asp:Label>
</td>
</tr>
</table>
</asp:Content>


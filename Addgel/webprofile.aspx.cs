using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
public partial class admin_Default : System.Web.UI.Page
{
    
    public string strSQL_Append = string.Empty;

    DataTable dt;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            if (!IsPostBack)
            {
                gridbind();
            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }   
    }

    public void AdminSearch_Click(object sender, EventArgs e)
    {

        gridbind();

    }

    public void gridbind()
    {

        string strSearchSQL = string.Empty;

        //if (find.Text.Trim() != "" )
        //{
        //    lblmsg.Text = "";
        //    strSearchSQL = " WHERE web_type LIKE '%" + find.Text.Trim() + "%' or web_description LIKE '%" + find.Text.Trim() + "%' order by web_id asc";
        //    lblmsg.Text = "Your search for Keyword <b>" + find.Text.Trim() + "</b>  ";

        //}
    

        //else if (find.Text.Trim() == "")
        //{
        //    lblmsg.Text = "";
        //    strSearchSQL = " order by web_id asc";
        //    // lblmsg.Text = "Your search for customer name <b>" + find.Text.Trim() + "</b>  ";

        //}

       string sel;


       sel = "SELECT * FROM CMS_control" + strSearchSQL+"";
       dt = new DataTable();
       dt.Clear();
       dt = Class1.GetRecord(sel);
       dgrd_Product.DataSource = dt;
        dgrd_Product.DataBind();
        lblrecordperpageTop.Text += " There are total records found <b>" + dgrd_Product.Items.Count + "</b>";
                
    }

    //++++++++++++++++++++++++++++++++++++++++++++
    //Handles page change links - paging system
    public void New_Page(object sender, DataGridPageChangedEventArgs e)
    {

        dgrd_Product.CurrentPageIndex = e.NewPageIndex;

        gridbind();
    }
    //++++++++++++++++++++++++++++++++++++++++++++



    public void dgProduct_ItemDataBound(object sender, DataGridItemEventArgs e)
    {

        int strIDcell = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "web_id"));



        // int strIDcell = Convert.ToInt16( e.Item.ItemIndex.  

        //First, make sure we're not dealing with a Header or Footer row
        if (e.Item.ItemType != ListItemType.Header & e.Item.ItemType != ListItemType.Footer)
        {

            //LinkButton deleteButton = e.Item.Cells[7].Controls[0];
            //LinkButton editButton = e.Item.Cells[6].Controls[0];

            //We can now add the onclick event handler
            e.Item.Cells[4].Attributes["onclick"] = "javascript:return confirm('Are you sure you want to delete Profile ID # " + DataBinder.Eval(e.Item.DataItem, "web_id") + "?')";
            e.Item.Cells[4].ToolTip = "Delete  (" + DataBinder.Eval(e.Item.DataItem, "web_type") + ") ID #:" + DataBinder.Eval(e.Item.DataItem, "web_id");
            e.Item.Cells[3].ToolTip = "Edit  (" + DataBinder.Eval(e.Item.DataItem, "web_type") + ") ID #:" + DataBinder.Eval(e.Item.DataItem, "web_id");


            //Data row mouseover changecolor
            e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='#ECF5FF'");
            e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#ffffff'");

            //handle cell 1 Product name change cell and font color
            e.Item.Cells[1].Attributes.Add("onmouseover", "this.style.backgroundColor='#F0E68C';this.style.cursor='pointer';this.style.color='#ff3e3e'");
            e.Item.Cells[1].Attributes.Add("onmouseout", "this.style.backgroundColor='#fff';this.style.cursor='pointer';this.style.color='#048'");
            e.Item.Cells[1].ForeColor = System.Drawing.ColorTranslator.FromHtml("#048");

            //Handle cell 1 - Product name click event
           // e.Item.Cells[1].Attributes.Add("Onclick", "javascript:window.open('viewing_articles.aspx?id=" + strIDcell + "'," + "'','height=690,width=700,scrollbars=1')");

            //Display cell ToolTip in the grid
            e.Item.Cells[0].ToolTip = "Profile # " + DataBinder.Eval(e.Item.DataItem, "web_id");
            e.Item.Cells[1].ToolTip = "Click to view: " + DataBinder.Eval(e.Item.DataItem, "web_type") + " Profile";
            e.Item.Cells[1].ToolTip = "Last updated on: " + DataBinder.Eval(e.Item.DataItem, "web_post_date") + " ";
           

            ////If we're in the approval tab, then we change the ToolTip
            //if (Request("tab") == "1")
            //{


            //    e.Item.Cells(1).ToolTip = "Waiting for approval, click to approve: " + DataBinder.Eval(e.Item.DataItem, "Name") + " Product";
            //}

            ////Display the sorted category name from the dropdownlist
            //if (!string.IsNullOrEmpty(Request("catid")))
            //{

            //    lblSortedCat.Text = "Sorted Category: " + DataBinder.Eval(e.Item.DataItem, "Category");

            //    lblletterlegendcat.text = DataBinder.Eval(e.Item.DataItem, "Category") + "&nbsp;Products A-Z:";

            //}
        }

        //Handles the header link ToolTip
        //First, make sure we're dealing with a Header
        //if (e.Item.ItemType == ListItemType.Header)
        //{

        //    //Display cell header ToolTip
        //    e.Item.Cells(0).ToolTip = "Sort by ID - ASC or DESC";
        //    e.Item.Cells(1).ToolTip = "Sort by Product Name";
        //    e.Item.Cells(2).ToolTip = "Sort by Product Category";
        //    e.Item.Cells(3).ToolTip = "Sort by Author";
        //    e.Item.Cells(4).ToolTip = "Sort by Submitted date";
        //    e.Item.Cells(5).ToolTip = "Sort by Most Popular - Hits";

        //    //handle clickable and change color cell header on mouseover
        //    e.Item.Cells(0).Attributes.Add("onmouseover", "this.style.backgroundColor='#428DFF';this.style.cursor='pointer'");
        //    e.Item.Cells(0).Attributes.Add("onmouseout", "this.style.backgroundColor='#79AEFF';this.style.cursor='pointer'");
        //    e.Item.Cells(0).Attributes.Add("Onclick", "javascript:__doPostBack('dgrd_Product$_ctl2$_ctl0')");
        //    e.Item.Cells(1).Attributes.Add("onmouseover", "this.style.backgroundColor='#428DFF';this.style.cursor='pointer'");
        //    e.Item.Cells(1).Attributes.Add("onmouseout", "this.style.backgroundColor='#79AEFF';this.style.cursor='pointer'");
        //    e.Item.Cells(1).Attributes.Add("Onclick", "javascript:__doPostBack('dgrd_Product$_ctl2$_ctl1')");
        //    e.Item.Cells(2).Attributes.Add("onmouseover", "this.style.backgroundColor='#428DFF';this.style.cursor='pointer'");
        //    e.Item.Cells(2).Attributes.Add("onmouseout", "this.style.backgroundColor='#79AEFF';this.style.cursor='pointer'");
        //    e.Item.Cells(2).Attributes.Add("Onclick", "javascript:__doPostBack('dgrd_Product$_ctl2$_ctl2')");
        //    e.Item.Cells(3).Attributes.Add("onmouseover", "this.style.backgroundColor='#428DFF';this.style.cursor='pointer'");
        //    e.Item.Cells(3).Attributes.Add("onmouseout", "this.style.backgroundColor='#79AEFF';this.style.cursor='pointer'");
        //    e.Item.Cells(3).Attributes.Add("Onclick", "javascript:__doPostBack('dgrd_Product$_ctl2$_ctl3')");
        //    e.Item.Cells(4).Attributes.Add("onmouseover", "this.style.backgroundColor='#428DFF';this.style.cursor='pointer'");
        //    e.Item.Cells(4).Attributes.Add("onmouseout", "this.style.backgroundColor='#79AEFF';this.style.cursor='pointer'");
        //    e.Item.Cells(4).Attributes.Add("Onclick", "javascript:__doPostBack('dgrd_Product$_ctl2$_ctl4')");
        //    e.Item.Cells(5).Attributes.Add("onmouseover", "this.style.backgroundColor='#428DFF';this.style.cursor='pointer'");
        //    e.Item.Cells(5).Attributes.Add("onmouseout", "this.style.backgroundColor='#79AEFF';this.style.cursor='pointer'");

        //    e.Item.Cells(5).Attributes.Add("Onclick", "javascript:__doPostBack('dgrd_Product$_ctl2$_ctl5')");
        //}



        //Display the pagecount in the top and footer
        int pageindex = dgrd_Product.CurrentPageIndex + 1;
        //LblPageInfo.Text = "Page " + pageindex.ToString() + " of " + dgrd_Product.PageCount.ToString() + "&nbsp;&nbsp;";

        LblPageInfoTop.Text = "Showing Page " + pageindex.ToString() + " of " + dgrd_Product.PageCount.ToString();
    }
    //++++++++++++++++++++++++++++++++++++++++++++


    private void MessageBox(string strMsg)
    {

        // generate a popup message using javascript alert function
        // dumped into a label with the string argument passed in
        // to supply the alert box text
        Label lbl = new Label();
        lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert(" + "'" + strMsg + "'" + ")</script>";

        // add the label to the page to display the alert

        Page.Controls.Add(lbl);
    }


    //++++++++++++++++++++++++++++++++++++++++++++
    //Handle the deletebutton click event
    public void Delete_Products(object sender, DataGridCommandEventArgs e)
    {

        if ((e.CommandName == "Delete"))
        {
            TableCell iIdNumber2 = e.Item.Cells[0];
            TableCell iIProductname = e.Item.Cells[1];



            string del_strsql = "";

            del_strsql = " delete from webprofile where id  =" + dgrd_Product.DataKeys[e.Item.ItemIndex]+"";

            dt=Class1.GetRecord(del_strsql);


            MessageBox("The articles has been deleted successfully !!");

            gridbind();

            //strURLRedirect = "confirmdel.aspx?catname=" + iIProductname.Text + "&mode=del";
            //Server.Transfer(strURLRedirect);
        }


    }
    //++++++++++++++++++++++++++++++++++++++++++++




    //Handle edit databound
    public void Edit_Handle(object sender, DataGridCommandEventArgs e)
    {

        if ((e.CommandName == "edit"))
        {
            // TableCell iIdNumber = dgrd_Product.DataKeys[e.Item.ItemIndex];

            Response.Redirect("updatearticle.aspx?id=" + dgrd_Product.DataKeys[e.Item.ItemIndex]);

            //Server.Transfer(strURLRedirect);

        }
    }


    private string strSQL;
    private string strURLRedirect;
    private int ResultCount;
    private string strSearchSQL;
}

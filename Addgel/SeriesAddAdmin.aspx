﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="SeriesAddAdmin.aspx.cs" Inherits="Admin_SeriesAddAdmin" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%">
       <tr>
<td colspan="2" align="center">
    <b>Pen Series Add   </b><hr />
</td>
</tr>   
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="lbladd" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
        <td style="width: 130px">
            Series Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
            <asp:DropDownList ID="DDLCategory" runat="server" Height="20px" Width="188px" 
                DataTextField="C_CategoryName" DataValueField="C_Id">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ControlToValidate="DDLCategory" ErrorMessage="Please select category" 
                InitialValue="0" ValidationGroup="1"></asp:RequiredFieldValidator>
        </td>
        </tr>
          <tr>
            <td style="width: 124px">
                Series Head
                </td>
            <td>
                <asp:TextBox ID="txthead" runat="server" Height="21px" Width="313px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                    ControlToValidate="txthead" ErrorMessage="Field cannot be blank" 
                    ValidationGroup="1"></asp:RequiredFieldValidator>
                </td>
        </tr>
        <tr>
            <td style="width: 130px">
                Description</td>
            <td>
                <asp:TextBox ID="txtdescription" TextMode="MultiLine" runat="server" 
                    Height="200px" Width="400px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="txtdescription" ErrorMessage="Field cannot be blank" 
                    ValidationGroup="1"></asp:RequiredFieldValidator>
                </td>
        </tr>
        
        
        
        
        <tr>
        <td style="width: 130px; height: 33px;">
           Banner Image</td>
        <td style="height: 33px">
            <asp:FileUpload ID="FUlarge" runat="server" />
            <asp:RegularExpressionValidator 
ID="RegularExpressionValidator2" runat="server" ControlToValidate="FUlarge" Display="None" 
 ErrorMessage="Please Add Only Image " ValidationExpression="^.+\.((jpg)|(gif)|(jpeg)|(png)|(bmp))$">
</asp:RegularExpressionValidator>
        </td>
        </tr>
        
        
        
        <tr>
        <td style="width: 130px; height: 33px;">
          Banner Footer Image</td>
        <td style="height: 33px">
            <asp:FileUpload ID="FUBannerFooter" runat="server" />
            <asp:RegularExpressionValidator 
ID="RegularExpressionValidator1" runat="server" ControlToValidate="FUBannerFooter" Display="None" 
 ErrorMessage="Please Add Only Image " ValidationExpression="^.+\.((jpg)|(gif)|(jpeg)|(png)|(bmp))$">
</asp:RegularExpressionValidator>
        </td>
        </tr>
        
        <tr>
        <td style="width: 130px; height: 33px;">
            Logo</td>
        <td style="height: 33px">
            <asp:FileUpload ID="FULogo" runat="server" />
            <asp:RegularExpressionValidator 
ID="RegularExpressionValidator3" runat="server" ControlToValidate="FULogo" Display="None" 
 ErrorMessage="Please Add Only Image " ValidationExpression="^.+\.((jpg)|(gif)|(jpeg)|(png)|(bmp))$">
</asp:RegularExpressionValidator>
        </td>
        </tr>
        
        
        
        <tr>
            <td colspan="2" align="center">
                <asp:Button ID="btnadd" runat="server" Text="Save" onclick="btnadd_Click" 
                    style="height: 26px" ValidationGroup="1" />
                    
                      </td>
        </tr>
    </table>
</asp:Content>


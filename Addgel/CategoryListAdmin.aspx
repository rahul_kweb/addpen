﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="CategoryListAdmin.aspx.cs" Inherits="Admin_CategoryListAdmin" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="100%">
<tr>
<td align="center">
 
Category Listing <hr />
</td>
</tr>
<tr>
<td >
             
    <asp:RadioButtonList ID="RBLStatus" runat="server" AutoPostBack="True" 
        RepeatDirection="Horizontal" 
        onselectedindexchanged="RBLStatus_SelectedIndexChanged">
        <asp:ListItem Selected="True">Active</asp:ListItem>
        <asp:ListItem>InActive</asp:ListItem>
    </asp:RadioButtonList>
</td>
</tr>
<tr>
<td style="height: 25px">
   <asp:DropDownList ID="DDLSelectSeries" runat="server" 
        AutoPostBack="True" 
        onselectedindexchanged="DDLSelectSeries_SelectedIndexChanged">
                    
                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                    
                    <asp:ListItem Value="1">Pen</asp:ListItem>
                    <asp:ListItem Value="2">Refill</asp:ListItem>
                </asp:DropDownList>
&nbsp;&nbsp;&nbsp;&nbsp;
          <asp:DropDownList ID="DDLCategory" Height="20px" Width="188px" runat="server"
             DataTextField="C_CategoryName" DataValueField="C_Id" AutoPostBack="True" 
                onselectedindexchanged="DDLRefilecategory_SelectedIndexChanged">
            </asp:DropDownList>   
           
</td>
</tr>
<tr>
<td align="center">
    <asp:Label ID="lblupdate" runat="server" Text=""></asp:Label>
</td>
</tr>
<tr>
<td>
<br />
</td>
</tr>
<tr>
<td>
    <asp:GridView ID="GVCategory" Width="100%" runat="server" 
        AutoGenerateColumns="False" EmptyDataText="NO RECORD FOUND" 
        onrowdeleting="GVCategory_RowDeleting" 
        onrowcancelingedit="GVCategory_RowCancelingEdit" 
        onrowediting="GVCategory_RowEditing" onrowupdating="GVCategory_RowUpdating">
        <Columns>
            <asp:BoundField DataField="C_PId" HeaderText="PID" ReadOnly="True" />
            <asp:BoundField DataField="C_Id" HeaderText="Id" ReadOnly="True" 
                Visible="False" />
            <asp:TemplateField Visible="False">
                <EditItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("C_Id") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblid" runat="server" Text='<%# Bind("C_Id") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Category Name">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("C_CategoryName") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("C_CategoryName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:CheckBox ID="chkstatus" runat="server" 
                        Checked='<%# Eval("C_Status").ToString().Equals("Active") %>' 
                        oncheckedchanged="chkstatus_CheckedChanged" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="C_Date" HeaderText="Date" 
                DataFormatString="{0:M-dd-yyyy}" ReadOnly="True" />
            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" />
            <asp:CommandField HeaderText="Edit" ShowEditButton="True" />
        </Columns>
    </asp:GridView>
</td>
</tr>
<tr>
<td align="center">
    <asp:Button ID="btnapply" runat="server" Text="Apply Change" 
        onclick="btnapply_Click" />
</td>
</tr>
</table>
</asp:Content>


﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class HistoryAddAdmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {

        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }

    }
    protected void btnadd_Click(object sender, EventArgs e)
    {
        string year = txtyear.Text.Replace("'", "''");
        string description = txtdescription.Text.Replace("'", "''");

        bool check = Class1.InsertUpdate("insert into HistoryTB values('" + year + "','" + description + "','Active')");
        if (check)
        {
            txtyear.Text = "";
            txtdescription.Text = "";
            lbladd.Text = "Year " + year + " is added sucessfully";
        }
    }
}

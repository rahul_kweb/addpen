﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_RefillSeriesDetailsupdate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    int id = Convert.ToInt32(Request.QueryString["id"]);

                    BindProductperbyid(id);
                    HiddenField1.Value = id.ToString();
                }
            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }
    }

    private void BindProductperbyid(int id)
    {
        DataTable dt = Class1.GetRecord("select * from RefillSeriesTB where RSeries_Id=" + id + "");

        txthead.Text = dt.Rows[0]["RSeries_Head"].ToString();
        txtdescription.Text = dt.Rows[0]["RSeries_Description"].ToString();

        Image1.ImageUrl = "~/Addgel/AdminImage/" + dt.Rows[0]["RSeries_BannerImage"].ToString();
        Image2.ImageUrl = "~/Addgel/AdminImage/" + dt.Rows[0]["RSeries_BannerFooterImage"].ToString();
        Image3.ImageUrl = "~/Addgel/AdminImage/" + dt.Rows[0]["RSeries_LogoImage"].ToString();

        hfbannerimg.Value = dt.Rows[0]["RSeries_BannerImage"].ToString();
        hfbannerfooter.Value = dt.Rows[0]["RSeries_BannerFooterImage"].ToString();
        hflogo.Value = dt.Rows[0]["RSeries_LogoImage"].ToString();

    }
    protected void btnadd_Click(object sender, EventArgs e)
    {
        string prohead = txthead.Text.Replace("'", "''");
        string prodescription = txtdescription.Text.Replace("'", "''");
        string seriesbanner = "";
        string seriesbannerfooter = "";
        string serieslogo = "";

        if (FUlarge.HasFile == true)
        {
            seriesbanner = "AddpenRefillSeriesBannner" + HiddenField1.Value + "" + System.IO.Path.GetExtension(FUlarge.FileName);
            FUlarge.SaveAs(Server.MapPath("adminImage\\" + seriesbanner + ""));
        }
        else
        {
            seriesbanner = hfbannerimg.Value;
        }
        if (FUBannerFooter.HasFile == true)
        {
            seriesbannerfooter = "AddpenRefillSeriesBannerFooter" + HiddenField1.Value + "" + System.IO.Path.GetExtension(FUBannerFooter.FileName);
            FUBannerFooter.SaveAs(Server.MapPath("adminImage\\" + seriesbannerfooter + ""));
        }
        else
        {
            seriesbannerfooter = hfbannerfooter.Value;
        }
        if (FULogo.HasFile == true)
        {
            serieslogo = "AddpenRefillSeriesLogo" + HiddenField1.Value + "" + System.IO.Path.GetExtension(FULogo.FileName);
            FULogo.SaveAs(Server.MapPath("adminImage\\" + serieslogo + ""));
        }
        else
        {
            serieslogo = hflogo.Value;
        }
        bool check = Class1.InsertUpdate("update RefillSeriesTB set RSeries_Head='" + prohead + "',RSeries_Description='" + prodescription + "',RSeries_BannerImage='" + seriesbanner + "',RSeries_BannerFooterImage='" + seriesbannerfooter + "',RSeries_LogoImage='" + serieslogo + "' where RSeries_Id=" + Convert.ToInt32(Request.QueryString["id"]) + "");
        if (check)
        {
            BindProductperbyid(Convert.ToInt32(Request.QueryString["id"]));
            lbladd.Text = "Updated Successfully";
        }       

    }
    //protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    //{
    //    bool check = Class1.InsertUpdate("update RefillSeriesTB set  RSeries_BannerImage='' where RSeries_Id=" + Convert.ToInt32(Request.QueryString["id"]) + "");
    //    if (check)
    //    {
    //        BindProductperbyid(Convert.ToInt32(Request.QueryString["id"]));
    //    }
    //}
    //protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    //{
    //    bool check = Class1.InsertUpdate("update RefillSeriesTB set  RSeries_BannerFooterImage='' where RSeries_Id=" + Convert.ToInt32(Request.QueryString["id"]) + "");
    //    if (check)
    //    {
    //        BindProductperbyid(Convert.ToInt32(Request.QueryString["id"]));
    //    }
    //}
    protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
    {
        bool check = Class1.InsertUpdate("update RefillSeriesTB set RSeries_LogoImage='' where RSeries_Id=" + Convert.ToInt32(Request.QueryString["id"]) + "");
        if (check)
        {
            BindProductperbyid(Convert.ToInt32(Request.QueryString["id"]));
        }
    }
}

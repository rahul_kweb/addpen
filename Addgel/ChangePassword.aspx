<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true"
    CodeFile="ChangePassword.aspx.cs" Inherits="Admin_ChangePassword" Title="~~ : Welcome to  Password Change : ~~" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table  width="75%" >
    <tr>
<td colspan="2" align="center">
 
</td>
</tr>
        <tr valign="top">
        <td width="10%" rowspan="7"> </td>
            <td colspan="2" align="center"  valign="top">
            <b>
             Change Password</b><hr />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="LblMessage" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
    <td colspan="2"></td>
    </tr>
        <tr>
            <td>
                Current Password
            </td>
            <td>
                <asp:TextBox ID="TxtCurrentPassword" runat="server" TextMode="Password" MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                New Password
            </td>
            <td>
                <asp:TextBox ID="TxtNewPassword" runat="server" TextMode="Password" MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Confirm New
            </td>
            <td>
                <asp:TextBox ID="TxtConfirmPassword" runat="server" TextMode="Password" MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        <tr><td></td>
            <td  align="left">
                <asp:Button ID="BtnApplyChange" runat="server" Text="Apply Change" OnClick="BtnApplyChange_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:RequiredFieldValidator ID="RFVCurrent" runat="server" ErrorMessage="Enter Current Password"
                    ControlToValidate="TxtCurrentPassword" Display="None"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RFVNew" runat="server" ErrorMessage="Enter New Password"
                    ControlToValidate="TxtNewPassword" Display="None"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RFVConfirm" runat="server" ErrorMessage="Enter Confirm Password"
                    ControlToValidate="TxtConfirmPassword" Display="None"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CVPassword" runat="server" ErrorMessage="Password Do Not Match"
                    ControlToCompare="TxtConfirmPassword" ControlToValidate="TxtNewPassword" Display="None"></asp:CompareValidator><br />
                &nbsp;<asp:RegularExpressionValidator ID="RENewPassword" runat="server" ControlToValidate="TxtNewPassword"
                    Display="None" ErrorMessage="Enter  alphanurmeric min 5 " ValidationExpression="^[a-zA-Z0-9]{5,}"></asp:RegularExpressionValidator>
            </td>
        </tr>
    </table>
    <asp:ValidationSummary ID="ValidationSummaryChangePassword" runat="server" ShowMessageBox="True"
        ShowSummary="False" />
</asp:Content>

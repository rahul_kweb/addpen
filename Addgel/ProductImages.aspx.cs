﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_ProductImages : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            lblsave.Text = "";

        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }

    }

    protected void btnimage_Click(object sender, EventArgs e)
    {
        int id = Convert.ToInt32(Request.QueryString["id"]);


        bool check = Class1.InsertUpdate("insert into ProductImageGalleryTB values(" + id + ",'" + FUthumbnail.FileName + "','" + FUZoom.FileName + "',getdate())");
        if (FUthumbnail != null)
        {

            FUthumbnail.SaveAs(Server.MapPath("~/Addgel/AdminImage//" + FUthumbnail.FileName + ""));
        }
        if (FUZoom != null)
        {

            FUZoom.SaveAs(Server.MapPath("~/Addgel/AdminImage//" + FUZoom.FileName + ""));
        }

        lblsave.Text = "Save Successfully";    
    }
}
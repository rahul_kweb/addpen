﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Addgel_HistoryDetailsadmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            lbladd.Text = "";
            if (!IsPostBack)
            {
                int id = Convert.ToInt32(Request.QueryString["id"]);
                bindhistoryperid(id);
            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }
    }
    private void bindhistoryperid(int id)
    {
        DataTable dt = Class1.GetRecord("select * from HistoryTB where H_Id='" + id + "'");
        txtyear.Text = dt.Rows[0]["H_Year"].ToString();
        txtdescription.Text = dt.Rows[0]["H_Description"].ToString();
       
    }
    protected void btnadd_Click(object sender, EventArgs e)
    {
        string year = txtyear.Text.Replace("'", "''");
        string description = txtdescription.Text.Replace("'", "''");

        bool check = Class1.InsertUpdate("update HistoryTB set H_Year='" + year + "',H_Description='" + description + "' where H_Id=" + Convert.ToInt32(Request.QueryString["id"]) + "");
        if (check)
        {
            lbladd.Text = "<strong> Year " + year + " updated sucessfully</strong>";
        }
    }
}

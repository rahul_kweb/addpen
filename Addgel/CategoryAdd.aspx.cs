﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_CategoryAdd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Check"] != null)
        {
            lbladd.Text = "";
            if (!IsPostBack)
            {
                BindDropDown(1);
            }

        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }

    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        bool check = Class1.categoryInsert(txtbrandname.Text.Replace("'", "''"),Convert.ToInt32(DDLSelectSeries.SelectedValue));
        if (check == true)//  = if (check)
        {
            lbladd.Text = "Save Successfully";
            BindDropDown(Convert.ToInt32(DDLSelectSeries.SelectedValue));
        }
        else if (check == false) // = else if(!check)
        {
            lbladd.Text = "Category Already Exits";
        }
    }
    private void BindDropDown(int id)
    {
        DataTable dt = Class1.GetRecord("select * from CategoryTB where C_CatType="+id+"");
        if (dt.Rows.Count > 0)
        {
            DDLCategory.DataSource = dt;
            DDLCategory.DataBind();
        }
        else
        {
            DDLCategory.Visible = false;
        }
    }
    protected void DDLSelectSeries_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = Class1.GetRecord("select * from CategoryTB where C_CatType=" + Convert.ToInt32(DDLSelectSeries.SelectedValue) + "");
        if (dt.Rows.Count > 0)
        {
            DDLCategory.DataSource = dt;
            DDLCategory.DataBind();
        }
        else
        {
            DDLCategory.Visible = false;
        }
    }
}

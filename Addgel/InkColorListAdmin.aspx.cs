﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Addgel_InkColorListAdmin : System.Web.UI.Page
{
    bool[] rowChanged;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            lblupdate.Text = "";
            rowChanged = new bool[30];
            if (!IsPostBack)
            {
                CategoryBind("Active");
               

            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }

    }


    private void CategoryBind(string status)
    {
        DataTable dt = Class1.GetRecord("select * from InkColorMTB where InkColorM_Status='" + status + "' order by InkColorM_id");
        GVCategory.DataSource = dt;
        GVCategory.DataBind();


    }
    protected void btnapply_Click(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsPostBack)
            {
                string IdList = "";
                int totalRows = 30;
                for (int r = 0; r < totalRows; r++)
                {
                    if (rowChanged[r])
                    {
                        GridViewRow thisGridViewRow = GVCategory.Rows[r];


                        Label LId = (Label)thisGridViewRow.FindControl("lblid");
                        int NewId = Convert.ToInt32(LId.Text);

                        IdList = IdList + " " + NewId;

                        CheckBox chkdeal = (CheckBox)thisGridViewRow.FindControl("chkstatus");
                        string status = "InActive";

                        if (chkdeal.Checked)
                        {
                            status = "Active";
                        }
                    
                        bool check = Class1.InsertUpdate("update InkColorMTB set InkColorM_Status='" + status + "' where InkColorM_id =" + NewId + "");

                    }
                }

                CategoryBind(RBLStatus.SelectedValue);
                if (IdList == "")
                {
                    lblupdate.Text = "No Change Occur";
                }
                else
                {
                    lblupdate.Text = "ID" + IdList + " Have been Updated";
                }
            }
        }
        catch (Exception ex)
        {
            lblupdate.Text = ex.Message;
        }
    }
    protected void chkstatus_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox thisTextBox = (CheckBox)sender;
            GridViewRow thisGridViewRow = (GridViewRow)thisTextBox.Parent.Parent;
            int row = thisGridViewRow.RowIndex;
            rowChanged[row] = true;
        }
        catch (Exception ex)
        {
            lblupdate.Text = ex.Message.ToString();
        }
    }
    protected void RBLStatus_SelectedIndexChanged(object sender, EventArgs e)
    {       
     CategoryBind(RBLStatus.SelectedValue);      

    }
    protected void GVCategory_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(GVCategory.Rows[e.RowIndex].Cells[0].Text);
        bool check = Class1.InsertUpdate("Delete from InkColorMTB where InkColorM_id=" + id + "");
        if (check)
        {
            CategoryBind(RBLStatus.SelectedValue);
        }
       
    }
    protected void GVCategory_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GVCategory.EditIndex = e.NewEditIndex;
        CategoryBind(RBLStatus.SelectedValue);
    }
    protected void GVCategory_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GVCategory.EditIndex = -1;
        CategoryBind(RBLStatus.SelectedValue);
    }
    protected void GVCategory_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string pk = ((Label)GVCategory.Rows[GVCategory.EditIndex].FindControl("Label1")).Text;

        string name = ((TextBox)GVCategory.Rows[GVCategory.EditIndex].FindControl("TextBox1")).Text;
        string hashcode = ((TextBox)GVCategory.Rows[GVCategory.EditIndex].FindControl("TextBox2")).Text;

        string str = "update InkColorMTB set InkColorM_Name='" + name + "',InkColorM_Code='" + hashcode + "' where InkColorM_id=" + pk + "";
        bool check = Class1.InsertUpdate(str);
        if (check == true)
        {
            GVCategory.EditIndex = -1;
            CategoryBind(RBLStatus.SelectedValue);
        }
    }
}

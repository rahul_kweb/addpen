﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="ProductImages.aspx.cs" Inherits="Admin_ProductImages" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%">
<tr>
<td colspan="2">
<b>Add Images</b><hr />
</td>
</tr>
<tr>
<td colspan="2">
    <asp:Label ID="lblsave" runat="server" Text=""></asp:Label>
</td>
</tr>

<tr>
<td>
Thumbnail Images
</td>
<td>
    <asp:FileUpload ID="FUthumbnail" runat="server" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
        ControlToValidate="FUZoom" ErrorMessage="Thumbnail Image Cannot Be Blank"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
        ControlToValidate="FUthumbnail" ErrorMessage="Add only Images in Thumbnail" 
        ValidationExpression="^.+\.((jpg)|(gif)|(jpeg)|(png)|(bmp))$" ></asp:RegularExpressionValidator>
</td>
</tr>
<tr>
<td>
Zoom Images
</td>
<td>
    <asp:FileUpload ID="FUZoom" runat="server" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
        ControlToValidate="FUZoom" ErrorMessage="Zoom Image Cannot Be Blank"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
        ControlToValidate="FUZoom" ErrorMessage="Add only Images in Zoom" 
        ValidationExpression="^.+\.((jpg)|(gif)|(jpeg)|(png)|(bmp))$"></asp:RegularExpressionValidator>
</td>
</tr>

<tr>
<td>
    &nbsp;</td>
<td>
 <asp:Button ID="btnimage" runat="server" Text="Save Images" 
        onclick="btnimage_Click" />
 
</td>
</tr>
<tr>
<td colspan="2">

    &nbsp;</td>
</tr>
</table>
</asp:Content>


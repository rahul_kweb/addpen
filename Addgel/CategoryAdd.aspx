﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="CategoryAdd.aspx.cs" Inherits="Admin_CategoryAdd" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table>
       <tr>
<td colspan="2" align="center">
<b> Category Add And List It  </b><hr />
</td>
</tr>
    
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="lbladd" runat="server" Text=""></asp:Label>
            </td>
        </tr>
       <tr>
            <td>
                Select Category
                Type
            </td>
            <td>
                <asp:DropDownList ID="DDLSelectSeries" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDLSelectSeries_SelectedIndexChanged">
                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                    <asp:ListItem Value="1">Pen</asp:ListItem>
                    <asp:ListItem Value="2">Refill</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="DDLSelectSeries" ErrorMessage="Please Select Type" 
                    InitialValue="0" ValidationGroup="1"></asp:RequiredFieldValidator>
            </td>
        </tr> 
        
        <tr>
            <td>
                Current Category
            </td>
            <td>
                <asp:DropDownList ID="DDLCategory" runat="server" 
                    DataTextField="C_CategoryName" DataValueField="C_Id">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Add New Category
            </td>
            <td>
                <asp:TextBox ID="txtbrandname" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtbrandname" 
                    ErrorMessage="Category Name Cannot Be Blank" ValidationGroup="1"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="btnadd" runat="server" Text="Submit" OnClick="btnadd_Click" ValidationGroup="1"/>
            </td>
        </tr>
    </table>
</asp:Content>


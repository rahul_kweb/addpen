using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;



public partial class Admin_AdminPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        TxtName.Focus();
        Lbltry.Text = "";
    }

    protected void BtnLogin_Click(object sender, EventArgs e)
    {
        try
        {
           // string encode = base64Encode(TxtPassword.Text.Trim());
            bool Check = Class1.Validation(TxtName.Text.Trim(), TxtPassword.Text.Trim());
            if (Check)
            {
                DataTable dt = Class1.GetRecord("select * from UserTab where UserId=1");
                Session["datetime"] = dt;
                bool datecheck = Class1.InsertUpdate("update UserTab set UserLogin=getdate(),UserLogout=getdate() where UserID=1");
                Session["Check"] = TxtName.Text.Trim();
                Session["password"] = TxtPassword.Text.Trim();
                // FormsAuthentication.RedirectFromLoginPage(TxtName.Text, false);
                Response.Redirect("~/Addgel/Default.aspx");
            }
            else
            {
                Lbltry.Text = "InValid User";
            }
        }
        catch (Exception ex)
        {
            Lbltry.Text = ex.Message.ToString();
        }
    }

    private string base64Encode(string sData)
    {
        try
        {
            byte[] encData_byte = new byte[sData.Length];

            encData_byte = System.Text.Encoding.UTF8.GetBytes(sData);

            string encodedData = Convert.ToBase64String(encData_byte);

            return encodedData;

        }
        catch (Exception ex)
        {
            throw new Exception("Error in base64Encode" + ex.Message);
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            string encode = base64Encode(TxtPassword.Text.Trim());
            bool Check = Class1.Validation(TxtName.Text.Trim(), encode);
            if (Check)
            {
                Session["Check"] = TxtName.Text.Trim();
                Session["password"] = TxtPassword.Text.Trim();
                // FormsAuthentication.RedirectFromLoginPage(TxtName.Text, false);
                Response.Redirect("~/Addgel/Default.aspx");
            }
            else
            {
                Lbltry.Text = "InValid User";
            }
        }
        catch (Exception ex)
        {
            Lbltry.Text = ex.Message.ToString();
        }
    }
}

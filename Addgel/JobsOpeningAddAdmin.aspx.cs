﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_JobsOpeningAddAdmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            lbladd.Text = "";
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }
        
    }
    protected void btnadd_Click(object sender, EventArgs e)
    {
        string title = txttitle.Text.Replace("'","''");
        string primaryskill = txtprimaryskills.Text.Replace("'","''");
        string location = txtlocation.Text.Replace("'","''");
        string description = txtdescription.Text.Replace("'","''");
        string skills = txtskills.Text.Replace("'","''");
        string Experience = txtexperience.Text.Replace("'","''");
        string salary = txtsalary.Text.Replace("'","''");
        string requirement = txtrequirement.Text.Replace("'","''");
        string date = DateTime.Today.ToShortDateString();

        bool check = Class1.InsertUpdate("insert into JobsTB values('" + title + "','" + primaryskill + "','" + location + "','" + description + "','" + skills + "','" + Experience + "','" + salary + "','" + requirement + "','Active','" + date + "')");       
        lbladd.Text = "Jobs with title " + title + " successfully added";

        txttitle.Text = "";
        txtprimaryskills.Text = "";
        txtlocation.Text = "";
        txtdescription.Text = "";
        txtskills.Text = "";
        txtexperience.Text = "";
        txtsalary.Text = "";
        txtrequirement.Text = "";
        
    }
}

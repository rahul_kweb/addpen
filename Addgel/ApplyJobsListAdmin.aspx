﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="ApplyJobsListAdmin.aspx.cs" Inherits="Admin_ApplyJobsListAdmin" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table  border="1" width="100%">
<tr>
<td colspan="3" align="center">
Job Application
</td>
</tr>
<tr>
<td colspan="4">
    <asp:Label ID="LblHead" runat="server" Text=""></asp:Label>
</td>

</tr>
<tr>
    <td align="left" width="20%">
        <asp:DropDownList ID="DDLStatus" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DDLStatus_SelectedIndexChanged">
            <asp:ListItem>UnCheck</asp:ListItem>
            <asp:ListItem>Check</asp:ListItem>
            <asp:ListItem>Special</asp:ListItem>
            <asp:ListItem>All</asp:ListItem>
        </asp:DropDownList> 
    </td>
    
    <td align="left" width="50%">
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="LblApplicationMessage" runat="server" Text=""></asp:Label>
    </td>
    <td align="center">
        <asp:Button ID="BtnMode" runat="server" Text="Mode" onclick="BtnMode_Click" />
    </td>
    
    </tr>
    <tr>
    <td colspan="3">
        <asp:GridView ID="GVJobApplication" Width="100%" runat="server" 
            AllowPaging="True" EmptyDataText="No Application"
            OnPageIndexChanging="GVJobApplication_PageIndexChanging" 
            AutoGenerateColumns="False" OnRowDataBound="GVJobApplication_RowDataBound" 
            onrowdeleting="GVJobApplication_RowDeleting">
            <Columns>
                <asp:BoundField DataField="ApplyJobs_Id" HeaderText="Id" />
                <asp:TemplateField HeaderText=" ID">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ApplyJobs_Id") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="LblID" runat="server" Text='<%# Bind("ApplyJobs_Id") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="JobID" HeaderText="Job ID" Visible="False" />                
                <asp:BoundField DataField="ApplyJobs_Name" HeaderText="Name" />                
                <asp:BoundField DataField="ApplyJobs_EmailId" HeaderText="Email" />
              
                <asp:TemplateField HeaderText="Status">
                 <ItemTemplate>
                    <asp:DropDownList ID="DDLChangeStatus" runat="server" 
                        SelectedValue='<%# Eval("ApplyJobs_Status") %>' OnSelectedIndexChanged="DDLChangeStatus_SelectedIndexChanged">
                        <asp:ListItem Selected="True">UnCheck</asp:ListItem>
                        <asp:ListItem>Check</asp:ListItem>
                        <asp:ListItem>Special</asp:ListItem>
                    </asp:DropDownList>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="File Name">
                <ItemTemplate>
<asp:HyperLink runat ="server" ID ="HLFileName" ForeColor="Blue" Target="_blank" Text= '<%#Bind("ApplyJobs_Resume") %>'        NavigateUrl=''>
</asp:HyperLink>
</ItemTemplate>

                </asp:TemplateField>
                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" />
            </Columns>
        </asp:GridView>
    
    </td>
    </tr>
    <tr>
    <td align="center" colspan="2">
        <asp:Button ID="BtnSave" runat="server" Text="Save Status Changed" OnClientClick="return confirm('Are you sure you want to update record?');" OnClick="BtnSave_Click"/>
    </td>
    </tr>
</table>
</asp:Content>


﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="ProductDetailsupdate.aspx.cs" Inherits="Admin_ProductDetailsupdate" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">



    function selectAll() {
        var box = document.getElementById('<%= lstrefillupdate.ClientID%>');
        if (box.length == 0) {
            alert('Please Select Color');
            document.getElementById('<%= lstrefill.ClientID%>').focus();
            return false;
        }
        else {
            for (var i = 0; i < box.length; i++) {
                box[i].selected = true;
            }
            return true;
        }
    }

    function checkvalidation() {
        if (document.frm.lstrefillupdate.value == "") {
            alert("Select Color!");
            return false;
        }
        return true;
    }





    function selectAll1() {
        var box = document.getElementById('<%= lstcolorupdate.ClientID%>');
        if (box.length == 0) {
            alert('Please Select Color');
            document.getElementById('<%= ListBox1.ClientID%>').focus();
            return false;
        }
        else {
            for (var i = 0; i < box.length; i++) {
                box[i].selected = true;
            }
            return true;
        }
    }

    function checkvalidation1() {
        if (document.frm.lstcolorupdate.value == "") {
            alert("Select Color!");
            return false;
        }
        return true;
    }
    
    </script>
<table width="100%">
       <tr>
<td colspan="4" align="center">
<b> Product Add   </b><hr />
</td>
</tr>
    
        <tr>
            <td align="center" colspan="4">
                <asp:Label ID="lblupdate" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        
        <tr>
            <td>
                Product Name
                </td>
            <td colspan="3">
                <asp:TextBox ID="txtproductname" runat="server" Height="21px" Width="210px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="txtproductname" ErrorMessage="Field cannot be blank" 
                    ValidationGroup="1"></asp:RequiredFieldValidator>
                </td>
        </tr>
                <tr>
            <td style="width: 124px">
                Product Head
                </td>
            <td colspan="3">
                <asp:TextBox ID="txthead" runat="server" Height="21px" Width="313px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                    ControlToValidate="txthead" ErrorMessage="Field cannot be blank" 
                    ValidationGroup="1"></asp:RequiredFieldValidator>
                </td>
        </tr>
        <tr>
            <td style="width: 124px">
                Description</td>
            <td colspan="3">
                <asp:TextBox ID="txtdescription" TextMode="MultiLine" runat="server" 
                    Height="130px" Width="313px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="txtdescription" ErrorMessage="Field cannot be blank" 
                    ValidationGroup="1"></asp:RequiredFieldValidator>
                </td>
        </tr>
        
        <tr>
            <td style="width: 124px">
                Features<br />
                <asp:Button ID="btnnewline" runat="server" Text="New Line" 
                    onclick="btnnewline_Click" />
                                      </td>
            <td colspan="3"> 
                <asp:TextBox ID="txtfeature" TextMode="MultiLine" runat="server" 
                    Height="130px" Width="313px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                    ControlToValidate="txtfeature" ErrorMessage="Field cannot be blank" 
                    ValidationGroup="1"></asp:RequiredFieldValidator>
                </td>
        </tr>
        <%----%>
        <tr>
        <td>
        Refill Category
        </td>
        <td colspan="3">
          <asp:DropDownList ID="DDLRefilecategory" Height="20px" Width="188px" runat="server"
             DataTextField="C_CategoryName" DataValueField="C_Id" AutoPostBack="True" 
                onselectedindexchanged="DDLRefilecategory_SelectedIndexChanged">
            </asp:DropDownList>   
           
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                ControlToValidate="DDLRefilecategory" ErrorMessage="Please select category" 
                InitialValue="0" ValidationGroup="1"></asp:RequiredFieldValidator>
        </td>
        </tr>
        <tr>
        <td>
        Refill
        </td>
        <td>
           
            <br />
            <asp:ListBox ID="lstrefill" runat="server" Width="160px" Height="128px"
                                            SelectionMode="Multiple" 
                DataTextField="C_CategoryName" DataValueField="C_PId" 
               ></asp:ListBox>
               </td>
               <td align="center" valign="middle">
                <asp:Button ID="btnLeft" runat="server" Text="<<" OnClick="btnLeft_Click" />
                <asp:Button ID="btnRight" runat="server" Text=">>" OnClick="btnRight_Click" /><br />
                <asp:Button ID="btnLeftAll" runat="server" Text="< All" OnClick="btnLeftAll_Click" />
                <asp:Button ID="btnRightAll" runat="server" Text="All >" OnClick="btnRightAll_Click" />
            </td>
        <td>
&nbsp;&nbsp;
            <asp:ListBox ID="lstrefillupdate" runat="server" Width="160px" Height="128px"
              SelectionMode="Multiple" 
                DataTextField="C_CategoryName" DataValueField="C_PId"></asp:ListBox>
        </td>
        </tr>
        <tr>
        <td>
        Ink Color
        </td>
        <td>
            <asp:ListBox ID="ListBox1" runat="server" Width="160px" Height="128px"
                                            SelectionMode="Multiple" 
                DataTextField="InkColorM_Name" DataValueField="InkColorM_id" 
                onselectedindexchanged="ListBox1_SelectedIndexChanged"></asp:ListBox>
        </td>
               <td align="center" valign="middle">
                <asp:Button ID="Button1" runat="server" Text="<<" onclick="Button1_Click"  />
                <asp:Button ID="Button2" runat="server" Text=">>" onclick="Button2_Click" /><br />
                <asp:Button ID="Button3" runat="server" Text="< All" onclick="Button3_Click" />
                <asp:Button ID="Button4" runat="server" Text="All >" onclick="Button4_Click" />
            </td>
        <td>
            &nbsp;&nbsp;&nbsp;
            <asp:ListBox ID="lstcolorupdate" runat="server" Width="160px" Height="150px"
                                            SelectionMode="Multiple" 
                DataTextField="InkColorM_Name" DataValueField="InkColorM_Code" 
                onselectedindexchanged="lstcolorupdate_SelectedIndexChanged"></asp:ListBox>
                                      </td>
        </tr>
        <tr>
        <td>
        Prices
        
       
        </td>
        <td colspan="3">
            <asp:TextBox ID="txtprices" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                ControlToValidate="txtprices" ErrorMessage="Field cannot be blank" 
                ValidationGroup="1"></asp:RequiredFieldValidator>
        </td>
        </tr>
        <tr>
        <td>
        Banner      
            Image</td>
        <td colspan="3">
            <asp:FileUpload ID="FUBannerimg" runat="server" />
            <asp:RegularExpressionValidator 

ID="RegularExpressionValidator4" runat="server" 
            ControlToValidate="FUBannerimg" Display="None" 
            ErrorMessage="Please Add Only Image " 
            

ValidationExpression="^.+\.((jpg)|(gif)|(jpeg)|(png)|(bmp))$">

</asp:RegularExpressionValidator>
        </td>
        </tr>
        <tr>
        <td>
        Thumbnail      
        </td>
        <td colspan="3">
            <asp:FileUpload ID="FUthumbnail" runat="server" Height="22px" />
            <asp:RegularExpressionValidator 

ID="RegularExpressionValidator1" runat="server" 
            ControlToValidate="FUthumbnail" Display="None" 
            ErrorMessage="Please Add Only Image " 
            

ValidationExpression="^.+\.((jpg)|(gif)|(jpeg)|(png)|(bmp))$">

</asp:RegularExpressionValidator>
        </td>
        </tr>
        
        <tr>
        <td>
        Large
        </td>
        <td colspan="3">
            <asp:FileUpload ID="FUlarge" runat="server" />
            <asp:RegularExpressionValidator 
ID="RegularExpressionValidator2" runat="server" 
            ControlToValidate="FUlarge" Display="None" 
            ErrorMessage="Please Add Only Image "          

ValidationExpression="^.+\.((jpg)|(gif)|(jpeg)|(png)|(bmp))$">

</asp:RegularExpressionValidator>
        </td>
        </tr>
        <tr>
        <td>
        Zoom
        </td>
        <td colspan="3">
            <asp:FileUpload ID="FUzoom" runat="server" />
            <asp:RegularExpressionValidator 
ID="RegularExpressionValidator3" runat="server" 
            ControlToValidate="FUzoom" Display="None" 
            ErrorMessage="Please Add Only Image "          

ValidationExpression="^.+\.((jpg)|(gif)|(jpeg)|(png)|(bmp))$">
</asp:RegularExpressionValidator>
        </td>
        </tr>
        
        <tr>
        <td>
        PDF
        </td>
        <td colspan="3">
            <asp:FileUpload ID="FUpdf" runat="server" Width="218px" />
            <asp:RegularExpressionValidator ID="REPDF" runat="server" 

Display="None" ErrorMessage="Select PDF Only"
        ValidationExpression=".*\.(pdf)" 

ControlToValidate="FUpdf"></asp:RegularExpressionValidator>
        </td>
        </tr>
        <tr>
            <td colspan="4">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnupdate" runat="server" Text="Update"  
                    style="height: 26px" onclick="btnupdate_Click" ValidationGroup="1" />  </td>
        </tr>
        <tr>
        <td>
        <br />
        </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Image ID="Image1" runat="server" Height="100px" Width="100px" />
                <asp:Image ID="Image2" runat="server" Height="100px" Width="100px" />
                <asp:Image ID="Image3" runat="server" Height="100px" Width="100px" />
                <asp:Image ID="Image4" runat="server" Height="100px" Width="100px" />
                <br />
                <asp:HiddenField ID="HiddenField1" runat="server" />
                <asp:HiddenField ID="hfbannerimg" runat="server" />
                <asp:HiddenField ID="hfthumbimg" runat="server" />
                <asp:HiddenField ID="hflargeimg" runat="server" />
                <asp:HiddenField ID="hfzoomimg" runat="server" />
                <asp:HiddenField ID="hfpdf" runat="server" />
                <asp:HiddenField ID="HFSearch" runat="server" />
                <asp:HiddenField ID="hfrefillid" runat="server" />
                <asp:HiddenField ID="hfinkcolor" runat="server" />
                <br />
                <br />
                <br />
            </td>
        </tr>
    </table>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
</asp:Content>


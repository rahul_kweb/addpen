﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="InkColorAdd.aspx.cs" Inherits="Admin_CategoryAdd" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table>
       <tr>
<td colspan="2" align="center">
<b> Ink Color Add And List It  </b><hr />
</td>
</tr>
    
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="lbladd" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Current Ink Color
            </td>
            <td>
                <asp:DropDownList ID="DDLCategory" runat="server" 
                    DataTextField="InkColorM_Name" DataValueField="InkColorM_id">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Add New Ink
            </td>
            <td>
                <asp:TextBox ID="txtbrandname" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtbrandname" 
                    ErrorMessage="Category Name Cannot Be Blank"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Add Hash Code
            </td>
            <td>
                <asp:TextBox ID="txthashcode" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="txthashcode" 
                    ErrorMessage="Hash Code Cannot Be Blank"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="btnadd" runat="server" Text="Submit" OnClick="btnadd_Click" />
            </td>
        </tr>
    </table>
</asp:Content>


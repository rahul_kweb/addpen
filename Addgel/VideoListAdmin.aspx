﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="VideoListAdmin.aspx.cs" Inherits="Admin_VideoListAdmin" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%">
<tr>
<td colspan="2">
&nbsp;
</td>
</tr>
<tr>
<td colspan="2" align="center">
<asp:DropDownList ID="DDLStatus" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DDLStatus_SelectedIndexChanged">
        <asp:ListItem Selected="True">Active</asp:ListItem>
        <asp:ListItem>InActive</asp:ListItem>
    </asp:DropDownList>
</td>
</tr>
<tr>
<td colspan="2" align="center">
    <asp:Label ID="lblupdate" runat="server" Text=""></asp:Label>
</td>
</tr>
<tr>
<td colspan="2">
    <asp:GridView ID="GVVedio" Width="100%" runat="server" 
        AutoGenerateColumns="False" onrowdeleting="GVVedio_RowDeleting" 
        AllowPaging="True" onpageindexchanging="GVVedio_PageIndexChanging" 
        EmptyDataText="NO RECORD FOUND">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="ID" />
            <asp:TemplateField HeaderText="Id" Visible="False">
                <EditItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Title" HeaderText="Title" />
            <asp:BoundField DataField="URL" HeaderText="URL" />
            <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" 
                        OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" 
                        SelectedValue='<%# Bind("Status") %>'>
                        <asp:ListItem>Active</asp:ListItem>
                        <asp:ListItem>InActive</asp:ListItem>
                    </asp:RadioButtonList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:HyperLinkField DataNavigateUrlFields="Id" 
                DataNavigateUrlFormatString="VedioDetailsAdmin.aspx?id={0}" Text="more..." />
            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" />
        </Columns>
    </asp:GridView>
</td>
</tr>
<tr>
<td colspan="2" align="center">
    <asp:Button ID="btnapplychange" runat="server" Text="Apply Change" 
        onclick="btnapplychange_Click" />
</td>
</tr>
</table>
</asp:Content>


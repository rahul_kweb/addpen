﻿<%@ Page Language="C#" MasterPageFile="~/Addgel/Admin.master" AutoEventWireup="true" CodeFile="HistoryListAdmin.aspx.cs" Inherits="Addgel_HistoryListAdmin" Title="Add Gel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <table width="100%">
<tr>
<td align="center">
 
Category Listing <hr />
</td>
</tr>
<tr>
<td >
             
    <asp:RadioButtonList ID="RBLStatus" runat="server" AutoPostBack="True" 
        RepeatDirection="Horizontal" 
        onselectedindexchanged="RBLStatus_SelectedIndexChanged">
        <asp:ListItem Selected="True">Active</asp:ListItem>
        <asp:ListItem>InActive</asp:ListItem>
    </asp:RadioButtonList>
</td>
</tr>

<tr>
<td align="center">
    <asp:Label ID="lblupdate" runat="server" Text=""></asp:Label>
</td>
</tr>
<tr>
<td>
<br />
</td>
</tr>
<tr>
<td>
    <asp:GridView ID="GVCategory" Width="100%" runat="server" 
        AutoGenerateColumns="False" EmptyDataText="NO RECORD FOUND" 
        onrowdeleting="GVCategory_RowDeleting" 
       >
        <Columns>
            <asp:BoundField DataField="H_Id" HeaderText="PID" ReadOnly="True" />
            <asp:TemplateField HeaderText="Id" Visible="False">
                <EditItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("H_Id") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblid" runat="server" Text='<%# Bind("H_Id") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="H_Year" HeaderText="Year" />
            <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:CheckBox ID="chkstatus" runat="server" 
                        Checked='<%# Eval("H_Status").ToString().Equals("Active") %>' 
                        oncheckedchanged="chkstatus_CheckedChanged" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" />
            <asp:HyperLinkField DataNavigateUrlFields="H_Id" 
                DataNavigateUrlFormatString="HistoryDetailsadmin.aspx?id={0}" HeaderText="Edit" 
                Text="Edit" />
        </Columns>
    </asp:GridView>
</td>
</tr>
<tr>
<td align="center">
    <asp:Button ID="btnapply" runat="server" Text="Apply Change" 
        onclick="btnapply_Click" />
</td>
</tr>
</table>
</asp:Content>


﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_CategoryAdd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Check"] != null)
        {
            lbladd.Text = "";
            if (!IsPostBack)
            {
                BindDropDown();
            }

        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }

    }

    protected void btnadd_Click(object sender, EventArgs e)
    {


        bool check = Class1.InkColorInsert(txtbrandname.Text.Replace("'", "''"), txthashcode.Text.Replace("'", "''"));
        if (check == true)//  = if (check)
        {
            lbladd.Text = "Save Successfully";
            txtbrandname.Text = "";
            txthashcode.Text = "";
            BindDropDown();
        }
        else if (check == false) // = else if(!check)
        {
            lbladd.Text = "Ink Color Already Exits";
            txtbrandname.Text = "";
            txthashcode.Text = "";
        }
    }
    private void BindDropDown()
    {
        DataTable dt = Class1.GetRecord("select * from InkColorMTB");
        if (dt.Rows.Count > 0)
        {
            DDLCategory.DataSource = dt;
            DDLCategory.DataBind();
        }
        else
        {
            DDLCategory.Visible = false;
        }
    }
}

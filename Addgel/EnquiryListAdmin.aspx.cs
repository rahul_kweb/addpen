﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Admin_EnquiryListAdmin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            if (!IsPostBack)
            {
                BindEnquiry("Enquiry");
            }
        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }
    }
    private void BindEnquiry(string sendenquiry)
    {
        DataTable dt = Class1.GetRecord("select * from EnquiryTB where E_CategoryName='" + DDLEnquiryCatList.SelectedItem.Text + "' order by E_Id desc");
        GVEnquiry.DataSource = dt;
        GVEnquiry.DataBind();
    }
    protected void GVEnquiry_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GVEnquiry.PageIndex = e.NewPageIndex;
        BindEnquiry(DDLEnquiryCatList.SelectedItem.Text);
    }
    protected void DDLEnquiryCatList_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindEnquiry(DDLEnquiryCatList.SelectedItem.Text);
    }
    private void BindWithSearch(string sendenquiry,string search)
    {
        DataTable dt = Class1.GetRecord("select * from EnquiryTB where E_CategoryName='" + sendenquiry + "' and  E_Name  like '%" + txtsearch.Text.Replace("'", "''") + "%'");
        GVEnquiry.DataSource = dt;
        GVEnquiry.DataBind();

    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        BindWithSearch(DDLEnquiryCatList.SelectedItem.Text,txtsearch.Text);
    }


      protected void btndeletemulti_Click(object sender, EventArgs e)
    {
        bool[] rowChanged = new bool[GVEnquiry.Rows.Count];
        for (int r = 0; r < GVEnquiry.Rows.Count; r++)
        {
            GridViewRow thisGridViewRow = GVEnquiry.Rows[r];


            Label LId = (Label)thisGridViewRow.FindControl("lblID");
            int NewId = Convert.ToInt32(LId.Text);



            CheckBox rbStatus = (CheckBox)thisGridViewRow.FindControl("chkBxSelect");
            if (rbStatus.Checked)
            {

                bool check = Class1.InsertUpdate("Delete from EnquiryTB where E_Id=" + NewId + "");
            }



        }
         BindEnquiry(DDLEnquiryCatList.SelectedItem.Text);
    }

    protected void GVEnquiry_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow &&
   (e.Row.RowState == DataControlRowState.Normal ||
    e.Row.RowState == DataControlRowState.Alternate))
        {
            CheckBox chkBxSelect = (CheckBox)e.Row.Cells[1].FindControl("chkBxSelect");
            CheckBox chkBxHeader = (CheckBox)this.GVEnquiry.HeaderRow.FindControl("chkBxHeader");
            chkBxSelect.Attributes["onclick"] = string.Format
                                                   (
                                                      "javascript:ChildClick(this,'{0}');",
                                                      chkBxHeader.ClientID
                                                   );
        }
    }
}


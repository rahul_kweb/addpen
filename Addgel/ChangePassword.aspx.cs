using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_ChangePassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Check"] != null)
        {
            

        }
        else
        {
            Response.Redirect("HQAGP.aspx");
        }

    }
    protected void BtnApplyChange_Click(object sender, EventArgs e)
    {
        try
        {
            string curent = TxtCurrentPassword.Text.Replace("'", "''");
            string newpass = TxtNewPassword.Text.Replace("'", "''");
            string encode = base64Encode(curent);
            string encodenewpass = base64Encode(newpass);
            if (Class1.Validation((string)Session["Check"], encode))
            {
                Class1.ChangePassword((string)Session["Check"], encode, encodenewpass);
                LblMessage.Text = "Password Change Done";
              //  Response.Redirect("ChangeDone.aspx?mess=Password Change Done");
            }
            else
            {
                LblMessage.Text = "Wrong Current Password";
            }
        }
        catch (Exception ex)
        {
            LblMessage.Text = ex.Message.ToString();
        }
    }


    private string base64Encode(string sData)
    {
        try
        {
            byte[] encData_byte = new byte[sData.Length];

            encData_byte = System.Text.Encoding.UTF8.GetBytes(sData);

            string encodedData = Convert.ToBase64String(encData_byte);

            return encodedData;

        }
        catch (Exception ex)
        {
            throw new Exception("Error in base64Encode" + ex.Message);
        }
    }
    
}

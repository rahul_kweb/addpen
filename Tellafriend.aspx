﻿<%@ Page Language="C#" MasterPageFile="~/main1.master" AutoEventWireup="true" CodeFile="Tellafriend.aspx.cs" Inherits="Tellafriend" Title="ADD Gel - Tell a friend" %>

<%@ Register src="about.ascx" tagname="about" tagprefix="uc1" %>

<%@ Register src="app.ascx" tagname="app" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style2
        {
            width: 16%;
        }
        .style3
        {
            width: 15%;
            height: 25px;
        }
        .style4
        {
            height: 25px;
        }
         .style5
        {
        	color:Red;
        	}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="1000px" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
      <tr>
        <td align="left" valign="top"><img src="images/spacer.gif" width="10" height="10" /></td>
      </tr>
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="15" align="left" valign="top"><img src="images/spacer.gif" width="15" height="1" /></td>
                 
                  <td  align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="23%" align="left" valign="top">
                         
                         <table width="226" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td align="left" valign="top"><img src="images/toppr.gif" width="226" height="14" /></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top" background="images/prbg.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="6%" align="left" valign="top">&nbsp;</td>
                              <td width="94%" align="left" valign="top">
                                  <uc2:app ID="app1" runat="server" />
                                </td>
                            </tr>
                          </table>                         </td>
                          </tr>
                        <tr>
                          <td align="left" valign="top"><img src="images/bot.gif" width="226" height="16" /></td>
                        </tr>
                      </table>
                         </td>
                      <td width="77%" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1%" align="left" valign="top">&nbsp;</td>
                          <td width="99%" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="right" valign="top"><table width="716" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif"><table width="716" border="0" align="right" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td align="left" valign="top" class="tellafriend"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td height="45" align="left" valign="top">&nbsp;</td>
                                          </tr>
                                          <tr>
                                            <td align="left" valign="top"><table width="684" border="0" align="center" cellpadding="0" cellspacing="0">
                                                <tr>
                                                  <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                      <td align="left" valign="top"><img src="images/tellafreind.jpg" width="686" height="151" /></td>
                                                    </tr>
                                                  </table></td>
                                                </tr>
                                                
                                            </table></td>
                                          </tr>
                                      </table></td>
                                    </tr>
                                    
                                    
                                  </table></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif">&nbsp;</td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif">
                                  
                                  
                                  
                                  
                                  
                                  
                                  <table width="684" border="0" align="center" cellpadding="2" cellspacing="2">
                                    <tr>
                                      <td align="left" valign="top" bgcolor="#f7f5f6" class="textone"><strong>Tell a 
                                          friend Form</strong></td>
                                      </tr>
                                    <tr>
                                      <td align="left" valign="top" class="textone">
                                      
                                      
                                      <table id="tbid1" runat="server"  width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#EFE9EC" class="textone">
                                      <tr>
                                       <td colspan="2" width="10%" align="right" valign="top" bgcolor="#FFFFFF">
                                        <span class="style5">*</span> Mandatory Fields
                                        </td>
                                       </tr>
                                        <tr>
                                          <td align="left" valign="top" bgcolor="#FFFFFF" class="style2">&nbsp;Your Name &nbsp; <span class="style5">*</span></td>
                                          <td width="90%" height="25" align="left" valign="top" bgcolor="#FFFFFF">
                                              <label>
                                              <asp:TextBox ID="txtyname" runat="server" CssClass="select"></asp:TextBox>
&nbsp;</label>                                        
                                              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                  ControlToValidate="txtyname" ErrorMessage="Field cannot be left blank" 
                                                  ValidationGroup="1"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="top" bgcolor="#FFFFFF" class="style3">Your Email-Id &nbsp; <span class="style5">*</span></td>
                                          <td align="left" valign="top" bgcolor="#FFFFFF" class="style4">
                                              <asp:TextBox ID="txtyemailid" runat="server" CssClass="select"></asp:TextBox>
                                              &nbsp;
                                              <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                  ControlToValidate="txtyemailid" ErrorMessage="Field cannot be left blank" 
                                                  ValidationGroup="1"></asp:RequiredFieldValidator>
                                              <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                                  ControlToValidate="txtyemailid" ErrorMessage="Email Id is not in correct order" 
                                                  ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                                  ValidationGroup="1"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                          <td align="left" valign="top" bgcolor="#FFFFFF" class="style2">&nbsp;Friend Name &nbsp; <span class="style5">*</span></td>
                                          <td width="90%" height="25" align="left" valign="top" bgcolor="#FFFFFF">
                                              <label>
                                              <asp:TextBox ID="txtfrndname" runat="server" CssClass="select"></asp:TextBox>
&nbsp;</label>                                       
                                              <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                                                  ControlToValidate="txtfrndname" ErrorMessage="Field cannot be left blank" 
                                                  ValidationGroup="1"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                         
                                        <tr>
                                          <td align="left" valign="top" bgcolor="#FFFFFF" class="style2">&nbsp;Friend Email-Id &nbsp; <span class="style5">*</span></td>
                                          <td height="25" align="left" valign="top" bgcolor="#FFFFFF">
                                              <asp:TextBox ID="txtfrndemail" runat="server" CssClass="select"></asp:TextBox>
                                              &nbsp;
                                              <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                                  ControlToValidate="txtfrndemail" ErrorMessage="Field cannot be left blank" 
                                                  ValidationGroup="1"></asp:RequiredFieldValidator>
                                              <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                                  ControlToValidate="txtfrndemail" ErrorMessage="Email Id is not in correct order" 
                                                  ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                                  ValidationGroup="1"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
         
                                        
                                        <tr>
                                          <td align="left" valign="top" bgcolor="#FFFFFF" class="style2">&nbsp;</td>
                                          <td height="25" align="left" valign="top" bgcolor="#FFFFFF">
                                              <asp:ImageButton ID="btnsubmit" runat="server" width="76" height="26" 
                                                  ImageUrl="~/images/submit.gif" onclick="btnsubmit_Click" ValidationGroup="1"/>
                                                 
                                          </td>
                                        </tr>
                                        
                                      </table>
                                      
                                      
                                      
                                         <table id="tbid2" runat="server" visible="false"  width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#EFE9EC" class="textone">
                                         <tr>
                                         <td align="center">
                                             <asp:Label ID="lblsubmit" runat="server" Text=""></asp:Label>
                                         </td>
                                         </tr>
                                         </table> 
                                          
                                          
                                        </td>
                                      </tr>
                                    
                                  </table></td>
                                </tr>
                                
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif" class="aboutusbot">&nbsp;</td>
                                </tr>
                                
                              </table></td>
                            </tr>
                          </table></td>
                        </tr>
                      </table></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top">&nbsp;</td>
                      <td align="left" valign="top">&nbsp;</td>
                    </tr>
                  </table></td>
                  <td width="12"  align="right" valign="top"><img src="images/spacer.gif" width="15" height="1" /></td>
                </tr>
                
          </table></td>
      </tr>
    </table>
</asp:Content>


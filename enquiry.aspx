﻿<%@ Page Language="C#" MasterPageFile="~/main1.master" AutoEventWireup="true" CodeFile="enquiry.aspx.cs" Inherits="enquiry" Title="ADD Gel - Enquiry" %>

<%@ Register Src="about.ascx" TagName="about" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style1 {
            height: 25px;
        }

        .style2 {
            color: Red;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="1000px" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
        <tr>
            <td align="left" valign="top">
                <img src="images/spacer.gif" width="10" height="10" /></td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="15" align="left" valign="top">
                            <img src="images/spacer.gif" width="15" height="1" /></td>

                        <td align="left" valign="top">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="23%" align="left" valign="top">
                                        <uc1:about ID="about1" runat="server" />
                                    </td>
                                    <td width="77%" align="left" valign="top">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="1%" align="left" valign="top">&nbsp;</td>
                                                <td width="99%" align="left" valign="top">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="right" valign="top">
                                                                <table width="716" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td align="left" valign="top" background="images/probg.gif">
                                                                            <table width="716" border="0" align="right" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td align="left" valign="top" class="enquiry">
                                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td height="45" align="left" valign="top">&nbsp;</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" valign="top">
                                                                                                    <table width="684" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                                                        <tr>
                                                                                                            <td align="left" valign="top">
                                                                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                    <tr>
                                                                                                                        <td align="left" valign="top">
                                                                                                                            <img src="images/enquiry.jpg" width="686" height="151" /></td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>


                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="top" background="images/probg.gif">&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="top" background="images/probg.gif">
                                                                            <table width="684" border="0" align="center" cellpadding="2" cellspacing="2">
                                                                                <tr>
                                                                                    <td align="left" valign="top" bgcolor="#f7f5f6" class="textone"><strong>Enquiry Form</strong></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" valign="top" bgcolor="" class="textone"><strong>For bulk inquiries : <a href="mailto:corpsales@addpens.com">corpsales@addpens.com</a> </strong></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" valign="top" class="textone">
                                                                                        <table id="tbid1" runat="server" width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#EFE9EC" class="textone">


                                                                                            <tr>
                                                                                                <td colspan="2" width="10%" align="left" class="textone" valign="top" bgcolor="#FFFFFF">
                                                                                                    <asp:RadioButtonList ID="RBLEnquiryType" runat="server" CssClass="textone"
                                                                                                        RepeatDirection="Horizontal" AutoPostBack="True"
                                                                                                        OnSelectedIndexChanged="RBLEnquiryType_SelectedIndexChanged">
                                                                                                        <asp:ListItem Value="1" Selected="True">Enquiry</asp:ListItem>
                                                                                                        <asp:ListItem Value="2">Suggestion</asp:ListItem>
                                                                                                        <asp:ListItem Value="3">Complaint</asp:ListItem>
                                                                                                    </asp:RadioButtonList>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="2" width="10%" align="right" valign="top" bgcolor="#FFFFFF">
                                                                                                    <span class="style2">*</span> Mandatory Fields
                                                                                                </td>
                                                                                            </tr>



                                                                                            <tr>
                                                                                                <td width="10%" align="left" valign="top" bgcolor="#FFFFFF">Name&nbsp;<span class="style2">*</span> </td>
                                                                                                <td width="90%" height="25" align="left" valign="top" bgcolor="#FFFFFF">
                                                                                                    <label>
                                                                                                        <asp:TextBox ID="txtname" runat="server" CssClass="select_a"></asp:TextBox>
                                                                                                        &nbsp;</label>
                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                                                                        ControlToValidate="txtname" ErrorMessage="Field cannot be left blank"
                                                                                                        ValidationGroup="1"></asp:RequiredFieldValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" valign="top" bgcolor="#FFFFFF">Address&nbsp;<span class="style2">*</span></td>
                                                                                                <td height="25" align="left" valign="top" bgcolor="#FFFFFF">
                                                                                                    <asp:TextBox ID="txtaddress" runat="server" CssClass="select_a" Height="80"
                                                                                                        TextMode="MultiLine"></asp:TextBox>
                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                                                                        ControlToValidate="txtaddress" ErrorMessage="Field cannot be left blank"
                                                                                                        ValidationGroup="1"></asp:RequiredFieldValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" valign="top" bgcolor="#FFFFFF">Email-Id&nbsp;<span class="style2">*</span></td>
                                                                                                <td height="25" align="left" valign="top" bgcolor="#FFFFFF">
                                                                                                    <asp:TextBox ID="txtemail" runat="server" CssClass="select_a"></asp:TextBox>
                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                                                                                        ControlToValidate="txtemail" ErrorMessage="Field cannot be left blank"
                                                                                                        ValidationGroup="1"></asp:RequiredFieldValidator>
                                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                                                                        ErrorMessage="Email - Id is not in correct order"
                                                                                                        ControlToValidate="txtemail"
                                                                                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                                                        ValidationGroup="1"></asp:RegularExpressionValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" valign="top" bgcolor="#FFFFFF" class="style1">Contact&nbsp;<span class="style2">*</span></td>
                                                                                                <td align="left" valign="top" bgcolor="#FFFFFF" class="style1">
                                                                                                    <asp:TextBox ID="txtcontact" runat="server" CssClass="select_a"></asp:TextBox>
                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                                                                                                        ControlToValidate="txtcontact" ErrorMessage="Field cannot be left blank"
                                                                                                        ValidationGroup="1"></asp:RequiredFieldValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td id="enquiry1" align="left" valign="top" bgcolor="#FFFFFF">
                                                                                                    <asp:Label ID="lblenquiry" runat="server" Text=""></asp:Label>&nbsp;<span class="style2">*</span></td>
                                                                                                <td height="25" align="left" valign="top" bgcolor="#FFFFFF">
                                                                                                    <asp:TextBox ID="txtcomments" runat="server" CssClass="select_a" Height="80"
                                                                                                        TextMode="MultiLine"></asp:TextBox>
                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server"
                                                                                                        ControlToValidate="txtcomments" ErrorMessage="Field cannot be left blank"
                                                                                                        ValidationGroup="1"></asp:RequiredFieldValidator>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
                                                                                                <td height="25" align="left" valign="top" bgcolor="#FFFFFF">
                                                                                                    <asp:ImageButton ID="btnsubmit" runat="server" Width="76" Height="26"
                                                                                                        ImageUrl="~/images/submit.gif" OnClick="btnsubmit_Click" ValidationGroup="1" />
                                                                                                </td>
                                                                                            </tr>

                                                                                        </table>
                                                                                        <table id="tbid2" runat="server" visible="false" width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#EFE9EC" class="textone">
                                                                                            <tr>
                                                                                                <td align="center">
                                                                                                    <asp:Label ID="lblsubmit" runat="server" Text=""></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>


                                                                                    </td>
                                                                                </tr>

                                                                            </table>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td align="left" valign="top" background="images/probg.gif" class="aboutusbot">&nbsp;</td>
                                                                    </tr>

                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">&nbsp;</td>
                                    <td align="left" valign="top">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                        <td width="12" align="right" valign="top">
                            <img src="images/spacer.gif" width="15" height="1" /></td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>
</asp:Content>



(function($){
	
	var opts, objs;
	
    $.fn.samscroller = function(settings){
		
		opts = jQuery.extend({timeoutTimer: false, scrollSpeed: 50});
		objs = this;
										  
		return this.each(function(){
			$(this).click(function(){
				$(this).samscroller.scrollnow(this);
				$('.samscroller').removeClass('selc');
				$(this).addClass('selc');
				return false;
			});			
		});
		
	};

	$.fn.samscroller.scrollnow = function(obj){
			
		clearInterval(opts.timeoutTimer);
		//alert($(window).scrollTop());
		//schd = $(document.body).height() - $(window).height();
		keys = obj.href.split('#');
		if(keys.length>0)key = keys[keys.length-1]; else return false;
		
		
		opts.timeoutTimer = window.setInterval(function(){_scrollto(key,obj)},opts.scrollSpeed);
		
		
		//$('#gallerybox-overlay').css({width: $(document.body).width(), height: $(document.body).height()});
		//$(window).animate({scrolltop:100},500,function(){ alert('ok');});
	};	
	
	function _scrollto(key,obj){
		
		//alert(obj);
		//clearInterval(opts.timeoutTimer); return;
		
		objs = $('#'+key);
		objtop = parseInt(objs.position().top);
		scrltop = $(window).scrollTop();
		schd = $(document.body).height() - $(window).height();
		if(objtop > scrltop)inc = 1; else inc = -1;		
		diff = Math.abs(objtop - scrltop);
		if(diff <=2){	clearInterval(opts.timeoutTimer); return; }
		if(scrltop+1 >=schd && inc>0){	clearInterval(opts.timeoutTimer); return; }
		if(scrltop<=0 && inc<0){	clearInterval(opts.timeoutTimer); return; }
		$(window).scrollTop(scrltop + ((diff/3)* inc));
										
		//clearInterval(opts.timeoutTimer);
	};		
		
		
})(jQuery);



$(document).ready(function () {							
    $('.samscroller').samscroller();
	
});
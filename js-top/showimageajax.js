
var enableCache = true;
var jsCache = new Array();

var dynamicContent_ajaxObjects = new Array();

function ajax_showContent(containerid,ajaxIndex,url)
{
	document.getElementById(containerid).innerHTML = dynamicContent_ajaxObjects[ajaxIndex].response;
	if(enableCache){
		jsCache[url] = 	dynamicContent_ajaxObjects[ajaxIndex].response;
	}
	dynamicContent_ajaxObjects[ajaxIndex] = false;
}

function ajax_loadContent(url,containerid,img)
{

/*
 if(enableCache && jsCache[url]){
		document.getElementById(containerid).innerHTML = jsCache[url];
		return;
	}*/
	

	
	//<IMG SRC="../images/loadingemail.gif" />
	var ajaxIndex = dynamicContent_ajaxObjects.length;
	document.getElementById(containerid).innerHTML = "<img src='"+img+"' align='middle'>";
	dynamicContent_ajaxObjects[ajaxIndex] = new sack();
	
	 showimage();
	
	
	setTimeout('clsdiv()', 500);
	
	dynamicContent_ajaxObjects[ajaxIndex].requestFile = url;	// Specifying which file to get
	dynamicContent_ajaxObjects[ajaxIndex].onCompletion = function(){ ajax_showContent(containerid,ajaxIndex,url); };	// Specify function that will be executed after file has been found
	dynamicContent_ajaxObjects[ajaxIndex].runAJAX();		// Execute AJAX function	
		
		if(enableCache && jsCache[url]){
		document.getElementById(containerid).innerHTML = jsCache[url];
		return;
	}
		
		
}


function sack(file){
	this.AjaxFailedAlert = "Your browser does not support the enhanced functionality of this website, and therefore you will have an experience that differs from the intended one.\n";
	this.requestFile = file;
	this.method = "POST";
	this.URLString = "";
	this.encodeURIString = true;
	this.execute = false;

	this.onLoading = function() { };
	this.onLoaded = function() { };
	this.onInteractive = function() { };
	this.onCompletion = function() { };

	this.createAJAX = function() {
		try {
			this.xmlhttp = new ActiveXObject("Msxml2.XMLHTTP.5.0");
		} catch (e) {
			try {
				this.xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (err) {
				this.xmlhttp = null;
			}
		}
		if(!this.xmlhttp && typeof XMLHttpRequest != "undefined")
			this.xmlhttp = new XMLHttpRequest();
		if (!this.xmlhttp){
			this.failed = true; 
		}
	};
	
	this.setVar = function(name, value){
		if (this.URLString.length < 3){
			this.URLString = name + "=" + value;
		} else {
			this.URLString += "&" + name + "=" + value;
		}
	}
	
	this.encVar = function(name, value){
		var varString = encodeURIComponent(name) + "=" + encodeURIComponent(value);
	return varString;
	}
	
	this.encodeURLString = function(string){
		varArray = string.split('&');
		for (i = 0; i < varArray.length; i++){
			urlVars = varArray[i].split('=');
			if (urlVars[0].indexOf('amp;') != -1){
				urlVars[0] = urlVars[0].substring(4);
			}
			varArray[i] = this.encVar(urlVars[0],urlVars[1]);
		}
	return varArray.join('&');
	}
	
	this.runResponse = function(){
		eval(this.response);
	}
	
	this.runAJAX = function(urlstring){
		this.responseStatus = new Array(2);
		if(this.failed && this.AjaxFailedAlert){ 
			alert(this.AjaxFailedAlert); 
		} else {
			if (urlstring){ 
				if (this.URLString.length){
					this.URLString = this.URLString + "&" + urlstring; 
				} else {
					this.URLString = urlstring; 
				}
			}
			if (this.encodeURIString){
				var timeval = new Date().getTime(); 
				this.URLString = this.encodeURLString(this.URLString);
				this.setVar("rndval", timeval);
			}
			if (this.element) { this.elementObj = document.getElementById(this.element); }
			if (this.xmlhttp) {
				var self = this;
				if (this.method == "GET") {
					var totalurlstring = this.requestFile + "?" + this.URLString;
					this.xmlhttp.open(this.method, totalurlstring, true);
				} else {
					this.xmlhttp.open(this.method, this.requestFile, true);
				}
				if (this.method == "POST"){
  					try {
						this.xmlhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded')  
					} catch (e) {}
				}

				this.xmlhttp.send(this.URLString);
				this.xmlhttp.onreadystatechange = function() {
					switch (self.xmlhttp.readyState){
						case 1:
							self.onLoading();
						break;
						case 2:
							self.onLoaded();
						break;
						case 3:
							self.onInteractive();
						break;
						case 4:
							self.response = self.xmlhttp.responseText;
							self.responseXML = self.xmlhttp.responseXML;
							self.responseStatus[0] = self.xmlhttp.status;
							self.responseStatus[1] = self.xmlhttp.statusText;
							self.onCompletion();
							if(self.execute){ self.runResponse(); }
							if (self.elementObj) {
								var elemNodeName = self.elementObj.nodeName;
								elemNodeName.toLowerCase();
								if (elemNodeName == "input" || elemNodeName == "select" || elemNodeName == "option" || elemNodeName == "textarea"){
									self.elementObj.value = self.response;
								} else {
									self.elementObj.innerHTML = self.response;
								}
							}
							self.URLString = "";
						break;
					}
				};
			}
		}
	};
this.createAJAX();
}


/* Offset position of tooltip 
var x_offset_tooltip = -400;
var y_offset_tooltip = -400;
*/
/* Don't change anything below here */
var ajax_tooltipObj = false;
var ajax_tooltipObj_iframe = false;
var ajax_tooltip_MSIE = false;
if(navigator.userAgent.indexOf('MSIE')>=0)ajax_tooltip_MSIE=true;

function showimage()
{
	   dh=window.screen.availHeight+'px'
		ajax_tooltipObj = document.createElement('DIV');
		ajax_tooltipObj.id = 'ajax_tooltipObj';	
		ajax_tooltipObj.style.display='block';
		ajax_tooltipObj.style.position = 'absolute';
	
					
	

	
	   ajax_tooltipObj.style.top ='0px';
	
	   ajax_tooltipObj.style.left ='0px';
	   ajax_tooltipObj.style.Width ='100%';
	
	   ajax_tooltipObj.style.height =dh;
	
	   ajax_tooltipObj.style.color = '#fff'; 
	   ajax_tooltipObj.style.zIndex = 5000;
	   ajax_tooltipObj.style.opacity=.1;
	   ajax_tooltipObj.style.filter = 'progid:DXImageTransform.Microsoft.-moz-opacity=0.1';
	   ajax_tooltipObj.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity=10)';
	
				
		window.document.body.appendChild(ajax_tooltipObj);	
		
	/*	var contentDiv = document.createElement('DIV'); /* Create tooltip content div 
		ajax_tooltipObj.appendChild(contentDiv);
		contentDiv.id = 'ajax_tooltip_content';
		
	
	
	
	
	
	
	var x = document.createElement('div');
	x.id = 'div1';  
	x.style.display='block';
	x.style.position = 'absolute';  
	x.style.top ='0px';
	x.style.left ='0px';
	x.style.width = '100%'; 
	x.style.height = dh;  
	//x.style.backgroundColor = '#ffffff'; 
	x.style.color = '#fff'; 
	x.style.zIndex = 5000;
	x.style.opacity=.1;
	x.style.filter = 'progid:DXImageTransform.Microsoft.-moz-opacity=0.1';
	x.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity=10)';
	window.document.body.appendChild(x);
	
	//prepareIE ('100%', 'hidden');

		
		
		
		if(ajax_tooltip_MSIE){	 Create iframe object for MSIE in order to make the tooltip cover select boxes 
			ajax_tooltipObj_iframe = document.createElement('<IFRAME frameborder="0">');
			ajax_tooltipObj_iframe.style.position = 'absolute';
			ajax_tooltipObj_iframe.border='0';
			ajax_tooltipObj_iframe.frameborder=0;
			ajax_tooltipObj_iframe.style.backgroundColor='#FFF';
			ajax_tooltipObj_iframe.src = 'about:blank';
			contentDiv.appendChild(ajax_tooltipObj_iframe);
			ajax_tooltipObj_iframe.style.left = '25px';
			ajax_tooltipObj_iframe.style.top = '25px';
		}*/
		
	//}
	// Find position of tooltip
	
	//ajax_loadContent('ajax_tooltip_content',externalFile);
//	if(ajax_tooltip_MSIE){
//		ajax_tooltipObj_iframe.style.width = ajax_tooltipObj.clientWidth + 'px';
//		ajax_tooltipObj_iframe.style.height = ajax_tooltipObj.clientHeight + 'px';
//	}

	//ajax_positionTooltip(inputObj);
	
	
	getScroll();
	setScroll(0, 0);	
	hideSelects('hidden');
		
		
	
}









function setScroll(x, y){
	window.scrollTo(x, y); 
}

function getScroll(){
	if (self.pageYOffset) {
		this.yPos = self.pageYOffset;
	} else if (document.documentElement && document.documentElement.scrollTop){
		this.yPos = document.documentElement.scrollTop; 
	} else if (document.body) {
		this.yPos = document.body.scrollTop;
	}
}

function prepareIE (height, overflow){
	bod = document.getElementsByTagName('body')[0];
	bod.style.height = height;
	bod.style.overflow = overflow;
	//bod.style.overflow-x= "hidden";
	//bod.style.overflow-y= "scroll";
	
	htm = document.getElementsByTagName('html')[0];
	htm.style.height = height;
	htm.style.overflow = overflow; 
}

function hideSelects(visibility){
	selects = document.getElementsByTagName('select');
	for(i = 0; i < selects.length; i++) {
		selects[i].style.visibility = visibility;
	}
}

function clsdiv(){
	var parent = document.getElementById('div1');
	parent.parentNode.removeChild(parent);	
	
	//prepareIE ("auto", "auto");
	setScroll(0,this.yPos);
	hideSelects("visible");
}



function clsdivp(){
	//hideChecks("false");	
	var parent = document.getElementById('dp');
	parent.parentNode.removeChild(parent);	
		
	setScroll(0,this.yPos);
	hideSelects("visible");
	
}

















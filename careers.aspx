﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeFile="careers.aspx.cs" Inherits="careers" Title="Add Gel - Careers" %>

<%@ Register src="download.ascx" tagname="download" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="1000px" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
      <tr>
        <td align="left" valign="top"><img src="images/spacer.gif" width="10" height="10" /></td>
      </tr>
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="15" align="left" valign="top"><img src="images/spacer.gif" width="15" height="1" /></td>
                 
                  <td  align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="23%" align="left" valign="top">
                          <uc1:download ID="download1" runat="server" />
                        </td>
                      <td width="77%" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="1%" align="left" valign="top">&nbsp;</td>
                          <td width="99%" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="right" valign="top"><table width="716" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif"><table width="716" border="0" align="right" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td align="left" valign="top" class="careers"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                            <td height="45" align="left" valign="top">&nbsp;</td>
                                          </tr>
                                          <tr>
                                            <td align="left" valign="top"><table width="684" border="0" align="center" cellpadding="0" cellspacing="0">
                                                <tr>
                                                  <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                      <td align="left" valign="top"><img src="images/careers.jpg" width="686" height="151" /></td>
                                                    </tr>
                                                  </table></td>
                                                </tr>
                                                
                                            </table></td>
                                          </tr>
                                      </table></td>
                                    </tr>
                                    
                                    
                                  </table></td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif">&nbsp;</td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif">
                                  
                                  
                                  <table width="100%">
                                  <tr>
                                  <td>
                                <table width="100%">
    <tr>
    <td>
        <asp:DataList ID="DataList1" runat="server" DataKeyField="Jobs_id"
        Width="90%"  onitemcommand="DataList1_ItemCommand">
        <ItemTemplate>
         <span  
                style="border-collapse: separate; color: rgb(0, 0, 0); font-family: 'Times New Roman'; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: 2; text-align: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; -webkit-text-decorations-in-effect: none; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; font-size: medium; ">
        <table width="100%">
        <tr>
        <td >
        <span 
                style="color:Red; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; line-height: 22px; text-align: left; ">
            Post Vacant : 
            <asp:Label ID="lbltitle" runat="server" Text='<%# Eval("Jobs_Title") %>'></asp:Label> 
            (<asp:Label ID="lblrequirement" runat="server" Text='<%# Eval("Jobs_Requirement") %>'></asp:Label>)</span>
        </td>
        </tr>
         <tr>
        <td>
         <img id="Img1" src="images/spacer.gif" width="1" height="7" alt="" runat="server" />
        </td>
        </tr>
        <tr>
        <td>
          <span 
                style="color:Gray; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; line-height: 22px; text-align: left; ">
            Company Name : </span>
            <span 
                style="color:Gray; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; line-height: 22px; text-align: left; ">
            
            <asp:Label ID="Label3" runat="server" Text="Add Pens Private Limited."></asp:Label></span>
        </td>
        </tr>
         <tr>
        <td align="justify">
          <span 
                style="color:Gray; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; line-height: 22px; text-align: left; ">
            Company Profile : </span>
            <span 
                style="color:Gray; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; line-height: 22px; text-align: left; ">
            
            <asp:Label ID="lblprimaryskill" runat="server" Text='<%# Eval("Jobs_PrimarySkills") %>'></asp:Label></span>
        </td>
        </tr>
        
        
         
         <tr>
        <td align="justify">
          <span 
                style="color:Gray; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; line-height: 22px; text-align: left; ">
            Job Description/Responsibility : </span>
         <span 
                style="color:Gray; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; line-height: 22px; text-align: left; ">
               <asp:Label ID="lbljobdescription" runat="server" Text='<%# Eval("Jobs_Description") %>'></asp:Label></span>
        </td>
        </tr>
        
          <tr>
        <td align="justify">
          <span 
                style="color:Gray; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; line-height: 22px; text-align: left; ">
             Desired profile of candidate : </span>
         <span 
                style="color:Gray; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; line-height: 22px; text-align: left; ">
               <asp:Label ID="Label4" runat="server" Text='<%# Eval("Jobs_Skills") %>'></asp:Label></span>
        </td>
        </tr>
        
         <tr>
        <td>
        <span 
                style="color:Gray; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; line-height: 22px; text-align: left; ">
             Minimum experience : </span>
        <span 
                style="color:Gray; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; line-height: 22px; text-align: left; ">
                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Jobs_Experience") %>'></asp:Label></span>
        </td>
        </tr>
        <tr>
        <td>
           <span 
                style="color:Gray; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; line-height: 22px; text-align: left; ">
            Location : </span>
             <span 
                style="color:Gray; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; line-height: 22px; text-align: left; ">
            
            <asp:Label ID="lbllocation" runat="server" Text='<%# Eval("Jobs_Location") %>'></asp:Label></span>
        </td>
        </tr>
         <tr>
        <td>
        <span 
                style="color:Gray; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; line-height: 22px; text-align: left; ">
            Salary : </span>
          <span 
                style="color:Gray; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; line-height: 22px; text-align: left; ">
                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Jobs_Salary") %>'></asp:Label></span>
        </td>
        </tr>
         <tr>
         <td>
             <img src="images/spacer.gif" width="1" height="7" alt="" runat="server" />
         </td>
         </tr>
        <tr>
        <td>
          <asp:ImageButton ID="addtolist" runat="server"  border="0" CommandName="applyjob" 
           ImageUrl="images/apply.jpg" onclick="addtolist_Click" />
          
        </td>
        </tr>
        <tr>
        <td>
         <br />
          
        </td>
        </tr>
        
        </table>          
           
         
             
</span>
        
        </ItemTemplate>
        </asp:DataList>
    </td>
    </tr>
    </table>
                                  
                                  </td>
                                  </tr>
                                  </table>
                                  
                                  
                                  
                                      &nbsp;</td>
                                </tr>
                                
                                <tr>
                                  <td align="left" valign="top" background="images/probg.gif" class="careersbot">&nbsp;</td>
                                </tr>
                                
                              </table></td>
                            </tr>
                          </table></td>
                        </tr>
                      </table></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top">&nbsp;</td>
                      <td align="left" valign="top">&nbsp;</td>
                    </tr>
                  </table></td>
                  <td width="12"  align="right" valign="top"><img src="images/spacer.gif" width="15" height="1" /></td>
                </tr>
                
          </table></td>
      </tr>
    </table>

</asp:Content>


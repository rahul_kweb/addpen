﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class DetailedToPerfection : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            Loaddata();

        }

    }

    public void Loaddata()
    {


        DataTable dt = Class1.GetRecord("SELECT * from CMS_control where web_id=5");

        if (dt.Rows.Count != 0)
        {


            if (dt.Rows[0]["web_description"] == System.DBNull.Value == false)
            {

                lbldetailedtoperfection.Text = dt.Rows[0]["web_description"].ToString();
            }
        }
    }
}

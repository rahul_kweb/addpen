<%@ Page Language="C#" MasterPageFile="~/main1.master" AutoEventWireup="true" CodeFile="history.aspx.cs"
    Inherits="history" Title="ADD Gel - History" %>

<%@ Register Src="about.ascx" TagName="about" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="1000px" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF"
        align="center">
        <tr>
            <td align="left" valign="top">
                <img src="images/spacer.gif" width="10" height="10" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="15" align="left" valign="top">
                            <img src="images/spacer.gif" width="15" height="1" />
                        </td>
                        <td align="left" valign="top">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="23%" align="left" valign="top">
                                        <uc1:about ID="about1" runat="server" />
                                    </td>
                                    <td width="77%" align="left" valign="top">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="1%" align="left" valign="top">
                                                    &nbsp;
                                                </td>
                                                <td width="99%" align="left" valign="top">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="right" valign="top">
                                                                <table width="716" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td align="left" valign="top" background="images/probg.gif">
                                                                            <table width="716" border="0" align="right" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td align="left" valign="top" class="aboutus">
                                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                            <tr>
                                                                                                <td height="45" align="left" valign="top">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left" valign="top">
                                                                                                    <table width="684" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                                                        <tr>
                                                                                                            <td align="left" valign="top">
                                                                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                                    <tr>
                                                                                                                        <td align="left" valign="top">
                                                                                                                            <img src="images/history.jpg" width="684" height="149" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="top" background="images/probg.gif">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" valign="top" background="images/probg.gif">
                                                                            <table width="684" border="0" align="center" cellpadding="2" cellspacing="2">
                                                                                <tr>
                                                                                    <td align="left" valign="top" bgcolor="#f7f5f6" class="textone">
                                                                                        <strong>History</strong>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" valign="top" class="textone">
                                                                                        <script type="text/javascript">


                                                                                            ddaccordion.init({
                                                                                                headerclass: "submenuheader", //Shared CSS class name of headers group
                                                                                                contentclass: "submenu", //Shared CSS class name of contents group
                                                                                                revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
                                                                                                mouseoverdelay: 0, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
                                                                                                collapseprev: true, //Collapse previous content (so only one open at any time)? true/false 
                                                                                                defaultexpanded: [1], //index of content(s) open by default [index1, index2, etc] [] denotes no content
                                                                                                onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
                                                                                                animatedefault: false, //Should contents open by default be animated into view?
                                                                                                persiststate: true, //persist state of opened contents within browser session?
                                                                                                toggleclass: ["", ""], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
                                                                                                togglehtml: ["suffix", "", ""], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
                                                                                                animatespeed: "normal", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
                                                                                                oninit: function (headers, expandedindices) { //custom code to run when headers have initalized
                                                                                                    //do nothing
                                                                                                },
                                                                                                onopenclose: function (header, index, state, isuseractivated) { //custom code to run whenever a header is opened or closed
                                                                                                    //do nothing
                                                                                                }
                                                                                            })


                                                                                        </script>
                                                                                        <table width="100%">
                                                                                            <span>�ADD� is a name synonymous to premium quality writing instruments in India and
                                                                                                Sub-continent. ADD Pens Private Limited was set up in 1987 with a clear vision to
                                                                                                become a leader in writing technology. From the humble beginnings, the company has
                                                                                                grown into one of India's leading manufacturer of Writing Instruments. This phenomenal
                                                                                                progress has seen many landmark moments over 2 decades. ADD Pens Private Limited
                                                                                                a flagship company of the ADD Group spells the success story of the Writing Instruments.
                                                                                                Based on sound human values and principles, following ethical practices and by using
                                                                                                state � of the art technology, ADD has pioneered in Gel Ink pens in India and quickly
                                                                                                followed it with ball pens, roller pens and utility products like markers, highlighters,
                                                                                                e.t.c.
                                                                                                <br />
                                                                                                <br />
                                                                                                Today, ADD Pens Private Limited has created a niche for itself by giving customers
                                                                                                the highest quality writing instruments in India. Having achieved the leadership
                                                                                                in Gel Pens technology, ADD also enjoys a reputation for introducing new products.
                                                                                            </span>
                                                                                            <%-- <asp:Label ID="lblhistory" runat="server" Text=""></asp:Label>--%>
                                                                                            <%--  <tr height="25" >
                        <td align="left" class="data">
                        <a href="#" style="font-family:Arial; font-size:12px;text-decoration: none; font-weight:bold; color:#218AC4;"  class="menuitem submenuheader"><strong>General Purpose Measurement Products</strong></a></td>
                      </tr>
                      <tr>
                        <td   class="data" >
                        <div  class="submenu" align="justify"   style="width: 100%; ">                     
                        <span>Hello,This is test</span>
                        </div>
                          </td>
                      </tr>
                     
                      <tr height="25">
                        <td align="left" class="data"><a href="#" style="font-family:Arial;text-decoration: none; font-size:12px; font-weight:bold; color:#218AC4;"  class="menuitem submenuheader"><strong>Low Resistance and Winding Resistance Products</strong></a></td>
                      </tr>
                      <tr>
                        <td  class="data">
                        
                        <div class="submenu" align="justify" style="width: 100%; ">
                        
                        
                       <span>Hello,This is Amit Sharma</span>
                        
                       
                        
                        
                        </div>
                          
                          </td>
                      </tr>
                      <tr>
                        <td align="left"  class="data">&nbsp;</td>
                      </tr>
                      <tr height="25">
                        <td align="justify" class="data">
                        
                        <a href="#" style="font-family:Arial; font-size:12px;text-decoration: none; font-weight:bold; color:#218AC4;"  class="menuitem submenuheader"><strong>Insulation Testing</strong></a> </td>
                      </tr>
                      <tr>
                          <td   class="data" >
                        
                        <div class="submenu" align="justify" style="width: 100%; ">
                        
                        
                        
                        
                        
                        
                                  <span>Hello,This is Vishal Narvekar</span>
                        
                        
                        
                        
                        </div>
                          
                          </td>
                      </tr>
                      <tr>
                        <td align="left" align="justify" class="data">&nbsp;</td>
                      </tr>
                      <tr height="25">
                        <td align="left" style="font-family:Arial; font-size:12px; font-weight:bold; color:#218AC4;"  class="data"><strong><a href="#" style="font-family:Arial;text-decoration: none; font-size:12px; font-weight:bold; color:#218AC4;"  class="menuitem submenuheader"><strong>Earth and Soil Resistivity Testing</strong></a></td>
                      </tr>
                      <tr>
                   <td   class="data" >
                        <div class="submenu" align="justify" style="width: 100%; ">
                        
                        
                        
                        <span>Hello,This is Ritesh Bhavsar</span>
                       
                        
                        </div>
                          
                          </td>
                      </tr>
                      <tr>
                        <td align="left" class="data">&nbsp;</td>
                      </tr>
                      <tr height="25">
                        <td align="left" class="data"><a href="#" style="font-family:Arial; font-size:12px; font-weight:bold;text-decoration: none; color:#218AC4;"   class="menuitem submenuheader" ><strong>Safety and Protection</strong></a></td>
                      </tr>
                      <tr >
                        <td align="justify"  class="data" >
                        
                        <div class="submenu" align="justify" style="width: 100%; ">
                        
                        
                        
                        
                     <span>Hello,This is Hussain Kalasekar</span>
                        
                        
                          
                          </div>
                          
                          </td>
                      </tr>
                      <tr>
                        <td align="left" class="data">&nbsp;</td>
                      </tr>
                    
                      <tr>
                        <td align="left" class="data">&nbsp;</td>
                      </tr>
                  
                    </table>--%>
                                                                                            <tr>
                                                                                                <td align="left" valign="top" class="textone">
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" valign="top" background="images/probg.gif" class="aboutusbot">
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    &nbsp;
                                                </td>
                                                <td align="left" valign="top">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="12" align="right" valign="top">
                                        <img src="images/spacer.gif" width="15" height="1" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
</asp:Content>

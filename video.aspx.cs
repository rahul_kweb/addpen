﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class video : System.Web.UI.Page
{
    public string zoomimg = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ProductBindById();
        }

    }
    private void ProductBindById()
    {
        DataTable dt1 = Class1.GetRecord("select * from VideoTB where Status='Active'");
        lblvideo.Text = dt1.Rows[0]["EmbedCode"].ToString();
        DLProduct.DataSource = dt1;
        DLProduct.DataBind();
    }


    protected void datalist_command(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "Large1")
        {
            Label linkButton1 = (Label)e.Item.FindControl("lblzoom1");
            lblvideo.Text = linkButton1.Text;
            zoomimg = linkButton1.Text;
        }
    }
}

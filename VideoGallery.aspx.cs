﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class VideoGallery : System.Web.UI.Page
{
    public string Zoomimage = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {        
        bind_photo();
        bind_photo_newlaunch();
         if (Request.QueryString["vid"] != null)
        {

            string video_id = Request.QueryString["vid"].ToString();
            ProductSubImageBindById(video_id);

        }
        else
        {
            DataTable dt = Class1.GetRecord("select top 1 * from VideoTB order by Id");

            Zoomimage = dt.Rows[0]["EmbedCode"].ToString();
        }

    }

    private void ProductSubImageBindById(string pid)
    {
        //DataTable dt = Class1.GetRecord("select * from VideoTB where Id='" + pid + "' order by Id");
        DataTable dt = Class1.GetRecord("EXEC GetVideoTB 'GetById',"+pid);

        Zoomimage = dt.Rows[0]["EmbedCode"].ToString();

        // Response.Write("images=" + pid );  

        //  Zoomimage.Src = "Addgel/adminImage/" + dt.Rows[0]["P_Banner"].ToString();


    }

    

    public void bind_photo()
    {
        string picture = "";
        //DataTable dt1 = Class1.GetRecord("select * from VideoTB where IsLaunch=0");
        DataTable dt1 = Class1.GetRecord("EXEC GetVideoTB 'Get_By_IsLaunch',0,0");
      //  zoomimg = dt1.Rows[0]["Product_ZoomImage"].ToString();

        for (int i = 0; dt1.Rows.Count - 1 >= i; i++)
        {
            //style='float: none; vertical-align:'middle'; left: 0px; '

            picture += " <div class='panel' style='float: none; vertical-align:'top'; left: 0px; bottom:0px;'  >";
            //   picture += " 	  <a id='aimages' target='' name='aimages'  href='#' onclick=\"ajax_loadContent('fetch_video.aspx?vid=" + dt1.Rows[i]["video_embed"].ToString() + "&imgname=vishal','info','ajaxloader.gif');return false\" >";
            
            picture += " 	  <a id='aimages' target='' name='aimages'  href='VideoGallery.aspx?vid=" + dt1.Rows[i]["Id"].ToString() + "' > ";

            picture += " 	<img id='thumbimg'  src='images/" + dt1.Rows[i]["videoImage"].ToString() + "' border='0' alt=''   ></a>";

            picture += " <p class='gD_12 MT5'>" + dt1.Rows[i]["video_comment"].ToString() + "</p>";


            //  picture += "<br><span> " + dt1.Rows[i]["video_comment"].ToString() + "</span>";


        


            picture += " </div>";
            //  picture += "   <img src='Addgel/adminImage/" + dt1.Rows[i]["Product_ThumbnailImage"].ToString() + "' alt='' width='244' height='122' />&nbsp;&nbsp;  <a href='#' onclick='show_image('" + dt1.Rows[i]["Product_ThumbnailImage"].ToString() + "')'> ";
        }

        lblpictures.Text = picture;
    }

    public void bind_photo_newlaunch()
    {
        //string picture = "";
        DataTable dt1 = Class1.GetRecord("select * from VideoTB where IsLaunch=1 order by Id");
       
        // zoomimg = dt1.Rows[0]["Product_ZoomImage"].ToString();


        dlstnew.DataSource = dt1;
        dlstnew.DataBind();

        //for (int i = 0; dt1.Rows.Count - 1 >= i; i++)
        //{
        //    //style='float: none; vertical-align:'middle'; left: 0px; '

        //    // picture += " <div class='panel' style='float: none; vertical-align:'middle'; left: 0px;'  >";

        //    //   picture += " 	  <a id='aimages' target='' name='aimages'  href='#' onclick=\"ajax_loadContent('fetch_video.aspx?vid=" + dt1.Rows[i]["video_embed"].ToString() + "&imgname=vishal','info','ajaxloader.gif');return false\" >";

        //    picture += " 	  <a id='aimages' target='' name='aimages'  href='VideoGallery.aspx?vid=" + dt1.Rows[i]["Id"].ToString() + "' > ";

        //    picture += " 	<img id='thumbimg'  src='images/" + dt1.Rows[i]["videoImage"].ToString() + "' border='0' alt=''   ></a> ";


        //    picture += "  <a href='#'><p class='gD_12 MT5'>" + dt1.Rows[i]["video_comment"].ToString() + "</p></a>";


        //    //picture += "<br><span> " + dt1.Rows[i]["video_comment"].ToString() + "</span>";

        //    //  picture += " 	</div>";
        //    //  picture += "   <img src='Addgel/adminImage/" + dt1.Rows[i]["Product_ThumbnailImage"].ToString() + "' alt='' width='244' height='122' />&nbsp;&nbsp;  <a href='#' onclick='show_image('" + dt1.Rows[i]["Product_ThumbnailImage"].ToString() + "')'> ";
        //}

        //lblnewlaunch.Text = picture;
    }



    
}


